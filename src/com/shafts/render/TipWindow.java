package com.shafts.render;

import com.sun.awt.AWTUtilities;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JEditorPane;
import javax.swing.JFrame;

/**
 *
 * @author Little-Kitty
 */
public class TipWindow implements Runnable {

    private JFrame frame;
    // private JLabel label1;
    private JEditorPane editorPane1;

    private final int width;
    private final int height;
    private final int stayTime;
    private final int style;
    private final int xPos, yPos;
    private final String title, message;
//    static{
//        try {
//            UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
//        } catch (ClassNotFoundException | InstantiationException
//                | IllegalAccessException | UnsupportedLookAndFeelException e1) {
//            e1.printStackTrace();
//        }
//    }

    public TipWindow(int width, int height,
            int stayTime, int style, String title,
            int xPos, int yPos, String message) {
        this.width = width;
        this.height = height;
        this.stayTime = stayTime;
        this.style = style;
        this.title = title;
        this.message = message;
        this.xPos = xPos;
        this.yPos = yPos;
    }

    public TipWindow(int xPos, int yPos, String message) {
        this.xPos = xPos;
        this.yPos = yPos;
        this.width = 200;
        this.height = 120;
        this.stayTime = 3;
        this.style = 2;
        this.title = "Message Box";
        this.message = message;
    }

    public void _setVisible() {
        new Thread(this).start();
    }

    public static void main(String[] args) {
        new TipWindow(200, 200, "hello")._setVisible();
    }

    public void print() {
        frame = new JFrame();
        editorPane1 = new JEditorPane();
        editorPane1.setEditable(false);
        editorPane1.setContentType("text/html");
        editorPane1.setText("<html><body><center><strong>" + message + "</strong></center></body></html>");
        frame.add(editorPane1);
        frame.setTitle(title);
        //int x = Toolkit.getDefaultToolkit().getScreenSize().width - Toolkit.getDefaultToolkit().getScreenInsets(frame.getGraphicsConfiguration()).right - width - 5;
        //int y = Toolkit.getDefaultToolkit().getScreenSize().height - Toolkit.getDefaultToolkit().getScreenInsets(frame.getGraphicsConfiguration()).bottom - height - 5;
        frame.setBounds(xPos, yPos, width, height);
        frame.setUndecorated(true);
        frame.getRootPane().setWindowDecorationStyle(style);
//      frame.getRootPane().setWindowDecorationStyle(JRootPane.PLAIN_DIALOG ); //窗体样式
        AWTUtilities.setWindowOpacity(frame, 0.01f);
        frame.setVisible(true);
        frame.setAlwaysOnTop(true);
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                hide();
            }
        });
    }

    /**
     * 窗体逐渐变清晰
     *
     */
    public void show() {
        for (int i = 0; i < 20; i++) {
            sleep(20);
            AWTUtilities.setWindowOpacity(frame, i * 0.05f);
        }
    }

    private void sleep(int millsec) {
        try {
            Thread.sleep(millsec);
        } catch (InterruptedException ex) {
            Logger.getLogger(TipWindow.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * 窗体逐渐变淡直至消失
     *
     */
    public void hide() {
        float opacity = 100;
        while (true) {
            if (opacity < 2) {
                System.out.println();
                break;
            }
            opacity -= 2;
            AWTUtilities.setWindowOpacity(frame, opacity / 100);
            sleep(20);
        }
//      frame.hide();
        frame.dispose();
    }

    @Override
    public void run() {
        print();
        show();
        sleep(stayTime * 1000);
        hide();
    }

}
