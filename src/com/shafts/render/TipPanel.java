package com.shafts.render;

import java.awt.Color;
//import java.awt.Font;
//import java.awt.Graphics;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

/**
 *
 * @author Little-Kitty
 * @date 2015-4-15 19:51:09
 */
public class TipPanel extends JLabel {//extends JPanel {

    //private int cordiY;
    private final String tip;
    private Color tipColor;
    private final String type;
    private final int time;

    public TipPanel(String tip, String type) {
        this.tip = tip;
        this.type = type;
        this.time = 5000;
        init_Color_Change();
    }

    public TipPanel(String tip, String type, int time) {
        this.tip = tip;
        this.type = type;
        this.time = time;
        init_Color_Change();
    }

    private void init_Color_Change() {
        if (type.equals("Error")) {
            //fontType = "Consolas";
            tipColor = new Color(255, 0, 0);
        } else {
            //fontType = "Microsoft YaHei";
            tipColor = new Color(0, 0, 0);
        }
        showTip show = new showTip(type, time);
        if (show.getStop()) {
            show.setStop(false);
        }
        show.start();
    }

    class showTip extends Thread {

        private final String tipType;
        private final int waitTime;
        private boolean stop = false;

        public showTip(String tipType, int waitTime) {
            this.tipType = tipType;
            this.waitTime = waitTime;
        }

        public void setStop(boolean stop) {
            this.stop = stop;
        }

        public boolean getStop() {
            return stop;
        }

        @Override
        public void run() {
            //stop = false;
            int red, green, black;
            TipPanel.this.setFont(new java.awt.Font("微软雅黑", 0, 13));
            TipPanel.this.setForeground(tipColor);
            TipPanel.this.setText(tip);
            TipPanel.this.updateUI();
            switch (tipType) {
                case "Error":
                    waitFor(waitTime);
                    red = 255;
                    green = 0;
                    black = 0;
                    //stop when the rgb: 240 240 240
                    while (!stop) {
                        if (red > 240) {
                            red -= 1;
                        } else if (green < 240) {
                            green += 1;
                        } else if (black < 240) {
                            black += 1;
                        } else {
                            stop = true;
                        }
                        waitFor(10);
                        TipPanel.this.setForeground(new Color(red, green, black));
                    }
                    TipPanel.this.setText("");
                    TipPanel.this.updateUI();
                    break;
                default://OK
                    waitFor(waitTime);
                    red = 0;
                    green = 0;
                    black = 0;
                    while (!stop) {
                        if (red < 240) {
                            red += 1;
                        } else if (green < 240) {
                            green += 1;
                        } else if (black < 240) {
                            black += 1;
                        } else {
                            stop = true;
                        }
                        waitFor(10);
                        TipPanel.this.setForeground(new Color(red, green, black));
                    }
                    TipPanel.this.setText("");
                    TipPanel.this.updateUI();
                    break;
            }
        }
    }
    /*
     //  this method can let the msg appear from up to down
     //  and then disappear from down to up
     //
     //
     private void init() {
     if (type.equals("Error")) {
     fontType = "Consolas";
     tipColor = new Color(255, 0, 0);
     } else {
     fontType = "Microsoft YaHei";
     tipColor = new Color(0, 0, 0);
     }
     new Thread(new Runnable() {
     @Override
     public void run() {
     while (true) {
     cordiY += 1;
     if (cordiY > 13) {
     close();
     break;//
     }
     TipPanel.this.repaint();
     TipPanel.this.updateUI();
     sleep(10);
     }

     }
     }).start();
     }


     public void close() {
     sleep(time);
     Thread t = new Thread(new Runnable() {
     @Override
     public void run() {
     while (true) {
     cordiY -= 1;
     if (cordiY < -11) {
     break;
     }
     TipPanel.this.repaint();
     sleep(40);
     }

     }
     });
     t.start();
     }

     @Override
     public void paintComponent(Graphics g) {
     super.paintComponent(g);
     g.setColor(tipColor);
     g.setFont(new Font(fontType, Font.TRUETYPE_FONT, 12));
     g.drawString(tip, 15, cordiY);
     }
     */
    //private String fontType;

    private void waitFor(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            Logger.getLogger(TipPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
