
package com.shafts.render.treerender;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;
import static java.awt.image.ImageObserver.ABORT;
import static java.awt.image.ImageObserver.ALLBITS;
import static java.awt.image.ImageObserver.FRAMEBITS;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

/**
 *
 * @author Little-Kitty
 * @date 2015-7-24 13:22:47
 */
public class NodeImageObserver implements ImageObserver {   
    
    JTree tree;     
    DefaultTreeModel model;     
    TreeNode node;   
  
    public NodeImageObserver(JTree tree, TreeNode node) {   
      this.tree = tree;   
      this.model = (DefaultTreeModel) tree.getModel();   
      this.node = node;   
    }   
  
    @Override
    public boolean imageUpdate(Image img, int flags, int x, int y, int w,   
        int h) {   
      if ((flags & (FRAMEBITS | ALLBITS)) != 0) {   
        TreePath path = new TreePath(model.getPathToRoot(node));   
        Rectangle rect = tree.getPathBounds(path);   
        if (rect != null) {   
          tree.repaint(rect);   
        }   
      }   
      return (flags & (ALLBITS | ABORT)) == 0;   
    }   
  }
