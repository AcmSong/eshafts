/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shafts.render.treerender;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTree;
import javax.swing.tree.*;

/**
 *
 * @author Little-Kitty
 * @date 2014-10-2 10:17:41
 */
public class TreeRender extends DefaultTreeCellRenderer {

    private String[] nodeContent = null;
    private final boolean isDone;

    public TreeRender(String[] nodeContent, boolean isDone) {
        this.nodeContent = nodeContent;
        this.isDone = isDone;
    }

    @Override
    public Component getTreeCellRendererComponent(JTree tree,
            Object value,
            boolean selected,
            boolean expanded,
            boolean leaf,
            int row,
            boolean hasFocus) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
        super.getTreeCellRendererComponent(tree, value, selected, expanded,
                leaf, row, hasFocus);
        /*change by crazybaby 20150723*/

        setTextNonSelectionColor(new Color(0, 0, 0));
        setBackgroundSelectionColor(new Color(0, 0, 255));
        setBackgroundNonSelectionColor(null);

        if (nodeContent != null) {
            for (String nodeContent1 : nodeContent) {
                if (node.toString().equals(nodeContent1) && !isDone) {
                    setTextNonSelectionColor(new Color(255, 0, 0));
                    setBackgroundSelectionColor(new Color(0, 0, 255));
                    setBackgroundNonSelectionColor(null);
                } else {
                    setTextNonSelectionColor(new Color(0, 0, 0));
                    setBackgroundSelectionColor(new Color(0, 0, 255));
                    setBackgroundNonSelectionColor(null);
                }
            }
        }
        return this;
    }
}
