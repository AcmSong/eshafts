
package com.shafts.render.treerender;

import java.awt.Component;
import java.util.Hashtable;
import javax.swing.Icon;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeCellRenderer;

/**
 *
 * @author Little-Kitty
 * @date 2015-7-24 13:23:58
 */
public class IconNodeRenderer extends DefaultTreeCellRenderer {   
  
  @Override
  public Component getTreeCellRendererComponent(JTree tree, Object value,   
      boolean sel, boolean expanded, boolean leaf, int row,   
      boolean hasFocus) {   
  
    super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf,   
        row, hasFocus);   
  
    Icon icon = ((IconNode) value).getIcon();   
  
    if (icon == null) {   
      Hashtable icons = (Hashtable) tree.getClientProperty("computing.png");   
      String name = ((IconNode) value).getIconName();   
      if ((icons != null) && (name != null)) {   
        icon = (Icon) icons.get(name);   
        if (icon != null) {   
          setIcon(icon);   
        }   
      }   
    } else {   
      setIcon(icon);   
    }   
  
    return this;   
  }   
}
