package com.shafts.render;

import com.shafts.targetshow.TargetManager;
import com.shafts.module.JmolPanel;
import com.shafts.ui.TargetEffectInfor;
import com.shafts.utils.InitTargetJob;
import com.shafts.utils.InitVector;
import com.shafts.utils.MutiMolDisplay;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;
import javax.swing.event.RowSorterEvent;
import javax.swing.event.RowSorterListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

public class InitTargetTable {

    public InitTargetTable() {
        this.headerName = InitVector.getTargetHeader();
    }
    
    private void initTableMap(){
        //use the father table' first column as the keyid 
        for(int i = 0; i<table.getRowCount(); i++){
            tableMap.put(i, table.getValueAt(i, 0));
        }
    }

    public JTable getTargetTable() {
        itj = new InitTargetJob();
        tableMap = new HashMap();
        target = itj.getTarget(path);
        targetlength = itj.gettargetlength();
        table = new TargetTable(new MyTargetTableModel());
        initTableMap();
        //init manager
        if(!manager.isEmpty())
            manager.clear();
        table.setBackground(new Color(254, 254, 254));
        table.setForeground(new Color(0, 0, 0));
        table.setDefaultRenderer(Object.class, new MyCellRenderer());
        TableRowSorter<TableModel> sorter = new TableRowSorter<>(table.getModel());
        table.setRowSorter(sorter);
        sorter.setSortable(0, false);
        sorter.setSortable(1, false);
        sorter.setSortable(2, false);
        sorter.setSortable(5, false);
        sorter.addRowSorterListener(new RowSorterListener() {
            //refresh the ui 
            @Override
            public void sorterChanged(RowSorterEvent e) {
                table.updateUI();
            }
        });
        
        table.getTableHeader().setDefaultRenderer(new FathertableHeadrenderForTarget(table));
        table.addMouseListener(new fatherTableLink());
        table.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                int row = table.rowAtPoint(e.getPoint());
                int col = table.columnAtPoint(e.getPoint());
                if (row > -1 && col > -1) {
                    Object value = (String) table.getValueAt(row, col);
                    if (null != value && !"".equals(value)) {
                        table.setToolTipText(value.toString());//悬浮显示单元格内容   
                        if (col == 1) {
                            table.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                        } else {
                            table.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                        }
                    } else {
                        table.setToolTipText(null);//关闭提示
                    }
                }
            }
        });
        return table;
    }
    

    class fatherTableLink extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            int row = table.rowAtPoint(e.getPoint());
            int column = table.columnAtPoint(e.getPoint());
            if (column == 1) {
                String value = (String) table.getValueAt(row, column);
                String[] v = value.split(":");
                String fv = v[1].trim();
                String url = "http://www.uniprot.org/uniprot/" + fv;
                Desktop desktop = Desktop.getDesktop();
                try {
                    desktop.browse(new URI(url));
                } catch (URISyntaxException | IOException ex) {
                    Logger.getLogger(InitTargetTable.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

    class targetTableLink extends MouseAdapter {
      private final JTable jTable;
      public targetTableLink(JTable jTable){
          this.jTable = jTable;
      }  

        @Override
        public void mouseClicked(MouseEvent e) {
            int row = jTable.rowAtPoint(e.getPoint());
            int column = jTable.columnAtPoint(e.getPoint());
            String id = (String) jTable.getValueAt(row, 0); // the data key id
            if (column == 2) {
                String url = "";
                String dbType = (String)jTable.getValueAt(row, 2);
                if(dbType.contains("BindingDB"))
                    url = "http://www.bindingdb.org/bind/chemsearch/marvin/MolStructure.jsp?monomerid=";
                else if(dbType.contains("DrugBank"))
                    url = "http://www.drugbank.ca/drugs/";
                else if(dbType.contains("ChEMBL"))
                    url = "https://www.ebi.ac.uk/chembldb/compound/inspect/";
                String value = itj.getsrcid(path, keyID, id);
                System.out.println("TYPE: " + jTable.getValueAt(row, 2));
                System.out.println("URL: " + url);
                url +=  value;
                Desktop desktop = Desktop.getDesktop();
                try {
                    desktop.browse(new URI(url));
                } catch (URISyntaxException | IOException ex) {
                    Logger.getLogger(InitTargetTable.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else if (column == 3 && ((String) jTable.getValueAt(row, 3)).replace("<html><body><a href> ", "").trim().equals("Model of Action")) {
                String effect = itj.geteffectinfor(path, keyID, id);
                int x = e.getXOnScreen();
                int y = e.getYOnScreen();
                effect = "<html><body>" + effect + "</body></html>";
                new TargetEffectInfor(effect, x, y).setVisible(true);
            }

        }
    }

    class MyTargetTableModel extends AbstractTableModel implements TargetTableModel {

        @Override
        public boolean isRowExpandable(int rowIndex) {
            return true;
        }

        public TableModel getSubModel(int rowIndex) {
            return null;
        }

        @Override
        public int getColumnCount() {
            return 6;
        }

        @Override
        public int getRowCount() {
            return targetlength;
        }

        @Override
        public String getColumnName(int column) {
            String result = "";
            switch (column) {
                case 0:
                    result = "Target Name";
                    break;
                case 1:
                    result = "Ref. Link";
                    break;
                case 2:
                    result = "Species";
                    break;
                case 3:
                    result = "Simi. Score";
                    break;
                case 4:
                    result = "Score";
                    break;
                case 5:
                    result = "Rank";

            }
            return result;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            if (columnIndex == 1) {
                String link = "<html><body><a href>" + target[rowIndex][columnIndex];// + "</a></body></html>"; //color='#8E236B'
                return link;
            } else {
                return target[rowIndex][columnIndex];
            }
        }

        @Override
        public JComponent getSubComponent(int rowIndex) {
            final JTable jTable = getChildTable(rowIndex, null);
            jTable.addMouseMotionListener(new MouseAdapter() {
                int row;
                int col;
                Object value;
                @Override
                public void mouseMoved(MouseEvent e) {
                    row = jTable.rowAtPoint(e.getPoint());
                    col = jTable.columnAtPoint(e.getPoint());
                    if (row > -1 && col > -1) {
                        value = jTable.getValueAt(row, col);
                        if (null != value && !"".equals(value)) {
                            jTable.setToolTipText(value.toString());//悬浮显示单元格内容   
                            if (col == 2 || (col == 3 && ((String) jTable.getValueAt(row, 3)).replace("<html><body><a href> ", "").trim().equals("Model of Action"))) {
                                jTable.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                            } else {
                                jTable.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                            }
                        }
                    } else {
                        jTable.setToolTipText(null);//关闭提示
                    }
                }
            });
            jTable.addMouseListener(new MutiMolDisplay(showMolPath, jTable, jmolPanel, hasShow, "TargetInfor", filePath, keyID));
            JPanel p = new JPanel(new BorderLayout());
            p.add(jTable);//, BorderLayout.CENTER
            p.add(jTable.getTableHeader(), BorderLayout.NORTH);
            JPanel pp = new JPanel(new GridLayout());
            pp.add(p);
            return pp;
        }

    }

    /**
     * get child table when expand the father table
     * @param rowIndex
     * @param jTable
     * @return 
     */
    public JTable getChildTable(int rowIndex, JTable jTable){
            JTable childTable = new JTable();
            Vector data;
            itj = new InitTargetJob();
            if(jTable == null){
                //used for expand child table
                keyID = (String) tableMap.get(rowIndex);
            }else{
                //used when export the child table
                keyID = (String) jTable.getValueAt(rowIndex, 0);
            }
            data = manager.getData(keyID);
            if(data == null){
                data = itj.getTargetdata(path, keyID);
                manager.addTable(keyID, data);
            }
            childTable.setBackground(new Color(0, 255, 255));
            childTable.setForeground(new Color(0, 0, 0));
            childTable.setRowHeight(20);
            childTable.setDefaultRenderer(String.class, new MyCellRenderer());
            CheckTableModelForTarget tableModel = new CheckTableModelForTarget(data, headerName);
            childTable.setModel(tableModel);
            childTable.addMouseListener(new targetTableLink(childTable));
            childTable.getTableHeader().setDefaultRenderer(new HeaderCellRenderForTarget(childTable));
            return childTable;
    }    

    public void setpara(JmolPanel jmolPanel, String showMolPath, Hashtable<String, Integer> hasShow, String filePath) {
        this.showMolPath = showMolPath;
        this.jmolPanel = jmolPanel;
        this.hasShow = hasShow;
        this.filePath = filePath;
    }

    public static String path;//the xml path (job config)           = "E:\\Master\\Test\\99ZXL0GSIDResult.xml";
    private String filePath;
    private String keyID; // node value or name
    private InitTargetJob itj;
    private Map tableMap;
    private String[][] target;
    private int targetlength;
    private TargetTable table;
    private final Vector headerName;
    private Hashtable<String, Integer> hasShow;
    private String showMolPath;
    private JmolPanel jmolPanel;
    
    private TargetManager manager = TargetManager.TARGET_MANAGER;
}
