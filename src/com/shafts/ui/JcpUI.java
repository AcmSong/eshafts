package com.shafts.ui;

import com.shafts.module.JCPPanel;
import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JDialog;

import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class JcpUI extends JDialog {

    private static final long serialVersionUID = 1L;
    private final JPanel contentPane;

    /**
     * Launch the application.
     *
     * @param args
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                JcpUI frame = new JcpUI();
                frame.setVisible(true);
            }
        });
    }

    /**
     * Create the frame.
     */
    public JcpUI() {

        setBounds(100, 100, 680, 500);//800��600
        setTitle(get2DFileName());
        contentPane = new JPanel();
        //contentPane.setBackground(new Color(0, 51, 51));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);
        JCPPanel jcpPanel = new JCPPanel();
        contentPane.add(jcpPanel);
        setModal(true);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    }

    /**
     * return 2D molecule name
     * @return
     */
    private static String get2DFileName() {
        //return "molFile-" +  new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date()) + ".mol";
        return "file_" +  new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + ".mol";
    }
    
    private static String get2DPath(){
        return System.getProperty("user.dir") + "\\files\\MOL\\" + get2DFileName();
    }
    
    public static final String DD_FILE_NAME = get2DFileName();
    public static final String DD_FILE_PATH = get2DPath();
}
