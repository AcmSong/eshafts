package com.shafts.ui;

import com.shafts.bridge.ServerGate;
import com.shafts.render.TipPanel;
import com.shafts.utils.HyperLink;
import com.socket.bean.InforBean;
import com.shafts.utils.PatchDoor;
import java.io.File;
import java.util.Vector;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Little-Kitty
 */
public class EnterKeyUI extends javax.swing.JDialog {

    private JPanel jobPanel;
    private JPanel netJobPanel;

    /**
     * Creates new form EnterKeyUI
     *
     * @param isOrder
     * @param userName
     * @param jobPanel
     * @param netJobPanel
     */
    public EnterKeyUI(boolean isOrder, String userName, JPanel jobPanel, JPanel netJobPanel) {
        setModal(true);
        this.isLock = !isOrder;
        this.userName = userName;
        this.jobPanel = jobPanel;
        this.netJobPanel = netJobPanel;
        initAccount();
        initComponents();
        setLocationRelativeTo(null);
    }

    /**
     * get company infor
     */
    private void initAccount() {
        serGate = new ServerGate();
        accountVec = new Vector();
        phoneVec = new Vector();
        mailVec = new Vector();
        String price = null;
        //没有用处，因为数据库里的数据都时空的，暂时注释掉 Song 2017.10.20
//        InforBean inforBean = (InforBean) serGate.getProInfor();
//        if (inforBean != null) {
//            this.accountVec = inforBean.getAccountVec();
//            this.phoneVec = inforBean.getPhoneVec();
//            this.mailVec = inforBean.getMailVec();
//            price = inforBean.getPrice();
//        }
   
        StringBuilder bankers = new StringBuilder();
        StringBuilder bankAddress = new StringBuilder();
        StringBuilder accountNo = new StringBuilder();
//        if (accountVec != null) {
//            for (int i = 0; i < accountVec.size(); i++) {
//                Vector account = (Vector) accountVec.get(i);
//                bankers.append(account.get(0)).append("&nbsp;&nbsp;&nbsp;&nbsp;");
//                bankAddress.append(account.get(1)).append("&nbsp;&nbsp;&nbsp;&nbsp;");
//                accountNo.append(account.get(2)).append("&nbsp;&nbsp;&nbsp;&nbsp;");
//                if (i % 2 == 0) {
//                    bankers.append("<br>&nbsp;&nbsp;&nbsp;&nbsp;");
//                    bankAddress.append("<br>&nbsp;&nbsp;&nbsp;&nbsp;");
//                    accountNo.append("<br>&nbsp;&nbsp;&nbsp;&nbsp;");
//                }
//            }
//        }
        //get contacts infor
//        String phoInfor = "";
//        // if (!phoneVec.isEmpty()) {
//        if (phoneVec != null) {
//            for (int i = 0; i < phoneVec.size(); i++) {
//                Vector phone = (Vector) phoneVec.get(i);
//                phoInfor += phone.get(0) + " :  " + phone.get(1);
//                if ((i + 1) % 2 == 0) {
//                    phoInfor += "<br/>\n";
//                }
//            }
//        }
        //get email
//        String mailInfor = "";
//        // if (!mailVec.isEmpty()) {
//        if (mailVec != null) {
//            for (int i = 0; i < mailVec.size(); i++) {
//                Vector mail = (Vector) mailVec.get(i);
//                mailInfor += mail.get(0) + " : " + mail.get(1);
//                if ((i + 1) % 2 == 0) {
//                    mailInfor += "<br/>\n";
//                }
//            }
//        }
        String path = System.getProperty("user.dir") + "\\";
        infor = "<html>"
                + "  <head>"
                + "  </head>"
                + "  <body style=\"text-align:justify;\">"
                + "<center><h2>Try eSHAFTS for Free</h2></center>"
                + "<p>"
                + "<font face=\"微软雅黑\" size=\"4\">"
                + "<strong>How do I register a trial?</strong><br>"
                + "Registering your eSHAFTS trial is easy and it's free. Simply click on "
                + "Create new account to setup your own account and then contact us "
                + "by phone or email. As soon as you register your eSHAFTS trial, we'll "
                + "give you 365 days of product support.<br><br>"
                + "<center><image src=\"file:///"
                + path
                + "Pictures\\SysImage\\forTrial.png\"/></center>"
                + "</font><br></p>"
                + "<center><h2>Ordering Notes</h2></center>"
                + "<p>"
                + "<font face=\"微软雅黑\" size=\"4\"><strong>Certificate File</strong>"
                + "<br>"
                + "To run the remote computation module you will need to obtain a certificate file"
                + " for eSHAFTS. The host merely establishes trust in a certificate issuer who needs "
                + "to be authenticated as a prerequisite to access."
                + "</font><br></p>"
                + "<p>"
                + "<font face=\"微软雅黑\" size=\"4\">"
                + "Typically, certificates contain the following information:</font></p>"
                //+ "<p>"
                + "<ul>"
                + "<li>"
                + "<p style=\"text-align:justify; text-justify:inter-ideograph;\">"
                + " The subject's identifier information, such as name and the e-mail address."
                + "</p></li>"
                + "<li>"
                + "<p style=\"text-align:justify; text-justify:inter-ideograph;\">"
                + "The device or service information."
                + "</p></li>"
                + "</ul>"
                //+ "</p>"
                //+ "<p>
                + "<font face=\"微软雅黑\" size=\"4\">"
                + "A certificate is valid only for the period of time "
                + "specified within it. Once a certificate’s validity "
                + "period has passed, you can renew your subscription "
                + "and do not bother to reinstall the key file. "
                + "</font>"
                //+ "</p>"
                + "<p>"
                + "<font face=\"微软雅黑\" size=\"4\"><strong>Payment Terms</strong>"
                + " <br>"
                + "To get a certificate file for this machine, customers should make the "
                + "payment to our bank account along with the detailed personal information. "
                + "After confirming the receival of your payment, we'll send you the key file "
                + "that you can install to activate the remote computation module on this machine.<br>"
                + "<strong>Please ensure to state your username on your payment transaction or to enclose a "
                + "copy of the invoice. Failure may result in a delay to your service. Thank you.</strong><br> "
              //  + "Price: ￥" + price + " / 3 months"
                + "</font></p>"
                + "<p>"
                + "<font face=\"微软雅黑\" size=\"4\"><strong>Payment by bank transfer</strong>"
                + "<br>Bankers: " + bankers.toString()
                + "<br>Bank Address: " + bankAddress.toString()
                + "<br>Account No: " + accountNo.toString()
                + "</font></p>"
                + "<p><font face=\"微软雅黑\" size=\"4\">"
                + "If you have any queries regarding our payment methods, please do not "
                + "hesitate to contact us:</font></p>"
                + "<p><font face=\"微软雅黑\" size=\"4\">"
                + "<strong>Contacts:</strong>  (86)21-64250213&nbsp;&nbsp;&nbsp; (86)21-64253506-8011<br>"
                + "<strong>E-mail:&nbsp;&nbsp;&nbsp;</strong> "
                + "<a href=\"mailto:hlli@ecust.edu.cn\">hlli@ecust.edu.cn</a>"
                + "&nbsp;&nbsp;&nbsp; "
                + "<a href=\"mailto:hegaoqi@ecust.edu.cn\">hegaoqi@ecust.edu.cn</a>"
                + "</font></p></body>"
                + "</html>";
    }

    /**
     * Creates new form EnterKeyUI
     */
    public EnterKeyUI() {
        //super(parent, modal);
        initAccount();
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        tipPanel = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane2 = new javax.swing.JTextPane();
        jButton3 = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jPanel2 = new javax.swing.JPanel();

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Install key"));

        jLabel1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel1.setText("Key File:");

        jTextField1.setEditable(false);
        jTextField1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N

        jButton1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jButton1.setText("Browse");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jButton2.setText("Install");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton4.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jButton4.setText("Cancel");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 102, 255));
        jLabel2.setText("Lost the key?");
        jLabel2.setToolTipText("We will send the key file to your mailbox if you have purchase the product.");
        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel2MouseExited(evt);
            }
        });

        tipPanel.setLayout(new java.awt.GridLayout(1, 0));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(tipPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton1)
                            .addComponent(jButton4))
                        .addGap(0, 18, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tipPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Install key"));

        jTextPane2.setEditable(false);
        jTextPane2.setFont(new java.awt.Font("宋体", 0, 14)); // NOI18N
        jTextPane2.setForeground(new java.awt.Color(0, 0, 204));
        jTextPane2.setText("  You have already installed the key file, and don't need to do it again. If you want to renew for the remote computation module, read the \"Order Notes\" above to know more about how to renew it.");
        jScrollPane2.setViewportView(jTextPane2);

        jButton3.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jButton3.setText("OK");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(433, Short.MAX_VALUE)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        if(isLock)
        jPanel2.add(jPanel3);
        else

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("ACTIVE");
        setResizable(false);

        jScrollPane1.setBorder(javax.swing.BorderFactory.createTitledBorder("Order Notes"));

        jTextPane1.setEditable(false);
        jTextPane1.setContentType("text/html"); // NOI18N
        jTextPane1.setText(infor);
        jTextPane1.setCaretPosition(0);

        jTextPane1.addHyperlinkListener(new HyperLink());
        jScrollPane1.setViewportView(jTextPane1);

        jPanel2.setLayout(new java.awt.GridLayout(1, 0));
        if(isLock)
        jPanel2.add(jPanel3);
        else
        jPanel2.add(jPanel4);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 194, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseExited
        // TODO add your handling code here:
        jLabel2.setForeground(new java.awt.Color(0, 102, 255));
    }//GEN-LAST:event_jLabel2MouseExited

    private void jLabel2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseEntered
        // TODO add your handling code here:
        jLabel2.setForeground(new java.awt.Color(255, 102, 0));
    }//GEN-LAST:event_jLabel2MouseEntered

    //reget the keyfile
    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        // TODO add your handling code here:
        new ReGetKeyUI(userName).setVisible(true);

    }//GEN-LAST:event_jLabel2MouseClicked

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton4ActionPerformed

    //install the key
    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        //TipsRender tipRender = new TipsRender();
        String keyPath = jTextField1.getText();
        if (!keyPath.equals("")) {
//            tipPanel.removeAll();
//            tipPanel.add(new TipPanel("Installing...", "Error"));
            boolean isOk = new PatchDoor().install(keyPath, jobPanel, netJobPanel);
            if (isOk) {
                dispose();
            }
        } else {
            tipPanel.removeAll();
            tipPanel.add(new TipPanel("Choose a key file!", "Error"));
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    //CHOOSE the key file
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        JFileChooser jfc = new JFileChooser();
        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        jfc.setAcceptAllFileFilterUsed(false);
        jfc.addChoosableFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return (f.getName().endsWith("pfx") || f.isDirectory());
            }

            @Override
            public String getDescription() {
                // TODO Auto-generated method stub
                return "Files(*.pfx)";
            }
        });
        int result = jfc.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            String keyFilePath = jfc.getSelectedFile().getAbsolutePath();
            jTextField1.setText(keyFilePath);
            jTextField1.setToolTipText(keyFilePath);
        }

    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        dispose();
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EnterKeyUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EnterKeyUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EnterKeyUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EnterKeyUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                // EnterKeyUI dialog = new EnterKeyUI(new javax.swing.JFrame(), true);
                EnterKeyUI dialog = new EnterKeyUI();
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    private boolean isLock;
    private String userName;
    private String infor;
    private Vector accountVec;
    private Vector phoneVec;
    private Vector mailVec;
    private ServerGate serGate;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTextPane jTextPane2;
    private javax.swing.JPanel tipPanel;
    // End of variables declaration//GEN-END:variables
}
