package com.shafts.ui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 *
 * @author Little-Kitty
 */
public class HelpDoc extends javax.swing.JFrame {

    /**
     * Creates new form HelpDoc
     */
    private String html;

    public HelpDoc() {       
        setTitle("帮助文档");
        
        html = initHtml();
        //initBrowser();
        initComponents();
        addListener();
        setLocationRelativeTo(null);
       
    }
    
    private void addListener(){
        jEditorPane1.addHyperlinkListener(new HyperlinkListener() {

            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                
            }
        });
    }

    public HelpDoc(int i) {
        initHtml();
        initComponents();
        jEditorPane1.setCaretPosition(i);
        setLocationRelativeTo(null);
    }

//    private void initBrowser() {
//        //NativeInterface.open();
//        String url = HelpDoc.class.getClassLoader().getResource("//").getPath() + "/Introduction.html";
//        webBrowser = new JWebBrowser();
//        webBrowser.navigate(url);
//        webBrowser.setButtonBarVisible(false);
//        webBrowser.setMenuBarVisible(false);
//        webBrowser.setBarsVisible(false);
//        webBrowser.setStatusBarVisible(false);        
//    }

    private String initHtml() {
        helpDoc = HelpDoc.class.getClassLoader().getResource("Introduction.html");
        StringBuilder builder = new StringBuilder();
        try {
            FileReader reader = new FileReader(new File(HelpDoc.class.getClassLoader().getResource("//").getPath() + "/Introduction.html"));
            BufferedReader br = new BufferedReader(reader);
            String line;
            while ((line = br.readLine()) != null) {
                builder.append(line);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(HelpDoc.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(HelpDoc.class.getName()).log(Level.SEVERE, null, ex);
        }
        return builder.toString();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        webPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        webPanel.setLayout(new java.awt.GridLayout(1, 0));

        try{
            jEditorPane1.setPage(helpDoc);
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
        jScrollPane1.setViewportView(jEditorPane1);

        webPanel.add(jScrollPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(webPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(webPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 500, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(HelpDoc.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(HelpDoc.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(HelpDoc.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(HelpDoc.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new HelpDoc().setVisible(true);
            }
        });
    }

    
    private URL helpDoc;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel webPanel;
    // End of variables declaration//GEN-END:variables
}
