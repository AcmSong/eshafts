package com.shafts.ui;

/**
 *
 * @author Little-Kitty
 */
public class LogWriter extends javax.swing.JPanel {

    /**
     * Creates new form LogShow
     */
    public LogWriter() {
        initComponents();
    }
    
    /**
     * UI
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        log = new javax.swing.JTextArea();

        setLayout(new java.awt.GridLayout());

        log.setEditable(false);
        log.setColumns(20);
        log.setRows(5);
        jScrollPane1.setViewportView(log);

        add(jScrollPane1);
    }// </editor-fold>//GEN-END:initComponents

    public void write(String text){
        log.append(text + "\n");
        log.setCaretPosition(log.getText().length());
        updateUI();
    }
    
    public void clear(){
        this.removeAll();;
        log = new javax.swing.JTextArea();
        log.setEditable(false);
        log.setColumns(20);
        log.setRows(5);
        jScrollPane1.setViewportView(log);

        add(jScrollPane1);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea log;
    // End of variables declaration//GEN-END:variables
}
