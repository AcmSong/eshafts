package com.shafts.ui;

import com.shafts.compute.Job;
import com.shafts.compute.JobStatusPool;
import com.shafts.compute.JobThreadPool;
import com.shafts.compute.LogTableHeader;
import com.shafts.compute.Worker;
import com.shafts.module.jobmanager.LocalManager;
import com.shafts.render.MyCellRenderer;
import java.awt.event.MouseEvent;
import java.util.Iterator;
import java.util.concurrent.ThreadPoolExecutor;
import javax.swing.JPanel;
import javax.swing.JTable;

/**
 *
 * @author Little-Kitty
 */
public class JobStatusUI extends javax.swing.JDialog {

    private final JobStatusPool jobData;

    /**
     * Creates new form JobStatusUI
     */
    private JobStatusUI() {
        setSize(400, 300);
        setModal(true);
        initComponents();
        jobData = new JobStatusPool();
        //initRunningPanel();
    }

    public void show(int x, int y) {
        setLocation(x - 350, y - 220);
        initAll();
        setVisible(true);
    }

    public void initAll() {
        initRunningPanel();
        initWaitingPanel();
        initDonePanel();
    }

    private synchronized void initRunningPanel() {
        JTable table = jobData.runJobTable();
        runningPanel.removeAll();
                runningPanel.add(defaultShow);
        if (table != null) {
            table.addMouseListener(new logDetail(table, "run"));
            table.setDefaultRenderer(Object.class, new MyCellRenderer());
            table.getTableHeader().setDefaultRenderer(new LogTableHeader(table));
            table.setCellSelectionEnabled(true);
            runScrollPane = new javax.swing.JScrollPane();
            runScrollPane.setBorder(null);
            runScrollPane.setViewportView(table);
            runningPanel.removeAll();
            runningPanel.add(runScrollPane);
        }
        runningPanel.updateUI();
    }

    private synchronized void initWaitingPanel() {
        JTable table = jobData.waitQueueTable();
        jobQueuePanel.removeAll();
                jobQueuePanel.add(defaultShow1);
        if (table != null) {
            table.addMouseListener(new logDetail(table, "wait"));
            table.setDefaultRenderer(Object.class, new MyCellRenderer());
            table.getTableHeader().setDefaultRenderer(new LogTableHeader(table));
            table.setCellSelectionEnabled(true);
            waitScrollPane = new javax.swing.JScrollPane();
            waitScrollPane.setBorder(null);
            waitScrollPane.setViewportView(table);
            jobQueuePanel.removeAll();
            jobQueuePanel.add(waitScrollPane);
        }
        jobQueuePanel.updateUI();
    }

    private synchronized void initDonePanel() {
        JTable table = jobData.doneTable();
        completePanel.removeAll();
        completePanel.add(defaultShow2);
        if (table != null) {
            table.setDefaultRenderer(Object.class, new MyCellRenderer());
            table.getTableHeader().setDefaultRenderer(new LogTableHeader(table));
            table.setCellSelectionEnabled(true);
            table.addMouseListener(new infoShow(table));
            doneScrollPane = new javax.swing.JScrollPane();
            doneScrollPane.setBorder(null);
            doneScrollPane.setViewportView(table);
            completePanel.removeAll();
            completePanel.add(doneScrollPane);
        }
        completePanel.updateUI();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        defaultShow = new javax.swing.JLabel();
        defaultShow1 = new javax.swing.JLabel();
        defaultShow2 = new javax.swing.JLabel();
        runScrollPane = new javax.swing.JScrollPane();
        waitScrollPane = new javax.swing.JScrollPane();
        doneScrollPane = new javax.swing.JScrollPane();
        completePanel2 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        runningPanel = new javax.swing.JPanel();
        jobQueuePanel = new javax.swing.JPanel();
        completePanel = new javax.swing.JPanel();
        completePanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();

        defaultShow.setFont(new java.awt.Font("微软雅黑", 0, 18)); // NOI18N
        defaultShow.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        defaultShow.setText("No Running Job");

        defaultShow1.setFont(new java.awt.Font("微软雅黑", 0, 18)); // NOI18N
        defaultShow1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        defaultShow1.setText("No Waiting Job");

        defaultShow2.setFont(new java.awt.Font("微软雅黑", 0, 18)); // NOI18N
        defaultShow2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        defaultShow2.setText("No Complete Job");

        runScrollPane.setBorder(null);

        waitScrollPane.setBorder(null);

        doneScrollPane.setBorder(null);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setUndecorated(true);

        completePanel2.setBackground(new java.awt.Color(255, 234, 185));
        completePanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jTabbedPane1.setBackground(new java.awt.Color(255, 234, 185));
        jTabbedPane1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jTabbedPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTabbedPane1MouseClicked(evt);
            }
        });
        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        runningPanel.setBackground(new java.awt.Color(255, 234, 185));
        runningPanel.setLayout(new java.awt.GridLayout(1, 0));
        //runningPanel.add(defaultShow);
        jTabbedPane1.addTab("RunningJob", runningPanel);

        jobQueuePanel.setBackground(new java.awt.Color(255, 234, 185));
        jobQueuePanel.setLayout(new java.awt.GridLayout(1, 0));
        //jobQueuePanel.add(defaultShow1);
        jTabbedPane1.addTab("WaitingQueue", jobQueuePanel);

        completePanel.setBackground(new java.awt.Color(255, 234, 185));
        completePanel.setLayout(new java.awt.GridLayout(1, 0));
        //completePanel.add(defaultShow2);
        jTabbedPane1.addTab("Complete", completePanel);

        completePanel1.setBackground(new java.awt.Color(255, 234, 185));

        jLabel1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 0, 153));
        jLabel1.setText("close");
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel1MouseExited(evt);
            }
        });

        //completePanel.add(defaultShow2);

        javax.swing.GroupLayout completePanel1Layout = new javax.swing.GroupLayout(completePanel1);
        completePanel1.setLayout(completePanel1Layout);
        completePanel1Layout.setHorizontalGroup(
            completePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, completePanel1Layout.createSequentialGroup()
                .addContainerGap(355, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(15, 15, 15))
        );
        completePanel1Layout.setVerticalGroup(
            completePanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1)
        );

        //completePanel.add(defaultShow2);

        javax.swing.GroupLayout completePanel2Layout = new javax.swing.GroupLayout(completePanel2);
        completePanel2.setLayout(completePanel2Layout);
        completePanel2Layout.setHorizontalGroup(
            completePanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(completePanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(completePanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(completePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, 0))
        );
        completePanel2Layout.setVerticalGroup(
            completePanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, completePanel2Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(completePanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 404, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(completePanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 203, Short.MAX_VALUE)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(completePanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    class logDetail extends java.awt.event.MouseAdapter {

        private JTable table;
        private final String type; //running or waiting

        public logDetail(JTable table, String type) {
            this.table = table;
            this.type = type;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            int row = table.rowAtPoint(e.getPoint());
            int col = table.columnAtPoint(e.getPoint());
            System.out.println(row + " ----table---- " + col);
            if (col == 2) {
                if ("run".equals(type)) {
                    Job job = getSelectedJob((String) table.getValueAt(row, 0));
                    if (job != null) {
                        JPanel log = job.getLog();
                        LogShower logShower = new LogShower();
                        logShower.show(log, e.getXOnScreen(), e.getYOnScreen());
                        //logShower.setVisible(true);
                    }
                } else {
                    Worker worker = getSelectedWorker((String) table.getValueAt(row, 0));
                    jobPool.getQueue().remove(worker);
                    table = jobData.waitQueueTable();
                    if (table != null) {
                        waitScrollPane.removeAll();
                        runScrollPane.setViewportView(table);
                        jobQueuePanel.removeAll();
                        jobQueuePanel.add(runScrollPane);
                    } else {
                        completePanel.removeAll();
                        completePanel.add(defaultShow1);
                    }

                }
            }
        }
    }

    class infoShow extends java.awt.event.MouseAdapter {

        private JTable table;

        public infoShow(JTable table) {
            this.table = table;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            int row = table.rowAtPoint(e.getPoint());
            int col = table.columnAtPoint(e.getPoint());
            if (col == 1) {// show detail
                Job job = getSelectedJob((String) table.getValueAt(row, 0));
                if (job != null) {
                    JPanel log = job.getLog();
                    LogShower logShower = new LogShower();
                    logShower.show(log, e.getXOnScreen(), e.getYOnScreen());
                    //logShower.setVisible(true);
                }
            } else if (col == 2) {//delete
                Worker worker = getSelectedWorker((String) table.getValueAt(row, 0));
                jobs.remove(worker);
                table = jobData.doneTable();
                if (table != null) {
                    doneScrollPane = new javax.swing.JScrollPane();
                    doneScrollPane.setBorder(null);
                    doneScrollPane.setViewportView(table);
                    completePanel.removeAll();
                    completePanel.add(doneScrollPane);
                } else {
                    completePanel.removeAll();
                    completePanel.add(defaultShow2);
                }
                completePanel.updateUI();
            }
        }
    }

    private void jLabel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseEntered
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
    }//GEN-LAST:event_jLabel1MouseEntered

    private void jLabel1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseExited
        jLabel1.setForeground(new java.awt.Color(153, 0, 153));
    }//GEN-LAST:event_jLabel1MouseExited

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        setVisible(false);
    }//GEN-LAST:event_jLabel1MouseClicked

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void jTabbedPane1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTabbedPane1MouseClicked
        int selectIndex = jTabbedPane1.getSelectedIndex();
        switch (selectIndex) {
            case 0:
                initRunningPanel();
                break;
            case 1:
                initWaitingPanel();
                break;
            case 2:
                initDonePanel();
                break;
        }
    }//GEN-LAST:event_jTabbedPane1MouseClicked

    private Job getSelectedJob(String jobId) {
        Iterator it = jobs.keySet().iterator();
        while (it.hasNext()) {
            Worker worker = (Worker) it.next();
            if (jobId.equals(worker.getJob().getJobId())) {
                return worker.getJob();
            }
        }
        return null;
    }

    private Worker getSelectedWorker(String jobId) {
        Iterator it = jobs.keySet().iterator();
        while (it.hasNext()) {
            Worker worker = (Worker) it.next();
            if (jobId.equals(worker.getJob().getJobId())) {
                return worker;
            }
        }
        return null;
    }

    public static JobStatusUI getFrame() {
        if (frame == null) {
            frame = new JobStatusUI();
        }
        return frame;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JobStatusUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JobStatusUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JobStatusUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JobStatusUI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {

                JobStatusUI ui = JobStatusUI.getFrame();
                ui.show(400, 300);
                //ui.initAll();
            }
        });
    }

    private final LocalManager jobs = LocalManager.localJob();
    private final ThreadPoolExecutor jobPool = JobThreadPool.threadPool();
    private static JobStatusUI frame = null;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel completePanel;
    private javax.swing.JPanel completePanel1;
    private javax.swing.JPanel completePanel2;
    private javax.swing.JLabel defaultShow;
    private javax.swing.JLabel defaultShow1;
    private javax.swing.JLabel defaultShow2;
    private javax.swing.JScrollPane doneScrollPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JPanel jobQueuePanel;
    private javax.swing.JScrollPane runScrollPane;
    private javax.swing.JPanel runningPanel;
    private javax.swing.JScrollPane waitScrollPane;
    // End of variables declaration//GEN-END:variables
}
