package com.shafts.ui;

import com.result.view.SpiderWebPlot;
import com.shafts.module.JmolPanel;
import com.shafts.module.WaitingGif;
import com.shafts.module.cyndi.Cyndi;
import com.shafts.render.TipPanel;
import com.shafts.render.treerender.IconNode;
import com.shafts.utils.CharacterValidate;
import com.shafts.utils.HyperLink;
import com.shafts.utils.InfoFactory;
import com.shafts.utils.file.MyFileFilter;
import com.shafts.utils.decompress.PdbAnalysis;
import com.shafts.utils.decompress.PdbDecompress;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ItemEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;
import org.apache.commons.lang.StringUtils;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;

/**
 *
 * @author Little-Kitty
 */
public class MainUI extends javax.swing.JFrame {

    private final int s_WIDTH;
    private final int s_HEIGHT;
    private final int verSplit;    // ui split
    private final int verSplit1;    // ui split
    private final int horSplit;    // ui split
    private final int horSplit1;
    public static int HTML_WIDTH;
    private final Point point;

    /**
     * Creates new form MainUI
     */
    public MainUI() {
        setLocationRelativeTo(null);
        setIconImage(new ImageIcon(MainUI.class.getResource("/pic/icon1.png")).getImage());

        Dimension screensize = Toolkit.getDefaultToolkit().getScreenSize();
        s_WIDTH = (int) screensize.getWidth();
        s_HEIGHT = (int) screensize.getHeight();
        verSplit = (s_HEIGHT - 40) * 2 / 3 - 10;
        verSplit1 = verSplit * 3 / 4 + 10;
        horSplit = (s_WIDTH - 100) * 3 / 4 - 50;
        horSplit1 = horSplit * 1 / 4 - 20;
        HTML_WIDTH = s_WIDTH - 150 - horSplit;
        point = new Point();
        point.setLocation(0, 0);
//        softwareInfo = " <html>\n"
//                + " <font face=\"微软雅黑\" color=\"#0000FF\">"
//                + " <strong>"
//                + " &nbsp;Software name: eSHAFTS<br>\n"
//                + " &nbsp;Version:       1.0<br>\n"
//                + " &nbsp;Size:          81604 KB<br>\n"
//                + " &nbsp;Language:      EN<br>\n"
//                + " &nbsp;Environments:      Win7/8/8.1/10 <br>\n"
//                + " &nbsp;Team:     Wenhao Wei, Xia Wang, Gaoqi He, "
//                + "<a href=\"http://lilab.ecust.edu.cn\" target=\"blank\">Honglin Li</a><br>"
////                + " <hr width=\"30%\"><br>\n"
////                + " &nbsp;If you have problems, please contact us:</strong></font>"
////                + " <font face=\"微软雅黑\" color=\"#FF8C00\">"
////                + "<strong><a href=\"mailto:lilab_ecust@163.com\">"
////                + " lilab_ecust@163.com</a><br>\n"
////                + " </strong>"
////                + " </font>"
////                + " <center><strong>"
////                + " <font face=\"微软雅黑\" color=\"#0000FF\">"
////                + " ©2013-2015 </font>"
////                + " <font face=\"微软雅黑\" color=\"#FF8C00\">"
////                + "<a href=\"http://sklndd.ecust.edu.cn\" target=\"blank\">"
////                + "Shanghai Key laboratory of New Drug Design</a>, "
////                + " <a href=\"http://www.ecust.edu.cn/s/2/t/31/main.htm\" target=\"blank\">"
////                + " East China University Of Science And Technology. </a></font>"
////                + " <font face=\"微软雅黑\" color=\"#0000FF\">"
////                + "All Rights Reserved.</font> "
////                + " </strong></center>"                
//                + " </html>";
// Song 2018.6.3论文需要暂时改动版权时间，org2013-2015
        softwareInfo = "<html>"
                + "<body>"
                + "<font face=\"微软雅黑\" size=\"3\">"
                + "eSHAFTS V1.0 "
                + "Copyright© 2013-2018 "
                + "<a href=\"http://sklndd.ecust.edu.cn\" target=\"blank\">"
                + "Shanghai Key Laboratory of New Drug Design</a>, "
                + "East China University of Science & Technology<br>"
                + "Developers&nbsp;:&nbsp;<a href=\"mailto:lucifer_whw@163.com\">Wenhao Wei</a>,&nbsp;"
                + "<a href=\"mailto:wangxia20080210@yeah.net\">Xia Wang</a>,&nbsp;"
                + "<a href=\"mailto:xxffliu@gmail.com\">Xiaofeng Liu</a>,&nbsp;"
                + "<a href=\"mailto:hegaoqi@ecust.edu.cn\">Gaoqi He</a>,&nbsp;"
                + "<a href=\"mailto:hlli@ecust.edu.cn\">Honglin Li</a>,"
                + "<br>"
                + "E-mail: <a href=\"mailto:lilab_ecust@163.com\"> lilab_ecust@163.com</a>"
                + "<br>"
                + "Web: <a href=\"http://lilab.ecust.edu.cn\"> http://lilab.ecust.edu.cn</a>"
                + "<br>"
                + "Tel&nbsp;: &nbsp;(86)21-64250213&nbsp;&nbsp;&nbsp; (86)21-64253506-8011"
                + "<br>"
                + "If you use eSHAFTS, please cite&nbsp;:"
                + "<ul>"
                + "<li>"
                + "<p style=\"text-align:justify; text-justify:inter-ideograph;\">"
                + "<a href=\"http://pubs.acs.org/doi/abs/10.1021/ci200060s\""
                + "target=\"blank\">"
                + "Xiaofeng Liu et al. "
                + "<i>J. Chem. Inf. Model</i>. "
                + "<b>2011</b>, 51, 2372-2385."
                + "</a></p></li>"
                + "<li>"
                + "<p style=\"text-align:justify; text-justify:inter-ideograph;\">"
                + "<a href=\"http://pubs.acs.org/doi/abs/10.1021/jm200139j\""
                + " target=\"blank\">"
                + "Weiqiang Lu et al. "
                + "<i>J. Med. Chem.</i> <b>2011</b>, 54, 3564-3574."
                + "</a></p></li>"
                + "<li>"
                + "<p style=\"text-align:justify; text-justify:inter-ideograph;\">"
                + "<a href=\"http://bioinformatics.oxfordjournals.org/content/29/14/1827\""
                + " target=\"blank\">"
                + "Jiayu Gong et al. "
                + "<i>Bioinformatics</i>. <b>2013</b>, 29, 1827-1829."
                + "</a></p></li>"
                + "</ul></p>"
                + "</font>"
                + "</body>"
                + "</html>";
        cyndi = new Cyndi();
        initComponents();
        this.setSize(s_WIDTH - 150, s_HEIGHT - 40);
        setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jScrollPane4 = new javax.swing.JScrollPane();
        jRadioButton1 = new javax.swing.JRadioButton();
        jRadioButton2 = new javax.swing.JRadioButton();
        netJobLockPanel = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        netJobParaPanel = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jComboBox2 = new javax.swing.JComboBox();
        jRadioButton3 = new javax.swing.JRadioButton();
        jRadioButton4 = new javax.swing.JRadioButton();
        jComboBox4 = new javax.swing.JComboBox();
        jComboBox3 = new javax.swing.JComboBox();
        jLabel12 = new javax.swing.JLabel();
        jButton20 = new javax.swing.JButton();
        jButton19 = new javax.swing.JButton();
        PDBstru = new javax.swing.JPopupMenu();
        wireframe = new javax.swing.JMenuItem();
        cartoon = new javax.swing.JMenuItem();
        strands = new javax.swing.JMenuItem();
        MOLstru = new javax.swing.JPopupMenu();
        ball_stcikStyle = new javax.swing.JMenuItem();
        stickStyle = new javax.swing.JMenuItem();
        wireframeStyle = new javax.swing.JMenuItem();
        cpkStyle = new javax.swing.JMenuItem();
        jLabel8 = new javax.swing.JLabel();
        userPop = new javax.swing.JPopupMenu();
        checkInfo = new javax.swing.JMenuItem();
        logOut = new javax.swing.JMenuItem();
        jToolBar1 = new javax.swing.JToolBar();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton18 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton22 = new javax.swing.JButton();
        jButton25 = new javax.swing.JButton();
        jToolBar2 = new javax.swing.JToolBar();
        jButton17 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jSplitPane1 = new javax.swing.JSplitPane();
        TopPanel = new javax.swing.JPanel();
        jSplitPane3 = new javax.swing.JSplitPane();
        leftPanel = new javax.swing.JPanel();
        jSplitPane4 = new javax.swing.JSplitPane();
        molPanel = new javax.swing.JPanel();
        centerPanel = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        tipPanel1 = new javax.swing.JPanel();
        jToggleButton1 = new javax.swing.JToggleButton();
        jButton16 = new javax.swing.JButton();
        jButton21 = new javax.swing.JButton();
        jToggleButton2 = new javax.swing.JToggleButton();
        jButton23 = new javax.swing.JButton();
        westPanel = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        treeRoot = new IconNode("My Work");
        jTree1 = new javax.swing.JTree(treeRoot);
        centerPanelChild1 = new javax.swing.JPanel();
        jButton15 = new javax.swing.JButton();
        rightPanel = new javax.swing.JPanel();
        eastPanel = new javax.swing.JPanel();
        jSplitPane2 = new javax.swing.JSplitPane();
        eastTopPanel = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        tipUI =  new com.shafts.ui.MoreTipUI();
        localJobPanel = new javax.swing.JPanel();
        localjobParaPanel = new javax.swing.JPanel();
        jTextField3 = new javax.swing.JTextField();
        jCheckBox1 = new javax.swing.JCheckBox();
        jButton10 = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        netJobPanel = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jTextField4 = new javax.swing.JTextField();
        jButton13 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        jComboBox5 = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        jComboBox6 = new javax.swing.JComboBox();
        jButton14 = new javax.swing.JButton();
        jCheckBox2 = new javax.swing.JCheckBox();
        paraPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        tipPanel2 = new javax.swing.JPanel();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton24 = new javax.swing.JButton();
        eastBottomPanel = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();
        BottomPanel = new javax.swing.JPanel();
        southPanel = new javax.swing.JPanel();
        resultShowPanel = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jPanel1 = new javax.swing.JPanel();
        scanningPanel = new javax.swing.JPanel();
        jobStatus4 = new javax.swing.JLabel();
        scanningPanel1 = new javax.swing.JPanel();
        jobStatus3 = new javax.swing.JLabel();
        jobStatus = new javax.swing.JLabel();
        jobStatus1 = new javax.swing.JLabel();
        jobStatus2 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();
        jMenuItem12 = new javax.swing.JMenuItem();
        jMenuItem5 = new javax.swing.JMenuItem();
        jMenuItem13 = new javax.swing.JMenuItem();
        jMenuItem3 = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem4 = new javax.swing.JMenuItem();
        jMenuItem14 = new javax.swing.JMenuItem();
        jMenu3 = new javax.swing.JMenu();
        jMenu8 = new javax.swing.JMenu();
        jCheckBoxMenuItem1 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem2 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem3 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem4 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem5 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem6 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem7 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem8 = new javax.swing.JCheckBoxMenuItem();
        jCheckBoxMenuItem9 = new javax.swing.JCheckBoxMenuItem();
        jMenu6 = new javax.swing.JMenu();
        jMenuItem15 = new javax.swing.JMenuItem();
        jMenuItem16 = new javax.swing.JMenuItem();
        jMenuItem17 = new javax.swing.JMenuItem();
        jMenuItem18 = new javax.swing.JMenuItem();
        jMenuItem23 = new javax.swing.JMenuItem();
        jMenuItem24 = new javax.swing.JMenuItem();
        jMenu11 = new javax.swing.JMenu();
        jMenuItem19 = new javax.swing.JMenuItem();
        jMenuItem20 = new javax.swing.JMenuItem();
        jMenuItem21 = new javax.swing.JMenuItem();
        jMenu10 = new javax.swing.JMenu();
        jCheckBoxMenuItem20 = new javax.swing.JCheckBoxMenuItem();
        jMenuItem11 = new javax.swing.JMenuItem();
        jMenu9 = new javax.swing.JMenu();
        jRadioButtonMenuItem1 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem2 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem3 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem4 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem5 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem6 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem7 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem8 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem9 = new javax.swing.JRadioButtonMenuItem();
        jRadioButtonMenuItem10 = new javax.swing.JRadioButtonMenuItem();
        jMenu4 = new javax.swing.JMenu();
        jMenuItem7 = new javax.swing.JMenuItem();
        jMenuItem6 = new javax.swing.JMenuItem();
        jMenu5 = new javax.swing.JMenu();
        jMenuItem8 = new javax.swing.JMenuItem();
        jMenu7 = new javax.swing.JMenu();
        jMenuItem9 = new javax.swing.JMenuItem();
        jMenuItem10 = new javax.swing.JMenuItem();
        jMenu12 = new javax.swing.JMenu();
        jMenuItem22 = new javax.swing.JMenuItem();
        jMenuItem25 = new javax.swing.JMenuItem();
        jMenuItem26 = new javax.swing.JMenuItem();

        buttonGroup3.add(jRadioButtonMenuItem1);
        buttonGroup3.add(jRadioButtonMenuItem2);
        buttonGroup3.add(jRadioButtonMenuItem3);
        buttonGroup3.add(jRadioButtonMenuItem4);
        buttonGroup3.add(jRadioButtonMenuItem5);
        buttonGroup3.add(jRadioButtonMenuItem6);
        buttonGroup3.add(jRadioButtonMenuItem7);
        buttonGroup3.add(jRadioButtonMenuItem8);
        buttonGroup3.add(jRadioButtonMenuItem9);
        buttonGroup3.add(jRadioButtonMenuItem10);

        buttonGroup1.add(jRadioButton1);
        buttonGroup1.add(jRadioButton2);

        buttonGroup2.add(jRadioButton3);
        buttonGroup2.add(jRadioButton4);

        jScrollPane4.getViewport().setBackground(new java.awt.Color(0,51,51));
        //jScrollPane4.getViewport().setForeground(new java.awt.Color(0,51,51));
        jScrollPane4.setBorder(null);

        jRadioButton1.setBackground(new java.awt.Color(245, 245, 245));
        jRadioButton1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jRadioButton1.setSelected(true);
        jRadioButton1.setText("Local Model");
        jRadioButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton1ActionPerformed(evt);
            }
        });

        jRadioButton2.setBackground(new java.awt.Color(245, 245, 245));
        jRadioButton2.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jRadioButton2.setText("Network Model");
        jRadioButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton2ActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel9.setText("The service is prohibited!");

        jLabel10.setFont(new java.awt.Font("微软雅黑", 0, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(0, 0, 255));
        jLabel10.setText("<html><body><u>Active now?</u><body><html>");
        jLabel10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel10MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel10MouseExited(evt);
            }
        });

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/forbiddenIcon.png"))); // NOI18N

        jLabel13.setFont(new java.awt.Font("微软雅黑", 1, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 102, 0));
        jLabel13.setText("<html><body><u>Application for trial</u></body></html>");
        jLabel13.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel13.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel13MouseClicked(evt);
            }
        });
        jLabel13.setVisible(false);

        javax.swing.GroupLayout netJobLockPanelLayout = new javax.swing.GroupLayout(netJobLockPanel);
        netJobLockPanel.setLayout(netJobLockPanelLayout);
        netJobLockPanelLayout.setHorizontalGroup(
            netJobLockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(netJobLockPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(netJobLockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13))
                .addGroup(netJobLockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(netJobLockPanelLayout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(jLabel9))
                    .addGroup(netJobLockPanelLayout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(jLabel10)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        netJobLockPanelLayout.setVerticalGroup(
            netJobLockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(netJobLockPanelLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(netJobLockPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(netJobLockPanelLayout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel13))
                    .addGroup(netJobLockPanelLayout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        netJobParaPanel.setBackground(new java.awt.Color(245, 245, 245));
        netJobParaPanel.setPreferredSize(new java.awt.Dimension(290, 125));

        jLabel4.setBackground(new java.awt.Color(245, 245, 245));
        jLabel4.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel4.setText("Similarity Method:");

        jComboBox2.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        //, "USR..."
        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SHAFTS..."}));
        jComboBox2.setToolTipText((String)jComboBox2.getSelectedItem());
        jComboBox2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox2ItemStateChanged(evt);
            }
        });

        jRadioButton3.setBackground(new java.awt.Color(245, 245, 245));
        jRadioButton3.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jRadioButton3.setText("Bioactivity Database:");
        jRadioButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton3ActionPerformed(evt);
            }
        });

        jRadioButton4.setBackground(new java.awt.Color(245, 245, 245));
        jRadioButton4.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jRadioButton4.setText("Compound Database:");
        jRadioButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButton4ActionPerformed(evt);
            }
        });

        jComboBox4.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        // "MayBridge", "Specs", "NCI"
        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Choose...", "ZINC_TCM(142,148)", "NCI(246,483)" }));
        jComboBox4.setSelectedIndex(0);

        jComboBox4.setToolTipText((String)jComboBox4.getSelectedItem());
        jComboBox4.setEnabled(false);
        jComboBox4.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox4ItemStateChanged(evt);
            }
        });

        jComboBox3.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        //, "KEGG"
        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Choose...", "DrugBank(4,645)", "ChEMBL(339,624)", "BindingDB(364,221)", "PDB(7,072)" }));
        jComboBox3.setSelectedIndex(0);
        jComboBox3.setEnabled(false);
        jComboBox3.setToolTipText((String)jComboBox3.getSelectedItem());
        jComboBox3.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox3ItemStateChanged(evt);
            }
        });

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/about.png"))); // NOI18N
        jLabel12.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel12MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel12MouseExited(evt);
            }
        });

        javax.swing.GroupLayout netJobParaPanelLayout = new javax.swing.GroupLayout(netJobParaPanel);
        netJobParaPanel.setLayout(netJobParaPanelLayout);
        netJobParaPanelLayout.setHorizontalGroup(
            netJobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(netJobParaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(netJobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jRadioButton4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jRadioButton3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, netJobParaPanelLayout.createSequentialGroup()
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(48, 48, 48)))
                .addGap(18, 18, 18)
                .addGroup(netJobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox4, 0, 71, Short.MAX_VALUE)
                    .addComponent(jComboBox3, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        netJobParaPanelLayout.setVerticalGroup(
            netJobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(netJobParaPanelLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(netJobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(netJobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(netJobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jButton20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/undo.png"))); // NOI18N
        jButton20.setToolTipText("undo");
        jButton20.setVisible(false);
        jButton20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton20ActionPerformed(evt);
            }
        });

        jButton19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/redo.png"))); // NOI18N
        jButton19.setToolTipText("redo");
        jButton19.setVisible(false);
        jButton19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton19ActionPerformed(evt);
            }
        });

        wireframe.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/wireframe.png"))); // NOI18N
        wireframe.setToolTipText("wireframe");
        wireframe.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wireframeActionPerformed(evt);
            }
        });
        PDBstru.add(wireframe);

        cartoon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/cartoon.png"))); // NOI18N
        cartoon.setToolTipText("cartoon");
        cartoon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cartoonActionPerformed(evt);
            }
        });
        PDBstru.add(cartoon);

        strands.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/strands.png"))); // NOI18N
        strands.setToolTipText("strands");
        strands.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                strandsActionPerformed(evt);
            }
        });
        PDBstru.add(strands);

        ball_stcikStyle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/ball_stick.png"))); // NOI18N
        ball_stcikStyle.setToolTipText("ball and stick");
        ball_stcikStyle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ball_stcikStyleActionPerformed(evt);
            }
        });
        MOLstru.add(ball_stcikStyle);

        stickStyle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/bond.png"))); // NOI18N
        stickStyle.setToolTipText("sticks");
        stickStyle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                stickStyleActionPerformed(evt);
            }
        });
        MOLstru.add(stickStyle);

        wireframeStyle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/lineframe.png"))); // NOI18N
        wireframeStyle.setToolTipText("wireframe");
        wireframeStyle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wireframeStyleActionPerformed(evt);
            }
        });
        MOLstru.add(wireframeStyle);

        cpkStyle.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/cpkSpacefill.png"))); // NOI18N
        cpkStyle.setToolTipText("CPK");
        cpkStyle.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cpkStyleActionPerformed(evt);
            }
        });
        MOLstru.add(cpkStyle);

        jLabel8.setText(softwareInfo);

        checkInfo.setText("Status...");
        userPop.add(checkInfo);

        logOut.setText("Log out...");
        userPop.add(logOut);

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("eSHAFTS");
        setAutoRequestFocus(false);

        jToolBar1.setBackground(new java.awt.Color(204, 204, 204));
        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setMaximumSize(new java.awt.Dimension(330, 35));
        jToolBar1.setMinimumSize(new java.awt.Dimension(330, 35));
        jToolBar1.setPreferredSize(new java.awt.Dimension(330, 35));

        jButton1.setBackground(new java.awt.Color(204, 204, 204));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/openf.png"))); // NOI18N
        jButton1.setToolTipText("Import Structures");
        jButton1.setAlignmentX(5.0F);
        jButton1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton1);

        jButton2.setBackground(new java.awt.Color(204, 204, 204));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/format.png"))); // NOI18N
        jButton2.setToolTipText("Convert to a mol2 file");
        jButton2.setAlignmentX(5.0F);
        jButton2.setEnabled(false);
        jButton2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jToolBar1.add(jButton2);

        jButton18.setBackground(new java.awt.Color(204, 204, 204));
        jButton18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/3D.png"))); // NOI18N
        jButton18.setToolTipText("Generate 3D molecular model");
        jButton18.setEnabled(false);
        jButton18.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton18.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar1.add(jButton18);

        jButton3.setBackground(new java.awt.Color(204, 204, 204));
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/noH.png"))); // NOI18N
        jButton3.setToolTipText("Show/Hide H");
        jButton3.setAlignmentX(5.0F);
        jButton3.setEnabled(false);
        jButton3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton3);

        jButton4.setBackground(new java.awt.Color(204, 204, 204));
        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/doSpin.png"))); // NOI18N
        jButton4.setToolTipText("Spin on/off");
        jButton4.setAlignmentX(5.0F);
        jButton4.setEnabled(false);
        jButton4.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton4);

        jButton5.setBackground(new java.awt.Color(204, 204, 204));
        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/Reset.png"))); // NOI18N
        jButton5.setToolTipText("View");
        jButton5.setAlignmentX(5.0F);
        jButton5.setEnabled(false);
        jButton5.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton5);

        jButton6.setBackground(new java.awt.Color(204, 204, 204));
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/ball_stick.png"))); // NOI18N
        jButton6.setToolTipText("Change the display mode of ligand");
        jButton6.setAlignmentX(5.0F);
        jButton6.setEnabled(false);
        jButton6.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton6);

        jButton7.setBackground(new java.awt.Color(204, 204, 204));
        jButton7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/Optimize.png"))); // NOI18N
        jButton7.setToolTipText("Minimize");
        jButton7.setAlignmentX(5.0F);
        jButton7.setEnabled(false);
        jButton7.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton7);

        jButton8.setBackground(new java.awt.Color(204, 204, 204));
        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/modelKit.png"))); // NOI18N
        jButton8.setToolTipText("Builder");
        jButton8.setAlignmentX(5.0F);
        jButton8.setEnabled(false);
        jButton8.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton8);

        jButton22.setBackground(new java.awt.Color(204, 204, 204));
        jButton22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/wireframe.png"))); // NOI18N
        jButton22.setToolTipText("Change the display mode of protein");
        jButton22.setEnabled(false);
        jButton22.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton22.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton22ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton22);

        jButton25.setBackground(new java.awt.Color(204, 204, 204));
        jButton25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/Visual.png")));
        jButton25.setToolTipText("Visualizing results");
        jButton25.setEnabled(false);
        jButton25.setFocusable(false);
        jButton25.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton25.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton25ActionPerformed(evt);
            }
        });
        jToolBar1.add(jButton25);

        jToolBar2.setBackground(new java.awt.Color(204, 204, 204));
        jToolBar2.setFloatable(false);
        jToolBar2.setRollover(true);

        jButton17.setBackground(new java.awt.Color(204, 204, 204));
        jButton17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton17.setFocusable(false);
        jButton17.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton17.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jToolBar2.add(jButton17);

        jButton9.setBackground(new java.awt.Color(204, 204, 204));
        jButton9.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jButton9.setForeground(new java.awt.Color(0, 0, 255));
        jButton9.setContentAreaFilled(false);
        jButton9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jButton9.setFocusable(false);
        jButton9.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jButton9.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jButton9.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jButton9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton9MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton9MouseExited(evt);
            }
        });
        jToolBar2.add(jButton9);

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(verSplit);
        jSplitPane1.setDividerSize(3);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setResizeWeight(1.0);

        jSplitPane3.setBorder(null);
        jSplitPane3.setDividerLocation(horSplit);
        jSplitPane3.setDividerSize(3);
        jSplitPane3.setResizeWeight(1.0);

        jSplitPane4.setBorder(null);
        jSplitPane4.setDividerLocation(horSplit1);
        jSplitPane4.setDividerSize(3);

        jmolPanel = new JmolPanel();
        centerPanel.setBackground(new java.awt.Color(0, 0, 0));
        centerPanel.setAutoscrolls(true);
        centerPanel.setLayout(new java.awt.GridLayout(1, 0));
        centerPanel.add(jmolPanel);
        jmolPanel.initComp(jButton2, jButton3, jButton4, jButton5, jButton6, jButton7, jButton8, jButton13, jButton18, jMenu3, jTextField1);

        tipPanel1.setBackground(new java.awt.Color(245, 245, 245));
        tipPanel1.setLayout(new java.awt.GridLayout(1, 0));

        jToggleButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/NOVDW.png"))); // NOI18N
        jToggleButton1.setToolTipText("Show VDW");
        jToggleButton1.setBorder(null);
        jToggleButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jToggleButton1MouseEntered(evt);
            }
        });

        jButton16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/clean.png"))); // NOI18N
        jButton16.setToolTipText("Close job");
        jButton16.setBorder(null);

        jButton21.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/save.png"))); // NOI18N
        jButton21.setToolTipText("Update Model");
        jButton21.setBorder(null);
        jButton21.setEnabled(false);

        jToggleButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/zoom.png"))); // NOI18N
        jToggleButton2.setToolTipText("Zoom In");
        jToggleButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton2ActionPerformed(evt);
            }
        });

        jButton23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/showInput.png"))); // NOI18N
        jButton23.setToolTipText("Hide Query Molecule");
        jButton23.setEnabled(false);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(tipPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 632, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton23, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jToggleButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jToggleButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tipPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jToggleButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton16, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1))
            .addComponent(jToggleButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jButton21, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jButton23, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        javax.swing.GroupLayout molPanelLayout = new javax.swing.GroupLayout(molPanel);
        molPanel.setLayout(molPanelLayout);
        molPanelLayout.setHorizontalGroup(
            molPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(molPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(centerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 765, Short.MAX_VALUE))
        );
        molPanelLayout.setVerticalGroup(
            molPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(molPanelLayout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0))
            .addGroup(molPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, molPanelLayout.createSequentialGroup()
                    .addGap(22, 22, 22)
                    .addComponent(centerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 579, Short.MAX_VALUE)))
        );

        jSplitPane4.setRightComponent(molPanel);

        westPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        jScrollPane1.setBorder(null);

        jTree1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        localworkNode = new IconNode("Local");
        networkNode = new IconNode("Remote");
        targetNode = new IconNode("Target Navigator");
        hitNode = new IconNode("Hit Explorer");
        treeRoot.add(localworkNode);
        treeRoot.add(networkNode);
        networkNode.add(targetNode);
        networkNode.add(hitNode);
        jTree1.setCellRenderer(null);
        jScrollPane1.setViewportView(jTree1);

        //jScrollPane1.getViewport().setBackground(new java.awt.Color(0,51,51));

        centerPanelChild1.setBackground(new java.awt.Color(245, 245, 245));

        jButton15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/collapse.png"))); // NOI18N
        jButton15.setToolTipText("Collapse/Expand all nodes");
        jButton15.setBorder(null);
        jButton15.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jButton15MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jButton15MouseExited(evt);
            }
        });

        javax.swing.GroupLayout centerPanelChild1Layout = new javax.swing.GroupLayout(centerPanelChild1);
        centerPanelChild1.setLayout(centerPanelChild1Layout);
        centerPanelChild1Layout.setHorizontalGroup(
            centerPanelChild1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(centerPanelChild1Layout.createSequentialGroup()
                .addContainerGap(34, Short.MAX_VALUE)
                .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        centerPanelChild1Layout.setVerticalGroup(
            centerPanelChild1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(centerPanelChild1Layout.createSequentialGroup()
                .addComponent(jButton15, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1))
        );

        javax.swing.GroupLayout westPanelLayout = new javax.swing.GroupLayout(westPanel);
        westPanel.setLayout(westPanelLayout);
        westPanelLayout.setHorizontalGroup(
            westPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(westPanelLayout.createSequentialGroup()
                .addGroup(westPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(centerPanelChild1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        westPanelLayout.setVerticalGroup(
            westPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(westPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(centerPanelChild1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 575, Short.MAX_VALUE))
        );

        jSplitPane4.setLeftComponent(westPanel);

        javax.swing.GroupLayout leftPanelLayout = new javax.swing.GroupLayout(leftPanel);
        leftPanel.setLayout(leftPanelLayout);
        leftPanelLayout.setHorizontalGroup(
            leftPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, leftPanelLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jSplitPane4)
                .addGap(1, 1, 1))
        );
        leftPanelLayout.setVerticalGroup(
            leftPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane4, javax.swing.GroupLayout.Alignment.TRAILING)
        );

        jSplitPane3.setLeftComponent(leftPanel);

        eastPanel.setPreferredSize(new java.awt.Dimension(333, 453));

        jSplitPane2.setBorder(null);
        jSplitPane2.setDividerLocation(verSplit1);
        jSplitPane2.setDividerSize(3);
        jSplitPane2.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane2.setResizeWeight(1.0);

        eastTopPanel.setBackground(new java.awt.Color(245, 245, 245));

        jTabbedPane1.setBackground(new java.awt.Color(204, 204, 204));
        jTabbedPane1.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        jTabbedPane1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jTabbedPane1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane1StateChanged(evt);
            }
        });

        localJobPanel.setBackground(new java.awt.Color(245, 245, 245));
        localJobPanel.setPreferredSize(new java.awt.Dimension(292, 306));
        localJobPanel.setLayout(new java.awt.GridLayout(1, 0));

        localjobParaPanel.setBackground(new java.awt.Color(245, 245, 245));
        localjobParaPanel.setPreferredSize(new java.awt.Dimension(290, 125));

        jTextField3.setEditable(false);
        jTextField3.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jTextField3.setToolTipText("");
        jTextField3.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jCheckBox1.setBackground(new java.awt.Color(245, 245, 245));
        jCheckBox1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jCheckBox1.setText("Generate Conformers");
        jCheckBox1.setToolTipText("Need more time...");
        jCheckBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox1ActionPerformed(evt);
            }
        });

        jButton10.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jButton10.setText("Browse");

        jLabel14.setFont(new java.awt.Font("微软雅黑", 1, 12)); // NOI18N
        jLabel14.setText("Compound database:");

        jLabel15.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 153, 153));
        jLabel15.setText("Maximum Conformations:");
        jLabel15.setVisible(false);

        jTextField5.setBackground(new java.awt.Color(240, 240, 240));
        jTextField5.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jTextField5.setForeground(new java.awt.Color(0, 153, 153));
        jTextField5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jTextField5.setVisible(false);

        javax.swing.GroupLayout localjobParaPanelLayout = new javax.swing.GroupLayout(localjobParaPanel);
        localjobParaPanel.setLayout(localjobParaPanelLayout);
        localjobParaPanelLayout.setHorizontalGroup(
            localjobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(localjobParaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(localjobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(localjobParaPanelLayout.createSequentialGroup()
                        .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                        .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(localjobParaPanelLayout.createSequentialGroup()
                        .addGroup(localjobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(jCheckBox1)
                            .addGroup(localjobParaPanelLayout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addGap(10, 10, 10)
                                .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        localjobParaPanelLayout.setVerticalGroup(
            localjobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(localjobParaPanelLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jLabel14)
                .addGap(15, 15, 15)
                .addGroup(localjobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addComponent(jCheckBox1)
                .addGap(5, 5, 5)
                .addGroup(localjobParaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(99, Short.MAX_VALUE))
        );

        localJobPanel.add(localjobParaPanel);

        jTabbedPane1.addTab("Local", localJobPanel);

        netJobPanel.setLayout(new java.awt.GridLayout(1, 0));
        jTabbedPane1.addTab("Remote", netJobPanel);

        jPanel2.setBackground(new java.awt.Color(245, 245, 245));

        jLabel5.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel5.setText("PDB 4-letter code:");

        jTextField4.setBackground(new java.awt.Color(245, 245, 245));
        jTextField4.setFont(new java.awt.Font("微软雅黑", 0, 14)); // NOI18N
        jTextField4.setForeground(new java.awt.Color(0, 0, 255));
        jTextField4.setToolTipText("Must contain both the letters and numbers");
        jTextField4.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jTextField4.setCaretColor(new java.awt.Color(0, 0, 255));
        jTextField4.setDocument(new PlainDocument(){
            public void insertString(int offset,String str,AttributeSet as)
            throws BadLocationException{
                if(this.getLength()+str.length() >= 5){
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }else{
                    super.insertString(offset,str,as);
                }
            }
        });
        jTextField4.getDocument().addDocumentListener(new DocumentListener(){
            @Override
            public void removeUpdate(DocumentEvent e) {
                String text1 = null;
                try {
                    text1 = e.getDocument().getText(e.getDocument().getStartPosition().getOffset(), e.getDocument().getLength());
                } catch (BadLocationException ex) {
                    Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(text1 == null || text1.length() != 4){
                    disableButton();
                    jButton11.setVisible(false);
                    initNetJobPanel();
                    initNativeJobPanel();
                    initParaPanel();
                    initPanel();
                    jButton13.setEnabled(false);
                }
            }
            @Override
            public void insertUpdate(DocumentEvent e) {
                try {
                    pdbCode = e.getDocument().getText(e.getDocument()
                        .getStartPosition().getOffset(), e.getDocument().getLength());
                } catch (BadLocationException ex) {
                    Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
                }
                if(pdbCode.length() == 4){
                    new Thread(){
                        @Override
                        public void run(){
                            WaitingGif wg = new WaitingGif("Loading pdb " + pdbCode, 90000);
                            wg._setVisible(true);
                            File pdbFile = new File(pdbPath + pdbCode +".pdb.gz");
                            initPanel();
                            ArrayList inforList = new ArrayList();
                            tipPanel1.removeAll();
                            tipPanel1.add(new TipPanel("Loading...", "Error", 90000));
                            if(pdbFile.exists()){
                                String status = jmolPanel.viewer.
                                scriptWait("load " + pdbPath + pdbCode +".pdb.gz;select not protein "
                                    + "and not solvent;spacefill off;select not selected;cpk off");
                                if(jmolPanel.viewer.getErrorMessage() != null){
                                    wg.close();
                                    int i = JOptionPane.showConfirmDialog(null, "File incomplete! Don you want delete it?",
                                        "Error", JOptionPane.OK_CANCEL_OPTION);
                                    if(i == JOptionPane.OK_OPTION){
                                        pdbFile.delete();
                                        tipPanel1.removeAll();
                                        tipPanel1.add(new TipPanel(pdbCode + " has deleted!", "Error"));
                                    }else{
                                        tipPanel1.removeAll();
                                        tipPanel1.add(new TipPanel("Loading failed!", "Error"));
                                    }
                                }else{
                                    String tempPath = new PdbDecompress().decompress(pdbFile.getAbsolutePath());
                                    if (tempPath != null) {
                                        inforList.add("PDB ID:" + pdbCode);
                                        inforList = PdbAnalysis.getInfor(tempPath, inforList);
                                    }
                                }
                            }else{
                                //                        String loadpdb = "zap all; load \"=" + pdbCode +"\";select not protein and"
                                //                                            + " not solvent;spacefill off;select not selected;cpk off";
                                //org 上述两行 Song 2017.6.13 由于jmol上PDB的地址默认为http://www.rcsb.org/，但是RCSB更新为https://files.rcsb.org/download，因此不能用上述语句
                                String loadpdb = "load \"https://files.rcsb.org/download/" + pdbCode + ".pdb.gz\"" + ";select not protein and not solvent;spacefill off;select not selected;cpk off";
                                System.out.println("============= " + loadpdb);
                                String status = jmolPanel.viewer.scriptWait(loadpdb);
                                if(jmolPanel.viewer.getErrorMessage() != null){
                                    wg.close();
                                    tipPanel1.removeAll();
                                    tipPanel1.add(new TipPanel("Loading failed!", "Error"));
                                }else{
                                    inforList.add("PDB ID:" + pdbCode);
                                    //inforList.add("Data Source:http://www.rcsb.org/");
                                    inforList.add("More Tips:Download to local can get "
                                        + "more detail information of the pdb " + pdbCode);
                                }
                            }
                            int atomcount;
                            atomcount = jmolPanel.viewer.getAtomCount();
                            if(atomcount != 0){
                                jButton13.setEnabled(true);
                                jButton22.setEnabled(true);
                                enableButton();
                                jComboBox5.setModel(new javax.swing.DefaultComboBoxModel(jmolPanel.getchains()));
                                tipPanel1.removeAll();
                                tipPanel1.add(new TipPanel("Loading finished...", "Error"));
                                String info = InfoFactory.setInfor(inforList);
                                jEditorPane1.setText(info);
                                jEditorPane1.setCaretPosition(0);
                            }
                            wg.close();
                        }
                    }.start();
                }
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });

        jButton13.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jButton13.setText("Download/Reload");
        jButton13.setToolTipText("Save to local");
        jButton13.setEnabled(false);

        jSeparator1.setBackground(new java.awt.Color(204, 204, 204));
        jSeparator1.setForeground(new java.awt.Color(255, 255, 255));
        jSeparator1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel6.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel6.setText("chain:");

        jComboBox5.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select chain..." }));
        jComboBox5.setToolTipText("Select the chain");
        jComboBox5.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox5ItemStateChanged(evt);
            }
        });

        jLabel7.setBackground(new java.awt.Color(0, 102, 102));
        jLabel7.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel7.setText("ligand:");

        jComboBox6.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jComboBox6.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Select ligand..." }));
        jComboBox6.setToolTipText("Select the ligand");
        jComboBox6.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox6ItemStateChanged(evt);
            }
        });

        jButton14.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jButton14.setText("Extract");
        jButton14.setToolTipText("Current selected will be opened in jmol separately");
        jButton14.setEnabled(false);

        jCheckBox2.setBackground(new java.awt.Color(245, 245, 245));
        jCheckBox2.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jCheckBox2.setText("With bond and atom information");
        jCheckBox2.setBorder(null);
        jCheckBox2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jCheckBox2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jCheckBox2MouseExited(evt);
            }
        });
        jCheckBox2.setVisible(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton13))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jComboBox6, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton14))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jCheckBox2)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(18, 18, 18)
                                .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton13, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 2, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox6, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton14, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox2)
                .addGap(20, 20, 20))
        );

        jTabbedPane1.addTab("PDB", jPanel2);

        paraPanel.setBackground(new java.awt.Color(245, 245, 245));
        paraPanel.setPreferredSize(new java.awt.Dimension(290, 128));

        jLabel1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel1.setText("Molecule:");

        jTextField1.setEditable(false);
        jTextField1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jTextField1.setToolTipText(jTextField1.getText());
        jTextField1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jLabel2.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel2.setText("Threshold:");

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0.8", "1.0", "1.2", "1.5" , "1.8"}));
        jComboBox1.setSelectedItem("1.2");
        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });

        jLabel3.setBackground(new java.awt.Color(245, 245, 245));
        jLabel3.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel3.setText("Max Hits:");

        jTextField2.setBackground(new java.awt.Color(240, 240, 240));
        jTextField2.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jTextField2.setText("1000");
        jTextField2.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        tipPanel2.setBackground(new java.awt.Color(245, 245, 245));
        tipPanel2.setLayout(new java.awt.GridLayout(1, 0));

        jButton11.setVisible(false);
        jButton11.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jButton11.setText("Export");

        jButton12.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jButton12.setText(" Start  ");
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jButton24.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jButton24.setText("Browse");
        jButton24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton24ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout paraPanelLayout = new javax.swing.GroupLayout(paraPanel);
        paraPanel.setLayout(paraPanelLayout);
        paraPanelLayout.setHorizontalGroup(
            paraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paraPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(paraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(tipPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, paraPanelLayout.createSequentialGroup()
                        .addGroup(paraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(paraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jTextField1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                            .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jTextField2))
                        .addGap(18, 18, 18)
                        .addComponent(jButton24))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, paraPanelLayout.createSequentialGroup()
                        .addGap(0, 107, Short.MAX_VALUE)
                        .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        paraPanelLayout.setVerticalGroup(
            paraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paraPanelLayout.createSequentialGroup()
                .addComponent(tipPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addGroup(paraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton24, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(paraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12)
                .addGroup(paraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(15, 15, 15)
                .addGroup(paraPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton11, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton12, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        javax.swing.GroupLayout eastTopPanelLayout = new javax.swing.GroupLayout(eastTopPanel);
        eastTopPanel.setLayout(eastTopPanelLayout);
        eastTopPanelLayout.setHorizontalGroup(
            eastTopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(eastTopPanelLayout.createSequentialGroup()
                .addGroup(eastTopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(paraPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 295, Short.MAX_VALUE))
                .addGap(1, 1, 1))
        );
        eastTopPanelLayout.setVerticalGroup(
            eastTopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(eastTopPanelLayout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
                .addGap(2, 2, 2)
                .addComponent(paraPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 304, Short.MAX_VALUE)
                .addGap(5, 5, 5))
        );

        jSplitPane2.setLeftComponent(eastTopPanel);

        eastBottomPanel.setLayout(new java.awt.GridLayout(1, 0));

        jPanel5.setBackground(new java.awt.Color(245, 245, 245));
        jPanel5.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.LOWERED));

        jScrollPane2.setBorder(null);
        jScrollPane2.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jScrollPane2.getViewport().setBackground(new java.awt.Color(0,51,51));

        jEditorPane1.setEditable(false);
        jEditorPane1.setBackground(new java.awt.Color(255, 234, 185));
        jEditorPane1.setContentType("text/html;charset=utf-8"); // NOI18N
        jEditorPane1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jEditorPane1.setText(com.shafts.utils.InfoFactory.baseInfor());
        jScrollPane2.setViewportView(jEditorPane1);

        //jScrollPane2.getViewport().setViewPosition(point);

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 195, Short.MAX_VALUE)
        );

        eastBottomPanel.add(jPanel5);

        jSplitPane2.setRightComponent(eastBottomPanel);

        javax.swing.GroupLayout eastPanelLayout = new javax.swing.GroupLayout(eastPanel);
        eastPanel.setLayout(eastPanelLayout);
        eastPanelLayout.setHorizontalGroup(
            eastPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2)
        );
        eastPanelLayout.setVerticalGroup(
            eastPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 601, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout rightPanelLayout = new javax.swing.GroupLayout(rightPanel);
        rightPanel.setLayout(rightPanelLayout);
        rightPanelLayout.setHorizontalGroup(
            rightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 296, Short.MAX_VALUE)
            .addGroup(rightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(eastPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 296, Short.MAX_VALUE))
        );
        rightPanelLayout.setVerticalGroup(
            rightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 601, Short.MAX_VALUE)
            .addGroup(rightPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(eastPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 601, Short.MAX_VALUE))
        );

        jSplitPane3.setRightComponent(rightPanel);

        javax.swing.GroupLayout TopPanelLayout = new javax.swing.GroupLayout(TopPanel);
        TopPanel.setLayout(TopPanelLayout);
        TopPanelLayout.setHorizontalGroup(
            TopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1127, Short.MAX_VALUE)
            .addGroup(TopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(TopPanelLayout.createSequentialGroup()
                    .addComponent(jSplitPane3)
                    .addGap(0, 0, 0)))
        );
        TopPanelLayout.setVerticalGroup(
            TopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 601, Short.MAX_VALUE)
            .addGroup(TopPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jSplitPane3))
        );

        jSplitPane1.setLeftComponent(TopPanel);

        BottomPanel.setBackground(new java.awt.Color(245, 245, 245));
        BottomPanel.setLayout(new java.awt.GridLayout(1, 0));

        resultShowPanel.setLayout(new java.awt.GridLayout(1, 0));

        jScrollPane3.setBorder(null);

        jTextPane1.setEditable(false);
        jTextPane1.setBorder(null);
        jTextPane1.setContentType("text/html"); // NOI18N
        jTextPane1.setText(softwareInfo);

        jTextPane1.addHyperlinkListener(new HyperLink());
        jScrollPane3.setViewportView(jTextPane1);

        resultShowPanel.add(jScrollPane3);

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(153, 153, 153)));
        jPanel1.setPreferredSize(new java.awt.Dimension(1127, 20));

        scanningPanel.setLayout(new java.awt.GridLayout(1, 0));

        jobStatus4.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jobStatus4.setText("Scanning job");

        jobStatus3.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jobStatus3.setText("Job Status:");

        jobStatus.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jobStatus.setText("N / N  | ");
        jobStatus.setToolTipText("Running / Total");
        jobStatus.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jobStatus1.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jobStatus1.setText("Detail");
        jobStatus1.setToolTipText("");
        jobStatus1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        jobStatus1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jobStatus1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jobStatus1MouseExited(evt);
            }
        });

        javax.swing.GroupLayout scanningPanel1Layout = new javax.swing.GroupLayout(scanningPanel1);
        scanningPanel1.setLayout(scanningPanel1Layout);
        scanningPanel1Layout.setHorizontalGroup(
            scanningPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(scanningPanel1Layout.createSequentialGroup()
                .addComponent(jobStatus3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jobStatus)
                .addGap(8, 8, 8)
                .addComponent(jobStatus1)
                .addGap(2, 2, 2))
        );
        scanningPanel1Layout.setVerticalGroup(
            scanningPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, scanningPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(scanningPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jobStatus3)
                    .addComponent(jobStatus)
                    .addComponent(jobStatus1)))
        );

        jobStatus2.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jobStatus2.setText(" | ");
        jobStatus2.setToolTipText("Running / Total");
        jobStatus2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jobStatus4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scanningPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
                .addGap(612, 612, 612)
                .addComponent(scanningPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(jobStatus2)
                .addGap(9, 9, 9))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(scanningPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jobStatus4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jobStatus2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(scanningPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout southPanelLayout = new javax.swing.GroupLayout(southPanel);
        southPanel.setLayout(southPanelLayout);
        southPanelLayout.setHorizontalGroup(
            southPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, southPanelLayout.createSequentialGroup()
                .addGroup(southPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(resultShowPanel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, 0))
        );
        southPanelLayout.setVerticalGroup(
            southPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, southPanelLayout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addComponent(resultShowPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 22, Short.MAX_VALUE)
                .addGap(1, 1, 1)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        BottomPanel.add(southPanel);

        jSplitPane1.setRightComponent(BottomPanel);

        jMenuBar1.setBorder(null);

        jMenu1.setText(" File ");

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Open");
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("New...");
        jMenu1.add(jMenuItem2);

        jMenuItem12.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem12.setText("Save molecule...");
        jMenuItem12.setEnabled(false);
        jMenu1.add(jMenuItem12);

        jMenuItem5.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem5.setText("Save picture");
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem5);

        jMenuItem13.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_W, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem13.setText("Close job");
        jMenuItem13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem13ActionPerformed(evt);
            }
        });
        jMenu1.add(jMenuItem13);

        jMenuItem3.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem3.setText("Exit");
        jMenu1.add(jMenuItem3);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Tools");

        jMenuItem4.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem4.setText("Convert");
        jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem4ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem4);

        jMenuItem14.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_M, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem14.setText("Energy Minimize");
        jMenuItem14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem14ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem14);

        jMenuBar1.add(jMenu2);

        jMenu3.setText("Render");
        jMenu3.setEnabled(false);

        jMenu8.setText("Surface");

        jCheckBoxMenuItem1.setText("Dot Surface");
        jCheckBoxMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem1ActionPerformed(evt);
            }
        });
        jMenu8.add(jCheckBoxMenuItem1);

        jCheckBoxMenuItem2.setText("van der Waals Surface");
        jCheckBoxMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem2ActionPerformed(evt);
            }
        });
        jMenu8.add(jCheckBoxMenuItem2);

        jCheckBoxMenuItem3.setText("Molecular Surface");
        jCheckBoxMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem3ActionPerformed(evt);
            }
        });
        jMenu8.add(jCheckBoxMenuItem3);

        jCheckBoxMenuItem4.setText("Solvent Surface(1.4-Angstrom probe)");
        jCheckBoxMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem4ActionPerformed(evt);
            }
        });
        jMenu8.add(jCheckBoxMenuItem4);

        jCheckBoxMenuItem5.setText("Soklvent-Accessible Surface(VDW+1.4 Angstrom)");
        jCheckBoxMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem5ActionPerformed(evt);
            }
        });
        jMenu8.add(jCheckBoxMenuItem5);

        jCheckBoxMenuItem6.setText("Molecular Electrostatic Potential");
        jCheckBoxMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem6ActionPerformed(evt);
            }
        });
        jMenu8.add(jCheckBoxMenuItem6);

        jCheckBoxMenuItem7.setText("Make Opaque");
        jCheckBoxMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem7ActionPerformed(evt);
            }
        });
        jMenu8.add(jCheckBoxMenuItem7);

        jCheckBoxMenuItem8.setText("MakeTranslucent");
        jCheckBoxMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem8ActionPerformed(evt);
            }
        });
        jMenu8.add(jCheckBoxMenuItem8);

        jCheckBoxMenuItem9.setSelected(true);
        jCheckBoxMenuItem9.setText("Off");
        jCheckBoxMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem9ActionPerformed(evt);
            }
        });
        jMenu8.add(jCheckBoxMenuItem9);

        jMenu3.add(jMenu8);

        jMenu6.setText("Ligand Style");

        jMenuItem15.setText("line");
        jMenuItem15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem15ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem15);

        jMenuItem16.setText("Ball and Stick");
        jMenuItem16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem16ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem16);

        jMenuItem17.setText("Stick");
        jMenuItem17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem17ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem17);

        jMenuItem18.setText("CPK");
        jMenuItem18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem18ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem18);

        jMenuItem23.setText("Show H");
        jMenuItem23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem23ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem23);

        jMenuItem24.setText("Hide H");
        jMenuItem24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem24ActionPerformed(evt);
            }
        });
        jMenu6.add(jMenuItem24);

        jMenu3.add(jMenu6);

        jMenu11.setText("Protein Style");
        jMenu11.setToolTipText("");

        jMenuItem19.setText("Line");
        jMenuItem19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem19ActionPerformed(evt);
            }
        });
        jMenu11.add(jMenuItem19);

        jMenuItem20.setText("Cartoon");
        jMenuItem20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem20ActionPerformed(evt);
            }
        });
        jMenu11.add(jMenuItem20);

        jMenuItem21.setText("Strands");
        jMenuItem21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem21ActionPerformed(evt);
            }
        });
        jMenu11.add(jMenuItem21);

        jMenu3.add(jMenu11);

        jMenu10.setText("Spin");

        jCheckBoxMenuItem20.setText("On");
        jCheckBoxMenuItem20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxMenuItem20ActionPerformed(evt);
            }
        });
        jMenu10.add(jCheckBoxMenuItem20);

        jMenuItem11.setText("Reset");
        jMenuItem11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem11ActionPerformed(evt);
            }
        });
        jMenu10.add(jMenuItem11);

        jMenu3.add(jMenu10);

        jMenu9.setText("Background");

        jRadioButtonMenuItem1.setText("White");
        jRadioButtonMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem1ActionPerformed(evt);
            }
        });
        jMenu9.add(jRadioButtonMenuItem1);

        jRadioButtonMenuItem2.setSelected(true);
        jRadioButtonMenuItem2.setText("Black");
        jRadioButtonMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem2ActionPerformed(evt);
            }
        });
        jMenu9.add(jRadioButtonMenuItem2);

        jRadioButtonMenuItem3.setText("Red");
        jRadioButtonMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem3ActionPerformed(evt);
            }
        });
        jMenu9.add(jRadioButtonMenuItem3);

        jRadioButtonMenuItem4.setText("Orange");
        jRadioButtonMenuItem4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem4ActionPerformed(evt);
            }
        });
        jMenu9.add(jRadioButtonMenuItem4);

        jRadioButtonMenuItem5.setText("Yellow");
        jRadioButtonMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem5ActionPerformed(evt);
            }
        });
        jMenu9.add(jRadioButtonMenuItem5);

        jRadioButtonMenuItem6.setText("Green");
        jRadioButtonMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem6ActionPerformed(evt);
            }
        });
        jMenu9.add(jRadioButtonMenuItem6);

        jRadioButtonMenuItem7.setText("Cyan");
        jRadioButtonMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem7ActionPerformed(evt);
            }
        });
        jMenu9.add(jRadioButtonMenuItem7);

        jRadioButtonMenuItem8.setText("Blue");
        jRadioButtonMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem8ActionPerformed(evt);
            }
        });
        jMenu9.add(jRadioButtonMenuItem8);

        jRadioButtonMenuItem9.setText("Indigo");
        jRadioButtonMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem9ActionPerformed(evt);
            }
        });
        jMenu9.add(jRadioButtonMenuItem9);

        jRadioButtonMenuItem10.setText("Violet");
        jRadioButtonMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonMenuItem10ActionPerformed(evt);
            }
        });
        jMenu9.add(jRadioButtonMenuItem10);

        jMenu3.add(jMenu9);

        jMenuBar1.add(jMenu3);

        jMenu4.setText(" Setting ");

        jMenuItem7.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem7.setText("Set Your Workpath");
        jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem7ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem7);

        jMenuItem6.setText("Other...");
        jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem6ActionPerformed(evt);
            }
        });
        jMenu4.add(jMenuItem6);

        jMenuBar1.add(jMenu4);

        jMenu5.setText(" Help ");

        jMenuItem8.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_H, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem8.setText("eSHAFTS Help");
        jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem8ActionPerformed(evt);
            }
        });
        jMenu5.add(jMenuItem8);

        jMenuBar1.add(jMenu5);

        jMenu7.setText(" About... ");

        jMenuItem9.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_U, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem9.setText("About Us...");
        jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem9ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem9);

        jMenuItem10 .setVisible(false);
        jMenuItem10.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.ALT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem10.setText("About eSHAFTS...");
        jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem10ActionPerformed(evt);
            }
        });
        jMenu7.add(jMenuItem10);

        jMenu12.setText("Protocol");

        jMenuItem22.setText("OpenBabel");
        jMenuItem22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem22ActionPerformed(evt);
            }
        });
        jMenu12.add(jMenuItem22);

        jMenuItem25.setText("JCemPaint");
        jMenuItem25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem25ActionPerformed(evt);
            }
        });
        jMenu12.add(jMenuItem25);

        jMenuItem26.setText("Jmol");
        jMenuItem26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem26ActionPerformed(evt);
            }
        });
        jMenu12.add(jMenuItem26);

        jMenu7.add(jMenu12);

        jMenuBar1.add(jMenu7);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0)
                .addComponent(jToolBar2, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jToolBar2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(1, 1, 1)
                .addComponent(jSplitPane1))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem4ActionPerformed
        // format conversion
        new FormatConvUI(jTextField1.getToolTipText(), jmolPanel, jTextField1).setVisible(true);
    }//GEN-LAST:event_jMenuItem4ActionPerformed

    private void jCheckBoxMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem3ActionPerformed
        // Molecular surface
        if (jCheckBoxMenuItem3.isSelected()) {
            jCheckBoxMenuItem9.setSelected(false);
            String controller = "isosurface delete resolution 0 molecular translucent";
            jmolPanel.viewer.evalStringQuiet(controller);
        } else {
            String controller = "isosurface off";
            jmolPanel.viewer.evalString(controller);
            if (!(jCheckBoxMenuItem1.isSelected() || jCheckBoxMenuItem2.isSelected() || jCheckBoxMenuItem3.isSelected() || jCheckBoxMenuItem4.isSelected() || jCheckBoxMenuItem5.isSelected() || jCheckBoxMenuItem6.isSelected() || jCheckBoxMenuItem7.isSelected())) {
                jCheckBoxMenuItem9.setSelected(true);
            }
        }
    }//GEN-LAST:event_jCheckBoxMenuItem3ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // optimize molecule structure
        minimize();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton9MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton9MouseEntered
        jButton9.setForeground(new java.awt.Color(255, 102, 0));
    }//GEN-LAST:event_jButton9MouseEntered

    private void jButton9MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton9MouseExited
        jButton9.setForeground(new java.awt.Color(0, 0, 255));
    }//GEN-LAST:event_jButton9MouseExited

    private void jRadioButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton1ActionPerformed
        paraPanel.removeAll();
        shaftsModel = 1;
        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(paraPanel);
        paraPanel.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(localjobParaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 124, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(localjobParaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        paraPanel.updateUI();
    }//GEN-LAST:event_jRadioButton1ActionPerformed

    private void jRadioButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton2ActionPerformed
        paraPanel.removeAll();
        shaftsModel = 2;
        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(paraPanel);
        paraPanel.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(netJobParaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
                jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGap(0, 124, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(netJobParaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        paraPanel.updateUI();
    }//GEN-LAST:event_jRadioButton2ActionPerformed

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // Open new molecule file
        openFile();
    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // export molecule picture
        int atomCount = jmolPanel.viewer.getAtomCount();
        //TipsRender tr = new TipsRender();
        if (atomCount == 0) {
            //tr.render(TipLable, "No file to export!!", "Error");
            tipPanel1.removeAll();
            tipPanel1.add(new TipPanel("No file to export!!", "Error"));
            // JOptionPane.showMessageDialog(null, "No file to export!!");
        } else {
            ExportImage ei = new ExportImage();
            ei.setVisible(true);
            String ifSave = ei.getCommand();
            if (ifSave.equals("yes")) {
                String path = ei.getPath();
                String command = "write gif " + path;
                String result = (String) jmolPanel.viewer.scriptWaitStatus(command, suffixJmol);
                tipPanel1.removeAll();
                tipPanel1.add(new TipPanel(result, "Error"));
            }
        }
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    private void jCheckBoxMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem1ActionPerformed
        // dot surface
        if (jCheckBoxMenuItem1.isSelected()) {
            jCheckBoxMenuItem9.setSelected(false);
            String controller = "dots on";
            jmolPanel.viewer.evalString(controller);
        } else {
            String controller = "dots off";
            jmolPanel.viewer.evalString(controller);
            if (!(jCheckBoxMenuItem1.isSelected() || jCheckBoxMenuItem2.isSelected() || jCheckBoxMenuItem3.isSelected() || jCheckBoxMenuItem4.isSelected() || jCheckBoxMenuItem5.isSelected() || jCheckBoxMenuItem6.isSelected() || jCheckBoxMenuItem7.isSelected())) {
                jCheckBoxMenuItem9.setSelected(true);
            }
        }
    }//GEN-LAST:event_jCheckBoxMenuItem1ActionPerformed

    private void jCheckBoxMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem2ActionPerformed
        // van der waals surface
        if (jCheckBoxMenuItem2.isSelected()) {
            jCheckBoxMenuItem9.setSelected(false);
            String controller = "isosurface delete resolution 0 solvent 0 translucent";
            jmolPanel.viewer.evalString(controller);
        } else {
            String controller = "isosurface off";
            jmolPanel.viewer.evalString(controller);
            if (!(jCheckBoxMenuItem1.isSelected() || jCheckBoxMenuItem2.isSelected() || jCheckBoxMenuItem3.isSelected() || jCheckBoxMenuItem4.isSelected() || jCheckBoxMenuItem5.isSelected() || jCheckBoxMenuItem6.isSelected() || jCheckBoxMenuItem7.isSelected())) {
                jCheckBoxMenuItem9.setSelected(true);
            }
        }
    }//GEN-LAST:event_jCheckBoxMenuItem2ActionPerformed

    private void jCheckBoxMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem4ActionPerformed
        // Solvent Surface:
        if (jCheckBoxMenuItem4.isSelected()) {
            jCheckBoxMenuItem9.setSelected(false);
            String controller = "isosurface delete resolution 0 solvent 1.4 translucent";
            jmolPanel.viewer.evalStringQuiet(controller);
        } else {
            String controller = "isosurface off";
            jmolPanel.viewer.evalString(controller);
            if (!(jCheckBoxMenuItem1.isSelected() || jCheckBoxMenuItem2.isSelected() || jCheckBoxMenuItem3.isSelected() || jCheckBoxMenuItem4.isSelected() || jCheckBoxMenuItem5.isSelected() || jCheckBoxMenuItem6.isSelected() || jCheckBoxMenuItem7.isSelected())) {
                jCheckBoxMenuItem9.setSelected(true);
            }
        }
    }//GEN-LAST:event_jCheckBoxMenuItem4ActionPerformed

    private void jCheckBoxMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem5ActionPerformed
        // Solvent-Accessible Surface
        if (jCheckBoxMenuItem5.isSelected()) {
            String controller = "isosurface delete resolution 0 sasurface 1.4 translucent";
            jmolPanel.viewer.evalStringQuiet(controller);
        } else {
            String controller = "isosurface off";
            jmolPanel.viewer.evalString(controller);
            if (!(jCheckBoxMenuItem1.isSelected() || jCheckBoxMenuItem2.isSelected() || jCheckBoxMenuItem3.isSelected() || jCheckBoxMenuItem4.isSelected() || jCheckBoxMenuItem5.isSelected() || jCheckBoxMenuItem6.isSelected() || jCheckBoxMenuItem7.isSelected())) {
                jCheckBoxMenuItem9.setSelected(true);
            }
        }
    }//GEN-LAST:event_jCheckBoxMenuItem5ActionPerformed

    private void jCheckBoxMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem6ActionPerformed
        // electrostatic surface
        if (jCheckBoxMenuItem6.isSelected()) {
            jCheckBoxMenuItem9.setSelected(false);
            String controller = "isosurface delete resolution 0 vdw color range all map MEP translucent";
            jmolPanel.viewer.evalStringQuiet(controller);
        } else {
            String controller = "isosurface off";
            jmolPanel.viewer.evalString(controller);
            if (!(jCheckBoxMenuItem1.isSelected() || jCheckBoxMenuItem2.isSelected() || jCheckBoxMenuItem3.isSelected() || jCheckBoxMenuItem4.isSelected() || jCheckBoxMenuItem5.isSelected() || jCheckBoxMenuItem6.isSelected() || jCheckBoxMenuItem7.isSelected())) {
                jCheckBoxMenuItem9.setSelected(true);
            }
        }
    }//GEN-LAST:event_jCheckBoxMenuItem6ActionPerformed

    private void jCheckBoxMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem7ActionPerformed
        // opaque
        if (jCheckBoxMenuItem7.isSelected()) {
            jCheckBoxMenuItem9.setSelected(false);
            String controller = "mo opaque;isosurface opaque";
            jmolPanel.viewer.evalStringQuiet(controller);
        } else {
            String controller = "isosurface off";
            jmolPanel.viewer.evalString(controller);
            if (!(jCheckBoxMenuItem1.isSelected() || jCheckBoxMenuItem2.isSelected() || jCheckBoxMenuItem3.isSelected() || jCheckBoxMenuItem4.isSelected() || jCheckBoxMenuItem5.isSelected() || jCheckBoxMenuItem6.isSelected() || jCheckBoxMenuItem7.isSelected())) {
                jCheckBoxMenuItem9.setSelected(true);
            }
        }
    }//GEN-LAST:event_jCheckBoxMenuItem7ActionPerformed

    private void jCheckBoxMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem8ActionPerformed
        // make translucent
        if (jCheckBoxMenuItem8.isSelected()) {
            String controller = "mo translucent;isosurface translucent";
            jmolPanel.viewer.evalStringQuiet(controller);
        } else {
            String controller = "isosurface off";
            jmolPanel.viewer.evalString(controller);
            if (!(jCheckBoxMenuItem1.isSelected() || jCheckBoxMenuItem2.isSelected() || jCheckBoxMenuItem3.isSelected() || jCheckBoxMenuItem4.isSelected() || jCheckBoxMenuItem5.isSelected() || jCheckBoxMenuItem6.isSelected() || jCheckBoxMenuItem7.isSelected())) {
                jCheckBoxMenuItem9.setSelected(true);
            }
        }
    }//GEN-LAST:event_jCheckBoxMenuItem8ActionPerformed

    private void jCheckBoxMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem9ActionPerformed
        // off
        if (jCheckBoxMenuItem9.isSelected()) {
            jCheckBoxMenuItem9.setSelected(true);
            jCheckBoxMenuItem1.setSelected(false);
            jCheckBoxMenuItem2.setSelected(false);
            jCheckBoxMenuItem3.setSelected(false);
            jCheckBoxMenuItem4.setSelected(false);
            jCheckBoxMenuItem5.setSelected(false);
            jCheckBoxMenuItem6.setSelected(false);
            jCheckBoxMenuItem7.setSelected(false);
            jCheckBoxMenuItem8.setSelected(false);
            String controller = "mo delete;isosurface delete;select *;dots off";
            jmolPanel.viewer.evalStringQuiet(controller);
        }
    }//GEN-LAST:event_jCheckBoxMenuItem9ActionPerformed

    private void jRadioButtonMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem1ActionPerformed
        //white
        jmolPanel.viewer.setColorBackground("white");
        centerPanel.updateUI();
    }//GEN-LAST:event_jRadioButtonMenuItem1ActionPerformed

    private void jRadioButtonMenuItem2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem2ActionPerformed
        //black
        jmolPanel.viewer.setColorBackground("black");
        centerPanel.updateUI();
    }//GEN-LAST:event_jRadioButtonMenuItem2ActionPerformed

    private void jRadioButtonMenuItem3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem3ActionPerformed
        // red
        jmolPanel.viewer.setColorBackground("red");
        centerPanel.updateUI();
    }//GEN-LAST:event_jRadioButtonMenuItem3ActionPerformed

    private void jRadioButtonMenuItem4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem4ActionPerformed
        // orange
        jmolPanel.viewer.setColorBackground("orange");
        centerPanel.updateUI();
    }//GEN-LAST:event_jRadioButtonMenuItem4ActionPerformed

    private void jRadioButtonMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem5ActionPerformed
        // yellow
        jmolPanel.viewer.setColorBackground("yellow");
        centerPanel.updateUI();
    }//GEN-LAST:event_jRadioButtonMenuItem5ActionPerformed

    private void jRadioButtonMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem6ActionPerformed
        // green
        jmolPanel.viewer.setColorBackground("green");
        centerPanel.updateUI();
    }//GEN-LAST:event_jRadioButtonMenuItem6ActionPerformed

    private void jRadioButtonMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem7ActionPerformed
        // cyan
        jmolPanel.viewer.setColorBackground("cyan");
        centerPanel.updateUI();
    }//GEN-LAST:event_jRadioButtonMenuItem7ActionPerformed

    private void jRadioButtonMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem8ActionPerformed
        // blue
        jmolPanel.viewer.setColorBackground("blue");
        centerPanel.updateUI();
    }//GEN-LAST:event_jRadioButtonMenuItem8ActionPerformed

    private void jRadioButtonMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem9ActionPerformed
        // indigo
        jmolPanel.viewer.setColorBackground("indigo");
        centerPanel.updateUI();
    }//GEN-LAST:event_jRadioButtonMenuItem9ActionPerformed

    private void jRadioButtonMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonMenuItem10ActionPerformed
        // violet
        jmolPanel.viewer.setColorBackground("violet");
        centerPanel.updateUI();
    }//GEN-LAST:event_jRadioButtonMenuItem10ActionPerformed

    private void jCheckBoxMenuItem20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxMenuItem20ActionPerformed
        // spin
        if (jCheckBoxMenuItem20.isSelected()) {
            String controller = "spin on";
            jmolPanel.viewer.evalString(controller);
        } else {
            String controller = "spin off";
            jmolPanel.viewer.evalString(controller);
        }
    }//GEN-LAST:event_jCheckBoxMenuItem20ActionPerformed

    private void jMenuItem11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem11ActionPerformed
        // reset
        String controller1 = "spin off; if (showBoundBox or showUnitcell) {moveto 1.0 front;moveto 2.0 back;delay 1} else {boundbox on;moveto 1.0 front;moveto 2.0 back;delay 1;boundbox off}";
        jmolPanel.viewer.evalString(controller1);
    }//GEN-LAST:event_jMenuItem11ActionPerformed

    private void jMenuItem7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem7ActionPerformed
        // set work path
        new SetworkPathUI().setVisible(true);
    }//GEN-LAST:event_jMenuItem7ActionPerformed

    private void jMenuItem8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem8ActionPerformed
        //shafts help
        openHelp();
    }//GEN-LAST:event_jMenuItem8ActionPerformed

    private void openHelp() {
        Desktop desktop = Desktop.getDesktop();
        try {
            desktop.open(new File(System.getProperty("user.dir") + "\\doc\\eSHAFTS_manual_EN.html"));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Can't open help file! ", "Error", JOptionPane.OK_OPTION);
            Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void jMenuItem9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem9ActionPerformed
        // soft infor
        Desktop desktop = Desktop.getDesktop();
        try {
            desktop.browse(new URI("http://lilab.ecust.edu.cn"));
        } catch (IOException e) {
        } catch (URISyntaxException ex) {
            Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem9ActionPerformed

    private void jMenuItem10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem10ActionPerformed
        // reserve
        openHelp();
    }//GEN-LAST:event_jMenuItem10ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // Open new molecule file
        openFile();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // show h
        jButton21.setEnabled(true);
        if (showHflag == 0) {
            hideH();
        } else if (showHflag == 1) {
            showH();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void hideH() {
        showHflag = 1;
        String command = "hide Hydrogens";
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/noH.png"))); // NOI18N
        jmolPanel.viewer.evalString(command);
    }

    private void showH() {
        showHflag = 0;
        String command = "hide none; calculate hydrogens";
        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/showH.png"))); // NOI18N
        jmolPanel.viewer.evalString(command);
    }


    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // rotation
        if (rotationFlag == 0) {
            rotationFlag = 1;
            String controller = "spin on";
            jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/noSpin.png"))); // NOI18N
            jmolPanel.viewer.evalString(controller);
        } else if (rotationFlag == 1) {
            rotationFlag = 0;
            String controller = "spin off";
            jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/doSpin.png"))); // NOI18N
            jmolPanel.viewer.evalString(controller);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // Reset
        String controller = "spin off";
        jmolPanel.viewer.evalString(controller);
        String controller1 = "moveto 1.0 front";
        jmolPanel.viewer.evalString(controller1);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // bar molecular model
        MOLstru.show(jButton6, 0, jButton6.getY() + jButton6.getHeight());
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // model kit
        if (modelkitFlag == 0) {
            modelkitFlag = 1;
            String command = " modelkitmode = false";
            jButton8.setBorder(javax.swing.BorderFactory.createEtchedBorder(javax.swing.border.EtchedBorder.RAISED));
            jmolPanel.viewer.evalString(command);
        } else if (modelkitFlag == 1) {
            modelkitFlag = 0;
            String command = " modelkitmode = true";
            jButton8.setBorder(javax.swing.BorderFactory.createEtchedBorder());
            jmolPanel.viewer.evalString(command);
        }
        jButton21.setEnabled(true);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        // select molecule threshold
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            threshold = (String) jComboBox1.getSelectedItem();
        }
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jComboBox2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox2ItemStateChanged
        // choose screen program:FeatureAlign or ShapeFilter
        switch ((String) jComboBox2.getSelectedItem()) {
            case "SHAFTS...":
                programModel = "FeatureAlign";
                break;
            case "USR...":
                programModel = "ShapeFilter";
                break;
        }
        jComboBox2.setToolTipText((String) jComboBox2.getSelectedItem());
    }//GEN-LAST:event_jComboBox2ItemStateChanged

    private void jRadioButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton3ActionPerformed
        jComboBox3.setEnabled(true);
        jComboBox4.setSelectedIndex(0);
        jComboBox4.setEnabled(false);
    }//GEN-LAST:event_jRadioButton3ActionPerformed

    private void jRadioButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButton4ActionPerformed
        jComboBox3.setEnabled(false);
        jComboBox3.setSelectedIndex(0);
        jComboBox4.setEnabled(true);
    }//GEN-LAST:event_jRadioButton4ActionPerformed

    private void jComboBox3ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox3ItemStateChanged
        // choose target database
        if (evt.getStateChange() == ItemEvent.SELECTED) {
           String screenDB1 = (String) jComboBox3.getSelectedItem();
            screenDB=StringUtils.substringBefore(screenDB1, "(");//org 没有截取指定字符串这个功能  Song 2017.6.12.目的是为了使传入到chemMapper的参数不含数据库括号后的为了展示的内容
            screenModel = 0;
            nodePlace = 2; // add to the TargetNavigator
            jComboBox3.setToolTipText((String) jComboBox3.getSelectedItem());
        }
    }//GEN-LAST:event_jComboBox3ItemStateChanged

    public void enableButton() {
        jButton2.setEnabled(true);
        jButton3.setEnabled(true);
        jButton4.setEnabled(true);
        jButton5.setEnabled(true);
        jButton6.setEnabled(true);
        jButton7.setEnabled(true);
        jButton8.setEnabled(true);
        jButton18.setEnabled(true);
        jMenu3.setEnabled(true);
        jMenuItem12.setEnabled(true);
        // jButton13.setEnabled(true);
    }

    public void disableButton() {
        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton4.setEnabled(false);
        jButton5.setEnabled(false);
        jButton6.setEnabled(false);
        jButton7.setEnabled(false);
        jButton8.setEnabled(false);
        jButton18.setEnabled(false);
        jMenu3.setEnabled(false);
        jMenuItem12.setEnabled(false);
        jButton13.setEnabled(false);
        jButton21.setEnabled(false);
        jButton22.setEnabled(false);
        jButton23.setEnabled(false);
        jToggleButton1.setEnabled(false);
    }

    private void jComboBox4ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox4ItemStateChanged
        // choose screen database
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            String screenDB2 = (String) jComboBox4.getSelectedItem();
            screenDB=StringUtils.substringBefore(screenDB2, "(");//org 没有截取指定字符串这个功能 Song 2017.6.12.目的是为了使传入到chemMapper的参数不含数据库括号后为了展示的内容
            screenModel = 1;
            nodePlace = 3;// add to the HitExplorer
            jComboBox4.setToolTipText((String) jComboBox4.getSelectedItem());
        }
    }//GEN-LAST:event_jComboBox4ItemStateChanged

    //select chain
    private void jComboBox5ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox5ItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            String myChain = (String) jComboBox5.getSelectedItem();
            if(isCanZoom){
                jmolPanel.viewer.evalString("zoom out;center all");
                isCanZoom = false;
            }
            if (myChain.equals("Select chain...")) {
                jmolPanel.viewer.evalString("select all;spacefill off;wireframe 0.1; dots off");
                jButton14.setEnabled(false);
                jComboBox6.setSelectedIndex(0);
            } else {
                String[] selectchain = myChain.split(":");
                chainid = selectchain[1].trim();
                String command = "ligandResidueIds={hetero and !(water) and chain='" + chainid + "'}.label('%[resno]');print ligandResidueIds";
                String ligand = jmolPanel.viewer.scriptWaitStatus(command, suffixJmol).toString();
                String[] oldligandid = ligand.split("\n");
                List<String> list = new ArrayList<>();
                list.add("Select ligand...");
                if (ligand.equals("")) {
                    list.add("ligand: <Undifined>");
                } else {
                    for (String oldligandid1 : oldligandid) {
                        //remove duplicate
                        if (!list.contains("ligand: " + oldligandid1)) {
                            list.add("ligand: " + oldligandid1);
                        }
                    }
                }
                String[] ligandID = list.toArray(new String[1]);
                jComboBox6.setModel(new javax.swing.DefaultComboBoxModel(ligandID));
            }
        }
    }//GEN-LAST:event_jComboBox5ItemStateChanged

    //select ligand
    private void jComboBox6ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox6ItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED) {
            String Ligandid = (String) jComboBox6.getSelectedItem();
            if(isCanZoom){
                jmolPanel.viewer.evalString("zoom out;center all");
                isCanZoom = false;
            }
            if (!Ligandid.equals("Select ligand...")) {
                String[] id = Ligandid.split(":");
                ligandid = id[1].trim();
                if (!ligandid.equals("<Undifined>")) {
                    String command = "select " + ligandid + " and chain='" + chainid + "';restrict bonds not selected;select not selected;spacefill 23%AUTO;wireframe 0.2;color cpk;center " + ligandid + "; zoomto in; dots on";
                    jmolPanel.viewer.evalString(command);
                    jButton14.setEnabled(true);
                    isCanZoom = true;
                }
            } else {
                jmolPanel.viewer.evalString("select all;spacefill off;wireframe 0.1; dots off");
                jButton14.setEnabled(false);
            }
        }
    }//GEN-LAST:event_jComboBox6ItemStateChanged

    private void jButton15MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton15MouseEntered
        jButton15.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, java.awt.Color.darkGray, java.awt.Color.black, null, null));
    }//GEN-LAST:event_jButton15MouseEntered

    private void jButton15MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton15MouseExited
        jButton15.setBorder(null);
    }//GEN-LAST:event_jButton15MouseExited

    /**
     * open file
     */
    public void openFile() {
        filePath = null;
        String info;
        filePath = getOpenFilePath();
        if (filePath != null) {
            initPara();
            enableButton();
            molType = "NORMAL";
            ArrayList inforList = new ArrayList();
            File f = new File(filePath);
            String fileType = (f.getName().split("\\.", 2))[1];
            if (fileType.equals("pdb.gz") || fileType.equals("pdb")) {
                if (f.length() > 10000) { //>10kb
                    String tempPath = new PdbDecompress().decompress(filePath);
                    if (tempPath != null) {
                        inforList.add("PDB name:" + f.getName());
                        inforList = PdbAnalysis.getInfor(tempPath, inforList);
                    }
                    molType = "PDB";
                    String loadpdb = "load " + filePath + ";select not protein and not solvent;spacefill off;select not selected;cpk off; select carbon/1.1 color [0,255,255]";
                    jmolPanel.viewer.scriptWait(loadpdb);
                    
                    jComboBox5.setModel(new javax.swing.DefaultComboBoxModel(jmolPanel.getchains()));  //org 没有这行 Song 2017.6.13 修改打开蛋白质文件不能解析的问题
                    jButton13.setEnabled(true);
                    jButton22.setEnabled(true);
                } else {
                    String loadpdb = "load " + filePath;// + ";select not protein and not solvent;spacefill off;select not selected;cpk off";                    
                    jmolPanel.viewer.scriptWait(loadpdb);
                    inforList.add("Ligand name:" + f.getName());
                    inforList.add("Atom num:" + jmolPanel.viewer.getAtomCount());
                    inforList.add("Bonds num:" + jmolPanel.viewer.getBondCount());
                }
                info = InfoFactory.setInfor(inforList);
            } else {
                jmolPanel.viewer.openFile(filePath);
                jmolPanel.viewer.evalString("hide Hydrogens");
                if (jmolPanel.viewer.getModelCount() > 1) {//more than one mols
                    info = InfoFactory.setInfo(" The file contains more than one molecule."
                            + " Please re-upload a standard file including only one molecule,"
                            + " and then submit the job!");
                    molType = "MULTI";
                } else {
                    inforList.add("Ligand name:" + f.getName());
                    inforList.add("Atom num:" + jmolPanel.viewer.getAtomCount());
                    inforList.add("Bonds num:" + jmolPanel.viewer.getBondCount());
                    info = InfoFactory.setInfor(inforList);
                }
            }
            jEditorPane1.setText(info);
            jEditorPane1.setCaretPosition(0);
            jTextField1.setText((new File(filePath)).getName());
            jTextField1.setToolTipText(filePath);
        }
    }

    public void saveFile(String oldPath, String fileName) {
        int modelCount = jmolPanel.viewer.getModelCount();
        if (modelCount == 0 || "".equals(fileName)) {
            tipPanel1.removeAll();
            tipPanel1.add(new TipPanel("no file to save", "Error"));
        } else if (modelCount > 1) {
            tipPanel1.removeAll();
            tipPanel1.add(new TipPanel("can't save multi-molecules at the save time", "Error"));
        } else {
            String[] fileInfo = fileName.split("\\.");
            JFileChooser jfc = new JFileChooser(defaultPath);//defaultPath
            jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            jfc.setAcceptAllFileFilterUsed(false);
            //mol. mol2. sd. sdf. pdb. xyz. smi.
            jfc.addChoosableFileFilter(new MyFileFilter(new String[]{fileInfo[1]}, "(*." + fileInfo[1] + ")"));
            int result = jfc.showSaveDialog(this);
            if (result == JFileChooser.APPROVE_OPTION) {
                String newPath = jfc.getSelectedFile().getAbsolutePath() + "." + fileInfo[1];
//                String comm = "write " + fileInfo[1] + " " + path + "." + fileInfo[1];
                String execute = copyFile(oldPath, newPath);
                tipPanel1.removeAll();
                tipPanel1.add(new TipPanel(execute, "Error"));
            }
        }
    }

    private String copyFile(String oldPath, String newPath) {
        String tip = null;
        int bytesum = 0;
        int byteread;
        File oldfile = new File(oldPath);
        if (oldfile.exists()) {
            InputStream inStream;
            try {
                inStream = new FileInputStream(oldPath);
                FileOutputStream fs = new FileOutputStream(newPath);
                byte[] buffer = new byte[1024 * 5];
                while ((byteread = inStream.read(buffer)) != -1) {
                    bytesum += byteread;
                    System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
                inStream.close();
                tip = "Success! file save to: " + newPath;
            } catch (FileNotFoundException ex) {
                tip = "Failed save file!";
                Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                tip = "Failed save file!";
                Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return tip;
    }

    /**
     * get open special file path
     *
     * @return
     */
    public String getOpenFilePath() {
        String path = null;
        JFileChooser jfc = new JFileChooser(defaultPath);//defaultPath

        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        jfc.setAcceptAllFileFilterUsed(false);
        //mol. mol2. sd. sdf. pdb. xyz. smi.
        jfc.addChoosableFileFilter(new MyFileFilter(new String[]{"xyz", "smi", "pdb.gz", "pdb", "sd", "sdf", "mol2", "mol"}, "Any format(*.*)"));
        jfc.addChoosableFileFilter(new MyFileFilter(new String[]{"mol"}, "MDL MOL format(*.mol)"));
        jfc.addChoosableFileFilter(new MyFileFilter(new String[]{"mol2"}, "Sybyl Mol2 format(*.mol2)"));
        jfc.addChoosableFileFilter(new MyFileFilter(new String[]{"sd", "sdf"}, "MDL MOL format(*.sd *.sdf)"));
        jfc.addChoosableFileFilter(new MyFileFilter(new String[]{"pdb"}, "Protein Data Bank format(*.pdb)"));
        jfc.addChoosableFileFilter(new MyFileFilter(new String[]{"pdb.gz"}, "Protein Data Bank format(*.pdb.gz)"));
        jfc.addChoosableFileFilter(new MyFileFilter(new String[]{"xyz"}, "XYZ cartesian coordinates format(*.xyz)"));
        jfc.addChoosableFileFilter(new MyFileFilter(new String[]{"smi"}, "SMILES format(*.smi)"));

        int result = jfc.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            path = jfc.getSelectedFile().getAbsolutePath();
            if (new File(path).isDirectory()) {
                path = null;
            } else if (path.contains(" ") || CharacterValidate.isContainChinese(path)) {
                String content;
                if (path.contains(" ")) {
                    content = "The file path contain blank spaces! This may cause an "
                            + "\n undesired exception. Are you sure to open it?";
                } else {
                    content = "The file path contain Chinese character! This may cause an "
                            + "\n undesired exception. Are you sure to open it?";
                }
                int confirm = JOptionPane.showConfirmDialog(null, content, "warning", JOptionPane.OK_CANCEL_OPTION);
                if (confirm == JOptionPane.CANCEL_OPTION) {
                    path = null;
                }
            }
        }
        if (path != null) {
            defaultPath = path;
        }
        return path;
    }

    public void initPanel() {

        jmolPanel.viewer.evalString("zap all");
        resultShowPanel.removeAll();
        //jTextPane1.setText(softwareInfo);
        jScrollPane3 = new javax.swing.JScrollPane();
        jScrollPane3.setViewportView(jTextPane1);
        resultShowPanel.add(jScrollPane3);
        resultShowPanel.updateUI();
        jEditorPane1.setText(InfoFactory.baseInfor());
    }

    public void initPara() {
        jmolPanel.viewer.evalString("zap all");
        disableButton();
        jButton11.setVisible(false);
        initNetJobPanel();
        initNativeJobPanel();
        initPDB_Panel();
        initParaPanel();
        initPanel();
        initJmolPanel();
    }

    private void initJmolPanel() {

    }

    private void initNetJobPanel() {
        jComboBox2.setSelectedIndex(0);
        jComboBox2.setEnabled(false);
        jComboBox3.setSelectedIndex(0);
        jComboBox3.setEnabled(false);
        jComboBox4.setSelectedIndex(0);
        jComboBox4.setEnabled(false);
        jRadioButton3.setSelected(false);
        jRadioButton4.setSelected(false);
    }

    private void initNativeJobPanel() {
        jTextField3.setText("");
        jTextField3.setToolTipText(null);
        jCheckBox1.setSelected(false);
        jLabel15.setVisible(false);
        jTextField5.setVisible(false);
    }

    private void initPDB_Panel() {
        jTextField4.setText("");
        jButton13.setEnabled(false);
        jComboBox5.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"Select chain..."}));
        jComboBox6.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"Select ligand..."}));
        jButton14.setEnabled(false);
    }

    private void minimize() {
        String command = "minimize";
        jmolPanel.viewer.evalString(command);
    }

    private void jTabbedPane1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane1StateChanged
        int selectIndex = jTabbedPane1.getSelectedIndex();
        switch (selectIndex) {
            case 0:
                //local model
                shaftsModel = 1;
                jEditorPane1.setText(InfoFactory.localDes());
                jEditorPane1.setCaretPosition(0);
                break;
            case 1:
                //remote model
                shaftsModel = 2;
                jEditorPane1.setText(InfoFactory.remoteDes());
                jEditorPane1.setCaretPosition(0);
                break;
            case 2:
                //PDB
                shaftsModel = 0;//should tip for choose a model
                jEditorPane1.setText(InfoFactory.baseInfor());
                jEditorPane1.setCaretPosition(0);
                break;
        }
    }//GEN-LAST:event_jTabbedPane1StateChanged

    private void jLabel10MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseEntered
        // TODO add your handling code here:
        jLabel10.setForeground(new java.awt.Color(255, 102, 0));
    }//GEN-LAST:event_jLabel10MouseEntered

    private void jLabel10MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel10MouseExited
        // TODO add your handling code here:
        jLabel10.setForeground(new java.awt.Color(0, 0, 255));
    }//GEN-LAST:event_jLabel10MouseExited

    private void jToggleButton1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jToggleButton1MouseEntered
        // TODO add your handling code here:
        if (jToggleButton1.isSelected()) {
            jToggleButton1.setToolTipText("Hide VDW");
        } else {
            jToggleButton1.setToolTipText("Show VDW");
        }
    }//GEN-LAST:event_jToggleButton1MouseEntered

    private void jButton20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton20ActionPerformed
        // TODO add your handling code here:
        jmolPanel.viewer.evalString("undo");
        int atomNum = jmolPanel.viewer.getAtomCount();
        if (atomNum == 0) {
            initPara();
        } else {
            enableButton();
        }
    }//GEN-LAST:event_jButton20ActionPerformed

    private void jButton19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton19ActionPerformed
        // TODO add your handling code here:
        jmolPanel.viewer.evalString("redo");
        int atomNum = jmolPanel.viewer.getAtomCount();
        if (atomNum != 0) {
            enableButton();
        } else {
            initPara();
        }
    }//GEN-LAST:event_jButton19ActionPerformed

    private void jToggleButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton2ActionPerformed
        // TODO add your handling code here:
        if (jmolPanel.viewer.getAtomCount() != 0) {
            if (jToggleButton2.isSelected()) {
                jmolPanel.viewer.evalString("center all; zoomto 0.7 out");
            } else {
                jmolPanel.viewer.evalString("zoomto 0.5 in");
            }
        }
    }//GEN-LAST:event_jToggleButton2ActionPerformed

    /**
     * pdb structure
     *
     * @param evt
     */
    private void jButton22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton22ActionPerformed
        // show pdb structure:
        PDBstru.show(jButton22, 0, jButton22.getY() + jButton22.getHeight());

    }//GEN-LAST:event_jButton22ActionPerformed

    /**
     * pdb structure: wireframe
     *
     * @param evt
     */
    private void wireframeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wireframeActionPerformed
        // TODO add your handling code here:
        //pdbStru = "wireframe";
        jmolPanel.viewer.evalString("cartoons off; strands off; wireframe 0.1; color inherit");
        jButton22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/wireframe.png"))); // NOI18N
        //jButton22.setToolTipText("wireframe");
    }//GEN-LAST:event_wireframeActionPerformed

    /**
     * pdb structure: cartoon
     *
     * @param evt
     */
    private void cartoonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cartoonActionPerformed
        // TODO add your handling code here:
        jmolPanel.viewer.evalString("cartoons only; color group");
        jButton22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/cartoon.png"))); // NOI18N
        //jButton22.setToolTipText("cartoon");

    }//GEN-LAST:event_cartoonActionPerformed

    /**
     * pdb structure: strands
     *
     * @param evt
     */
    private void strandsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_strandsActionPerformed
        // TODO add your handling code here:
        jmolPanel.viewer.evalString("strands only; color chain");
        jButton22.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/strands.png"))); // NOI18N
        //jButton22.setToolTipText("strands");
    }//GEN-LAST:event_strandsActionPerformed

    /**
     * MOL structure: ball and stick
     *
     * @param evt
     */
    private void ball_stcikStyleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ball_stcikStyleActionPerformed
        // TODO add your handling code here:
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/ball_stick.png"))); // NOI18N
        String command = "select all; spacefill 25%AUTO;wireframe 0.15";
        jmolPanel.viewer.evalString(command);
        //jButton6.setToolTipText("ball and stick");
    }//GEN-LAST:event_ball_stcikStyleActionPerformed

    /**
     * MOL structure: sticks
     *
     * @param evt
     */
    private void stickStyleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_stickStyleActionPerformed
        // TODO add your handling code here:
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/bond.png"))); // NOI18N
        String command = "select all; spacefill off;cpk off;wireframe 0.2";
        jmolPanel.viewer.evalString(command);
        //jButton6.setToolTipText("bond");
    }//GEN-LAST:event_stickStyleActionPerformed

    /**
     * MOL structure: wireframe
     *
     * @param evt
     */
    private void wireframeStyleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wireframeStyleActionPerformed
        // TODO add your handling code here:
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/lineframe.png"))); // NOI18N
        String command = "select all; spacefill off;cpk off;wireframe on";
        jmolPanel.viewer.evalString(command);
        //jButton6.setToolTipText("wireframe");
    }//GEN-LAST:event_wireframeStyleActionPerformed

    /**
     * MOL structure: cpk
     *
     * @param evt
     */
    private void cpkStyleActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cpkStyleActionPerformed
        // TODO add your handling code here:
        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/cpkSpacefill.png"))); // NOI18N
        String command = "select all; spacefill off;wireframe off;cpk on";
        jmolPanel.viewer.evalString(command);
        //jButton6.setToolTipText("CPK");
    }//GEN-LAST:event_cpkStyleActionPerformed

    private void jCheckBox2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBox2MouseEntered
        // TODO add your handling code here:
        tipUI.dispose();
        tipX = evt.getXOnScreen();
        tipY = evt.getYOnScreen();
        tipUI.setLocation(tipX, tipY - 100);
        tipUI.setVisible(true);
    }//GEN-LAST:event_jCheckBox2MouseEntered

    private void jCheckBox2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jCheckBox2MouseExited

        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        if (!(x > tipX && x < tipX + 260 && y > tipY - 80 && y < tipY)) {
            tipUI.dispose();
        }
    }//GEN-LAST:event_jCheckBox2MouseExited

    /**
     * other seetings
     *
     * @param evt
     */
    private void jMenuItem6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem6ActionPerformed
        new SettingsUI().setVisible(true);
    }//GEN-LAST:event_jMenuItem6ActionPerformed

    /**
     * close current job
     *
     * @param evt
     */
    private void jMenuItem13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem13ActionPerformed
        initPara();
        jButton11.setVisible(false);
    }//GEN-LAST:event_jMenuItem13ActionPerformed

    private void jMenuItem14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem14ActionPerformed
        minimize();
    }//GEN-LAST:event_jMenuItem14ActionPerformed

    private void jMenuItem24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem24ActionPerformed
        hideH();
    }//GEN-LAST:event_jMenuItem24ActionPerformed

    private void jMenuItem15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem15ActionPerformed
        wireframeStyleActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem15ActionPerformed

    private void jMenuItem16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem16ActionPerformed
        ball_stcikStyleActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem16ActionPerformed

    private void jMenuItem17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem17ActionPerformed
        stickStyleActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem17ActionPerformed

    private void jMenuItem18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem18ActionPerformed
        cpkStyleActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem18ActionPerformed

    private void jMenuItem23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem23ActionPerformed
        showH();
    }//GEN-LAST:event_jMenuItem23ActionPerformed

    private void jMenuItem19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem19ActionPerformed
        wireframeActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem19ActionPerformed

    private void jMenuItem20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem20ActionPerformed
        cartoonActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem20ActionPerformed

    private void jMenuItem21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem21ActionPerformed
        strandsActionPerformed(evt);
    }//GEN-LAST:event_jMenuItem21ActionPerformed

    /**
     * about net comoputing
     *
     * @param evt
     */
    private void jLabel12MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseEntered
        tipUI = new MoreTipUI("Computing");
        tipUI.dispose();
        tipX = evt.getXOnScreen();
        tipY = evt.getYOnScreen();
        tipUI.setLocation(tipX - 300, tipY);
        tipUI.setVisible(true);
    }//GEN-LAST:event_jLabel12MouseEntered

    private void jLabel12MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel12MouseExited
        int x = evt.getXOnScreen();
        int y = evt.getYOnScreen();
        if (x > tipX + 30 || x < tipX - 300 || y > tipY + 150 || y < tipY) {
            tipUI.dispose();
        }
    }//GEN-LAST:event_jLabel12MouseExited

    private void jLabel13MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel13MouseClicked
        // Application for trial
        openHelp();
    }//GEN-LAST:event_jLabel13MouseClicked

    //open new file
    private void jButton24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton24ActionPerformed
        openFile();
    }//GEN-LAST:event_jButton24ActionPerformed

    //OpenBabel protocol
    private void jMenuItem22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem22ActionPerformed
        Desktop desktop = Desktop.getDesktop();
        try {
            desktop.browse(new URI("http://www.gnu.org/copyleft/fdl.html"));
        } catch (IOException e) {
        } catch (URISyntaxException ex) {
            Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem22ActionPerformed
    //JChemPaint protocol
    private void jMenuItem25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem25ActionPerformed
        Desktop desktop = Desktop.getDesktop();
        try {
            desktop.browse(new URI("http://opensource.org/licenses/lgpl-2.1.php"));
        } catch (IOException e) {
        } catch (URISyntaxException ex) {
            Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem25ActionPerformed
    //Jmol protocol
    private void jMenuItem26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem26ActionPerformed
        Desktop desktop = Desktop.getDesktop();
        try {
            desktop.browse(new URI("http://wiki.jmol.org/index.php/License"));
        } catch (IOException e) {
        } catch (URISyntaxException ex) {
            Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }//GEN-LAST:event_jMenuItem26ActionPerformed

    private void jobStatus1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jobStatus1MouseEntered
        jobStatus1.setForeground(new java.awt.Color(153, 0, 153));
    }//GEN-LAST:event_jobStatus1MouseEntered

    private void jobStatus1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jobStatus1MouseExited
        jobStatus1.setForeground(new java.awt.Color(0, 0, 0));
    }//GEN-LAST:event_jobStatus1MouseExited

    private void jCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox1ActionPerformed
        if (jCheckBox1.isSelected()) {
            String conformerParam = cyndi.getParam();
            jTextField5.setText(conformerParam);
            jLabel15.setVisible(true);
            jTextField5.setVisible(true);
        } else {
            jLabel15.setVisible(false);
            jTextField5.setVisible(false);
        }
    }//GEN-LAST:event_jCheckBox1ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton25ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButton25ActionPerformed

    private void initParaPanel() {
        jTextField1.setText("");
        jTextField1.setToolTipText(null);
        jComboBox1.setSelectedItem("1.2");
        jTextField2.setText("1000");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainUI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainUI().setVisible(true);
            }
        });
    }
    
    private boolean isCanZoom = false;
    public JmolPanel jmolPanel;
    public String filePath; // current molecule path
    protected String molType;
    public static String defaultPath; //the default path of the workspace
    public String threshold = "1.2";
    public String programModel = "FeatureAlign";
    public String screenDB;
    private String suffixJmol;
    public String chainid;
    public String ligandid;
    public String pdbCode = null; //get the input pdb code
    public String softwareInfo;
    private final String pdbPath = System.getProperty("user.dir") + "\\files\\PDB\\";
    private MoreTipUI tipUI;
    public int screenModel;
    public int nodePlace;
    public int showHflag = 1; //the flag of show H
    public int rotationFlag = 0;
    public int isbarFlag = 0;
    public int modelkitFlag = 0;
    public int shaftsModel = 1;   //the type of job, default is local
    //private final String pdbStru = "wireframe";
    public IconNode treeRoot;
    public IconNode localworkNode;
    public IconNode networkNode;
    public IconNode targetNode;
    public IconNode hitNode;
    public Cyndi cyndi;
    //public static String openFilePath = null; 
    private int tipX;
    private int tipY;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel BottomPanel;
    private javax.swing.JPopupMenu MOLstru;
    private javax.swing.JPopupMenu PDBstru;
    private javax.swing.JPanel TopPanel;
    private javax.swing.JMenuItem ball_stcikStyle;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.JMenuItem cartoon;
    private javax.swing.JPanel centerPanel;
    private javax.swing.JPanel centerPanelChild1;
    public javax.swing.JMenuItem checkInfo;
    private javax.swing.JMenuItem cpkStyle;
    private javax.swing.JPanel eastBottomPanel;
    private javax.swing.JPanel eastPanel;
    private javax.swing.JPanel eastTopPanel;
    public javax.swing.JButton jButton1;
    public javax.swing.JButton jButton10;
    public javax.swing.JButton jButton11;
    public javax.swing.JButton jButton12;
    public javax.swing.JButton jButton13;
    public javax.swing.JButton jButton14;
    public javax.swing.JButton jButton15;
    public javax.swing.JButton jButton16;
    public javax.swing.JButton jButton17;
    public javax.swing.JButton jButton18;
    private javax.swing.JButton jButton19;
    public javax.swing.JButton jButton2;
    private javax.swing.JButton jButton20;
    public javax.swing.JButton jButton21;
    public javax.swing.JButton jButton22;
    public javax.swing.JButton jButton23;
    private javax.swing.JButton jButton24;
    public javax.swing.JButton jButton25;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    public javax.swing.JButton jButton9;
    public javax.swing.JCheckBox jCheckBox1;
    public javax.swing.JCheckBox jCheckBox2;
    public javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem1;
    public javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem2;
    public javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem20;
    public javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem3;
    public javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem4;
    public javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem5;
    public javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem6;
    public javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem7;
    public javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem8;
    public javax.swing.JCheckBoxMenuItem jCheckBoxMenuItem9;
    public javax.swing.JComboBox jComboBox1;
    public javax.swing.JComboBox jComboBox2;
    public javax.swing.JComboBox jComboBox3;
    public javax.swing.JComboBox jComboBox4;
    public javax.swing.JComboBox jComboBox5;
    public javax.swing.JComboBox jComboBox6;
    public javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    protected javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    public javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu10;
    private javax.swing.JMenu jMenu11;
    private javax.swing.JMenu jMenu12;
    private javax.swing.JMenu jMenu2;
    public javax.swing.JMenu jMenu3;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu5;
    private javax.swing.JMenu jMenu6;
    private javax.swing.JMenu jMenu7;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenu jMenu9;
    private javax.swing.JMenuBar jMenuBar1;
    public javax.swing.JMenuItem jMenuItem1;
    public javax.swing.JMenuItem jMenuItem10;
    private javax.swing.JMenuItem jMenuItem11;
    public javax.swing.JMenuItem jMenuItem12;
    private javax.swing.JMenuItem jMenuItem13;
    private javax.swing.JMenuItem jMenuItem14;
    private javax.swing.JMenuItem jMenuItem15;
    private javax.swing.JMenuItem jMenuItem16;
    private javax.swing.JMenuItem jMenuItem17;
    private javax.swing.JMenuItem jMenuItem18;
    private javax.swing.JMenuItem jMenuItem19;
    public javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JMenuItem jMenuItem20;
    private javax.swing.JMenuItem jMenuItem21;
    private javax.swing.JMenuItem jMenuItem22;
    private javax.swing.JMenuItem jMenuItem23;
    private javax.swing.JMenuItem jMenuItem24;
    private javax.swing.JMenuItem jMenuItem25;
    private javax.swing.JMenuItem jMenuItem26;
    public javax.swing.JMenuItem jMenuItem3;
    public javax.swing.JMenuItem jMenuItem4;
    public javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JMenuItem jMenuItem6;
    public javax.swing.JMenuItem jMenuItem7;
    public javax.swing.JMenuItem jMenuItem8;
    public javax.swing.JMenuItem jMenuItem9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JRadioButton jRadioButton1;
    public javax.swing.JRadioButton jRadioButton2;
    public javax.swing.JRadioButton jRadioButton3;
    public javax.swing.JRadioButton jRadioButton4;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem1;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem10;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem2;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem3;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem4;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem5;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem6;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem7;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem8;
    private javax.swing.JRadioButtonMenuItem jRadioButtonMenuItem9;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JScrollPane jScrollPane2;
    protected javax.swing.JScrollPane jScrollPane3;
    public javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JSplitPane jSplitPane3;
    private javax.swing.JSplitPane jSplitPane4;
    private javax.swing.JTabbedPane jTabbedPane1;
    public javax.swing.JTextField jTextField1;
    public javax.swing.JTextField jTextField2;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField4;
    protected javax.swing.JTextField jTextField5;
    protected javax.swing.JTextPane jTextPane1;
    public javax.swing.JToggleButton jToggleButton1;
    private javax.swing.JToggleButton jToggleButton2;
    private javax.swing.JToolBar jToolBar1;
    public javax.swing.JToolBar jToolBar2;
    public javax.swing.JTree jTree1;
    protected javax.swing.JLabel jobStatus;
    protected javax.swing.JLabel jobStatus1;
    protected javax.swing.JLabel jobStatus2;
    private javax.swing.JLabel jobStatus3;
    protected javax.swing.JLabel jobStatus4;
    private javax.swing.JPanel leftPanel;
    public javax.swing.JPanel localJobPanel;
    private javax.swing.JPanel localjobParaPanel;
    public javax.swing.JMenuItem logOut;
    private javax.swing.JPanel molPanel;
    public javax.swing.JPanel netJobLockPanel;
    public javax.swing.JPanel netJobPanel;
    public javax.swing.JPanel netJobParaPanel;
    private javax.swing.JPanel paraPanel;
    protected javax.swing.JPanel resultShowPanel;
    private javax.swing.JPanel rightPanel;
    protected javax.swing.JPanel scanningPanel;
    private javax.swing.JPanel scanningPanel1;
    public javax.swing.JPanel southPanel;
    private javax.swing.JMenuItem stickStyle;
    private javax.swing.JMenuItem strands;
    public javax.swing.JPanel tipPanel1;
    public javax.swing.JPanel tipPanel2;
    public javax.swing.JPopupMenu userPop;
    public javax.swing.JPanel westPanel;
    private javax.swing.JMenuItem wireframe;
    private javax.swing.JMenuItem wireframeStyle;
    // End of variables declaration//GEN-END:variables
}
