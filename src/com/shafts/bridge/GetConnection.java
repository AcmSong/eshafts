/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shafts.bridge;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Little-Kitty
 * @date 2015-4-5 13:53:14
 */
public class GetConnection {

    /**
     * connect the server
     *
     * @return
     */
    public Socket connect() {
        Socket socket = null;
        final String ip = "59.78.98.102";// server IP//ori 59.78.96.61   172.20.84.11 Song 2017.6.3 服务器ip的更换
        //private final String ip = "localhost";// server IP
        final int port = 8821;  //8821
        try {
            socket = new Socket(ip, port);
            socket.setSoTimeout(60000);
            return socket;
        } catch (IOException ex) {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException ex1) {
                    Logger.getLogger(GetConnection.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            Logger.getLogger(GetConnection.class.getName()).log(Level.SEVERE, null, ex);
            boolean network = new CheckNetWork().netstatus();
            String tip;
            if (network) {
                tip = "Server error! Try it later.";
            } else {
                tip = "Connection Exception! Check your network.";
            }
            JOptionPane.showMessageDialog(null, tip, "ERROR", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }

}
