package com.shafts.bridge;

import com.shafts.utils.MyProgressBar;
import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Little-Kitty
 * @date 2014-10-28 15:54:27
 */
public class LoadPDB {

    public final static boolean DEBUG = true;
    private static final int BUFFER_SIZE = 20480;
    private final ArrayList<String> vDownLoad;
    private final ArrayList<String> vFileList;
    private MyProgressBar myBar;

    public LoadPDB() {
        this.vFileList = new ArrayList<>();
        this.vDownLoad = new ArrayList<>();
    }

    /**
     * reset
     */
    public void resetList() {
        vDownLoad.clear();
        vFileList.clear();
    }

    /**
     * @param url String
     * @param filename String
     */
    public void addItem(String url, String filename) {
        vDownLoad.add(url);
        vFileList.add(filename);
    }

    /**
     * load multi files
     */
    public void downLoadByList() {
        String url;
        String filename;
        for (int i = 0; i < vDownLoad.size(); i++) {
            url = (String) vDownLoad.get(i);
            filename = (String) vFileList.get(i);
            try {
                saveToFile(url, filename);
            } catch (IOException err) {
                if (DEBUG) {
                    System.out.println("[" + url + "] invalid!!!");
                }
            }
        }
        if (DEBUG) {
            System.out.println("Success!!!");
        }
    }

    /**
     * @param destUrl String
     * @param fileName String
     * @throws java.io.IOException
     */
    public void saveToFile(String destUrl, String fileName) throws IOException {
        FileOutputStream fos;
        BufferedInputStream bis;
        HttpURLConnection httpUrl;
        URL url;
        byte[] buf = new byte[BUFFER_SIZE];
        int size;
        url = new URL(destUrl);
        httpUrl = (HttpURLConnection) url.openConnection();
        int lenth = httpUrl.getContentLength();
        httpUrl.connect();

        bis = new BufferedInputStream(httpUrl.getInputStream());
        fos = new FileOutputStream(fileName);

        while ((size = bis.read(buf)) != -1) {
            fos.write(buf, 0, size);
        }
        fos.close();
        bis.close();
        httpUrl.disconnect();
    }

    /**
     *
     * @param destUrl String
     * @param fileName String
     * @param pdbName
     * @return
     */
    public boolean saveToFile2(String destUrl, String fileName, String pdbName) {
        boolean flag = false;
        myBar = new MyProgressBar("Download PDB " + pdbName);
        FileOutputStream fos = null;
        BufferedInputStream bis = null;
        HttpURLConnection httpUrl = null;
        InputStream in = null;
        URL url;
        byte[] buf = new byte[BUFFER_SIZE];
        int size;
        try {
            // create connect            
            url = new URL(destUrl);
            HttpURLConnection.setFollowRedirects(false);
            httpUrl = (HttpURLConnection) url.openConnection();
            httpUrl.setConnectTimeout(10000);
            httpUrl.setReadTimeout(90000);
            int state = httpUrl.getResponseCode();
            System.out.println(state);
            if (state == 200 || state != 200) {
                String fileDes;
                httpUrl.connect();
                fileDes = httpUrl.getHeaderField("Content-Length");
                System.out.println(fileDes);
                if (fileDes == null) {
                    myBar.setParamNoClear();
                    myBar.setLabel(0);
                }
                myBar.setShow(true);
                in = httpUrl.getInputStream();
                bis = new BufferedInputStream(in);
                fos = new FileOutputStream(fileName);                
                int totalSize = httpUrl.getContentLength();                                               
                // save file
                int currentSize = 0;
                while ((size = bis.read(buf)) != -1) {
                    fos.write(buf, 0, size);
                    currentSize += size;
                    if (fileDes != null) {
                        myBar.setValue((currentSize * 100) / totalSize);
                    } else {
                        myBar.setLabel(currentSize / 1024);
                    }
                }
                fos.close();
                bis.close();
                in.close();
                fos = null;
                bis = null;
                in = null;                                
                flag = true;
            } else {
                JOptionPane.showMessageDialog(null, "Connect Exception! Error code: " + state, "Error", JOptionPane.OK_OPTION);
            }
        } catch (MalformedURLException ex) {
            JOptionPane.showMessageDialog(null, "Loading Failed", "Error", JOptionPane.OK_OPTION);
            Logger.getLogger(LoadPDB.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LoadPDB.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if(httpUrl != null)
                httpUrl.disconnect();
            myBar.setShow(false);
            try {
                if (fos != null) {
                    fos.close();
                }
                if (bis != null) {
                    bis.close();
                }
                if (in != null) {
                    in.close();
                }
            } catch (IOException iOException) {                
                Logger.getLogger(LoadPDB.class.getName()).log(Level.SEVERE, null, iOException);
            }
        }
        return flag;
    }

    /**
     * set proxy server
     *
     * @param proxy String
     * @param proxyPort String
     */
    public void setProxyServer(String proxy, String proxyPort) {
        // set proxy server
        System.getProperties().put("proxySet", "true");
        System.getProperties().put("proxyHost", proxy);
        System.getProperties().put("proxyPort", proxyPort);
    }

//	public void setAuthenticator(String uid, String pwd) {
//		Authenticator.setDefault(new MyAuthenticator());
//	}
    /**
     * @param argv String[]
     * @throws java.io.IOException
     */
    public static void main(String argv[]) throws IOException {
        //HttpGet oInstance = new HttpGet();        
        LoadPDB save = new LoadPDB();

        boolean flag = save.saveToFile2("http://www.rcsb.org/pdb/files/1fvv.pdb.gz",
                "1fvv.pdb.gz", "1fvv");
        System.out.println(flag);
    }
}
