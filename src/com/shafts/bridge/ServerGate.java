package com.shafts.bridge;

import com.socket.bean.InforBean;
import com.socket.bean.RegisterBean;
import com.socket.bean.StatusBean;
import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.ecust.remote.chemmapper.model.ChemmapperServiceModel;

/**
 *
 * @author Little-Kitty
 * @date 2015-4-4 16:27:29
 */
public class ServerGate {

    private Socket socket = null;
    private ObjectOutputStream os = null;
    private ObjectInputStream is = null;
    Client client= ClientBuilder.newClient();
   

    public ServerGate() {

    }

    /**
     * submit and get the register infor
     *
     * @param regBean
     * @return
     */
    public String submitInfor(String name,String password1,String password2,String cellphone, String phone,
                                String email,String fax,String country,String city,String job,String institute,
                                String address,String realName,String webUrl,String serial) {
        String regResult = null;
        String url1="http://localhost:8080/api/software/register";
        WebTarget webTarget1=client.target(url1);
        Builder builder1=webTarget1.request(MediaType.TEXT_PLAIN);
        Form form1=new Form();
        form1.param("name", name);
        form1.param("password", password1);
        form1.param("password1", password2);
        form1.param("mobile", cellphone);
        form1.param("phone", phone);
        form1.param("email", email);
        form1.param("fax",fax);
        form1.param("country", country);
        form1.param("city", city);
        form1.param("job", job);
        form1.param("webUrl", webUrl);
        form1.param("company",institute);
        form1.param("address",address);
        form1.param("serial", serial);
        Response response1=builder1.post(Entity.entity(form1, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
        regResult=response1.readEntity(String.class);
        System.out.println(regResult);
        return regResult;
    }

    /**
     * check authorize is expired
     *
     * @param userName
     * @return
     * 这个功能没有用到,注释掉了 Song 2017.10.20
     */
//   public String isExpired(String userName) {
////        getConnection = new GetConnection();
//        String isExpired = null;
////        socket = getConnection.connection();
//        socket = GetConnection.CONNECT();
//        if (socket != null) {
//            try {
//                os = new ObjectOutputStream(socket.getOutputStream());
//                os.writeUTF("ISEXPIRED");
//                os.writeUTF(userName);
//                os.flush();
//                //get the infor form the server
//                is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
//                isExpired = is.readUTF();
//                is.close();
//                os.close();
//            } catch (IOException ex) {
//                Logger.getLogger(ServerGate.class.getName()).log(Level.SEVERE, null, ex);
//            } finally {
//                try {
//                    if (os != null) {
//                        os.close();
//                    }
//                    if (is != null) {
//                        is.close();
//                    }
//                    if (socket != null) {
//                        socket.close();
//                    }
//                } catch (IOException ex) {
//                    Logger.getLogger(ServerGate.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//        return isExpired;
//    }

    /**
     * verify the user account
     *
     * @param name
     * @param password
     * @return
     */
    public String verifyAccount(String name, String password) {
       
        String verResult = null;
        String url3="http://localhost:8080/api/software/loginVerify";
        WebTarget webTarget3=client.target(url3);
        Builder builder3=webTarget3.request(MediaType.TEXT_PLAIN);
        Form form4=new Form();
        form4.param("name", name).param("password", password);
        Response response3=builder3.post(Entity.entity(form4,MediaType.APPLICATION_FORM_URLENCODED_TYPE));
        verResult=response3.readEntity(String.class);
        return verResult;
    }

    /**
     * forget password and find it
     *
     * @param mailAddress
     * @param userName
     * @return
     */
   public String getPassword(String mailAddress, String userName) {

        String isSend = "";
        String url4="http://localhost:8080/api/software/getPassword";
        WebTarget webTarget4=client.target(url4);
        Builder builder4=webTarget4.request(MediaType.TEXT_PLAIN);
        Form form4=new Form();
        form4.param("email", mailAddress);
        form4.param("name", userName);
        Response response4=builder4.post(Entity.entity(form4, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
        isSend=response4.readEntity(String.class);
        return isSend;
    }
   

    /**
     * check the user name if existed
     *
     * @param name
     * @return
     */
    public String isNameExist(String name) {
       
        String isExisted = null;
        String url2="http://localhost:8080/api/software/isNameExist";
        WebTarget webTarget2=client.target(url2);
        Builder builder2=webTarget2.request(MediaType.TEXT_PLAIN);
        Form form2=new Form();
        form2.param("name",name);
        Response response2=builder2.post(Entity.entity(form2,MediaType.APPLICATION_FORM_URLENCODED_TYPE));
        isExisted=response2.readEntity(String.class);
        System.out.println(isExisted);
        return isExisted;
    }

    /**
     * get key file
     *
     * @param mailAddress
     * @param userName
     * @return
     */
    public String getKeyFile(String mailAddress, String userName) {
        String isSend=null;
        String url5="http://localhost:8080/api/software/getKeyFile";
        WebTarget webTarget5=client.target(url5);
        Builder buider5=webTarget5.request(MediaType.TEXT_PLAIN);
        Form form5=new Form();
        form5.param("name",userName);
        form5.param("email",mailAddress);
        Response response5=buider5.post(Entity.entity(form5, MediaType.APPLICATION_FORM_URLENCODED));
        isSend=response5.readEntity(String.class);
        System.out.println(isSend);
     return isSend;
    }
    /**
     * get the company infor
     *没有用，暂时注释掉了 Song 2017.10.20
     * @return
     */
//    public InforBean getProInfor() {
//        socket = GetConnection.CONNECT();
//        InforBean inforBean = null;
//        if (socket != null) {
//            try {
//                os = new ObjectOutputStream(socket.getOutputStream());
//                os.writeUTF("GETCOMPANYINFOR");
//                os.flush();
//                is = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
//                inforBean = (InforBean) is.readObject();
//                is.close();
//                os.close();
//            } catch (IOException | ClassNotFoundException ex) {
//                Logger.getLogger(ServerGate.class.getName()).log(Level.SEVERE, null, ex);
//            } finally {
//                try {
//                    if (os != null) {
//                        os.close();
//                    }
//                    if (is != null) {
//                        is.close();
//                    }
//                    if (socket != null) {
//                        socket.close();
//                    }
//                } catch (IOException ex) {
//                    Logger.getLogger(ServerGate.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//        return inforBean;
//    }

    /**
     * get order infor
     *
     * @param userName
     * @return
     */
   public String getStatusInfor(String userName) {
       // socket = GetConnection.CONNECT();
        String StatusString = null;
        String url6="http://localhost:8080/api/software/getStatus";
        WebTarget webTarget6=client.target(url6);
        Builder builder6=webTarget6.request(MediaType.TEXT_PLAIN);
        Form form6=new Form();
        form6.param("name",userName);
        Response response6=builder6.post(Entity.entity(form6, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
        StatusString=response6.readEntity(String.class);
        System.out.println(StatusString);
        return StatusString;
        
}


    /**
     * verify the user
     *
     * @return
     */
    public boolean verify() {
//        return true;//Song
        boolean reach = false;
        String part1 = new ClientInfo().getDst();
        String part2 = DecName.GETNAME();
        StringBuilder sb = new StringBuilder(part1);
        sb.insert(8, part2);
        String fin = sb.toString();
        String key = new OpenDoor().getBar();
        if (fin.equals(key)) {
            reach = true;
        }
        return reach;
    }

    public String getJobId(String filePath,int screenModel,String Filed2text,String programModel,String screenDB,String threshold) {
      
        String jobId = "EXCEPTION";
        String url7="http://localhost:8080/api/software/getJobId";
        WebTarget webTarget7=client.target(url7);
        Builder builder7=webTarget7.request(MediaType.TEXT_PLAIN);
        String name=DecName.GETNAME();
        System.out.println(name);
        String StringScreenModel=String.valueOf(screenModel);
        Form form7=new Form();
        form7.param("name", name);
        form7.param("filePath", filePath);
        form7.param("StringScreenModel", StringScreenModel);
        form7.param("Filed2text", Filed2text);
        form7.param("programModel", programModel);
        form7.param("screenDB", screenDB);
        form7.param("threshold", threshold);
        System.out.println("Request Form: \n" + form7.asMap().toString());
        Response response7=builder7.post(Entity.entity(form7, MediaType.APPLICATION_FORM_URLENCODED_TYPE));
        jobId=response7.readEntity(String.class);
        System.out.println("jobId  "+jobId);
        return jobId;
    }
    
    public static void main(String[] args) {
        //{name=[GoodDay], filePath=[G:\ECUST\project\test\Target\ChEBI_2896.mol2], StringScreenModel=[0], Filed2text=[1000], programModel=[FeatureAlign], screenDB=[DrugBank], threshold=[1.2]}
        ServerGate gate = new ServerGate();
        System.out.println(gate.getJobId("G:\\ECUST\\project\\test\\Target\\ChEBI_2896.mol2", 0, "1000", "FeatureAlign", "DrugBank", "1.2"));
        
    }
   
}
