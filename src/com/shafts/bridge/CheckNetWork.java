/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shafts.bridge;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Little-Kitty
 * @date 2014-11-7 15:13:57
 */
public class CheckNetWork {

    public boolean netstatus() {
        
//       return true;//Song

        Runtime runtime = Runtime.getRuntime();
        Process process;
        String line;
        InputStream is;
        InputStreamReader isr;
        BufferedReader br;
        String[] ip = new String[]{"www.ifeng.com", "www.sina.com.cn", "www.baidu.com"};
        //boolean res = false;
        try {
            for (String ip1 : ip) {
                process = runtime.exec("ping " + ip1);
                is = process.getInputStream();
                isr = new InputStreamReader(is);
                br = new BufferedReader(isr);
                while ((line = br.readLine()) != null) {
                    if (line.contains("TTL")) {
                        return true;
                        // break;
                    }
                }
                is.close();
                isr.close();
                br.close();
            }
        } catch (IOException ex) {
            Logger.getLogger(CheckNetWork.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static void main(String args[]) {
        CheckNetWork cnk = new CheckNetWork();
        boolean re = cnk.netstatus();
        System.out.println(re);
    }
}
