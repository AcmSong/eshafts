package com.shafts.bridge;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

/**
 *
 * @author Little-Kitty
 * @date 2014-9-15 12:48:53
 */
public class OpenDoor {

    /**
     *
     * @param key 
     * @param raw 
     * @return 
     * @throws Exception
     */
    public byte[] decrypt(Key key, byte[] raw) throws Exception {
        try {
            Cipher cipher = Cipher.getInstance("RSA", new org.bouncycastle.jce.provider.BouncyCastleProvider());
            cipher.init(Cipher.DECRYPT_MODE, key);
            int blockSize = cipher.getBlockSize();
            ByteArrayOutputStream bout = new ByteArrayOutputStream(64);
            int j = 0;
            while (raw.length - j * blockSize > 0) {
                bout.write(cipher.doFinal(raw, j * blockSize, blockSize));
                j++;
            }
            return bout.toByteArray();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException | IOException e) {
            Logger.getLogger(OpenDoor.class.getName()).log(Level.SEVERE, null, e);
            return null;
        }       
    }

    public static final byte[] toBytes(String s) {
        byte[] bytes;
        bytes = new byte[s.length() / 2];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) Integer.parseInt(s.substring(2 * i, 2 * i + 2),
                    16);
        }
        return bytes;
    }

    public static PrivateKey getPrivateKey(byte[] keyBytes) throws
            NoSuchAlgorithmException, InvalidKeySpecException {
        PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
        return privateKey;
    }

    public String getBar() {
        String filePath = path + "digiVeriSign.pem";
        File file = new File(filePath);
        BufferedReader bf;
        String prikey = null;
        String content = null;
        String bar = null;
        if (file.exists()) {
           
            try {
               bf = new BufferedReader(new FileReader(file));
                String temp;
                while ((temp = bf.readLine()) != null) {
                    switch (temp) {
                        case "<@Pipespace@>":
                            prikey = bf.readLine();
                            break;
                        case "<@Deepcrack@>":
                            if (content == null) {
                                content = bf.readLine();
                            } else {
                                content += bf.readLine();
                            }   break;
                    }
                }
                bf.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(OpenDoor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(OpenDoor.class.getName()).log(Level.SEVERE, null, ex);
            }
            if(prikey == null || content == null)
                return null;
            byte[] key = toBytes(prikey);
            PrivateKey Prikey;
            try {
                Prikey = getPrivateKey(key);
                RSAPrivateKey priKey = (RSAPrivateKey) Prikey;
                bar = new String(decrypt(priKey, toBytes(content)));
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(OpenDoor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeySpecException ex) {
                Logger.getLogger(OpenDoor.class.getName()).log(Level.SEVERE, null, ex);
            } catch (Exception ex) {
                Logger.getLogger(OpenDoor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return bar;
    }

    public static void main(String args[]) throws Exception {

    }
    private final String path = System.getProperty("user.dir") + "\\configuration\\PKCS#12\\";
}
