package com.shafts.utils;

import java.io.File;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Little-Kitty
 * @date 2014-11-13 13:09:52
 */
public class InitTargetJob {
    //param of delete node
    //  private static String deleteID;

    //param of update node 
    // private static String updateNumber;
    //读取传入的路径，返回一个document对象
    public static Document loadInit(String filePath) {
        Document document = null;
        try {
            File file = new File(filePath);
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            if (file.exists()) {

                document = builder.parse(new File(filePath));
                document.normalize();
            }
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(InitTargetJob.class.getName()).log(Level.SEVERE, null, ex);
        }
        return document;
    }

    /**
     * get the target data
     * @param filePath
     * @return
     */
    public String[][] getTarget(String filePath) {
        Document document = loadInit(filePath);
        //get leaf node
        NodeList nodeList = document.getElementsByTagName("target");
        row = nodeList.getLength();
        String[][] target = new String[row][6];
        for (int i = 0; i < row; i++) {
            String targetname;
            String reflink;
            String species = "Not Available";
            String simiscore;
            String score;
            String rank;
            targetname = nodeList.item(i).getChildNodes().item(0).getTextContent();
            Node rootchild = nodeList.item(i);
            NamedNodeMap attributes = rootchild.getAttributes();
            reflink = "swiss-port:" + attributes.getNamedItem("uniprot").getNodeValue();
            for (int j = 0; j < attributes.getLength(); j++) {
                if (attributes.item(j).getNodeName().equals("species")) {
                    species = attributes.getNamedItem("species").getNodeValue();
                    break;
                }
            }
            //System.out.println(attributes.item(6).getNodeName());
            simiscore = attributes.getNamedItem("simiscore").getNodeValue();
            score = attributes.getNamedItem("score").getNodeValue();
            rank = attributes.getNamedItem("rank").getNodeValue();
            //System.out.println("*****" + targetname + "*****" + reflink + "*****" + species + "*****" + simiscore + "*****" + score + "*****" + rank);
            target[i][0] = targetname;
            target[i][1] = reflink;
            target[i][2] = species;
            target[i][3] = simiscore;
            target[i][4] = score;
            target[i][5] = rank;
        }
        return target;
    }

    public int gettargetlength() {
        return row;
    }

    /**
     * 获取带注释结果的数据
     *
     * @param filePath
     * @param KeyID
     * @return
     */
    public Vector getTargetdata(String filePath, String KeyID) {
        Document document = loadInit(filePath);
        Vector data = new Vector();
        NodeList nodeList = document.getElementsByTagName("target");
        for (int i = 0; i < nodeList.getLength(); i++) {
            String targetname =nodeList.item(i).getChildNodes().item(0).getTextContent();//.item(i).getFirstChild().getNodeValue();
            if (targetname.equals(KeyID)) {//find the compounds
                //NodeList nodeList1 = document.getElementsByTagName("compounds").item(i).getChildNodes();
                NodeList nodeList1 = nodeList.item(i).getChildNodes().item(4).getChildNodes();
                for (int j = 0; j < nodeList1.getLength(); j++) {
                    Node compoundNode = nodeList1.item(j);
                    NamedNodeMap attributes = compoundNode.getAttributes();
                    String id = attributes.getNamedItem("id").getNodeValue();
                    String score = attributes.getNamedItem("score").getNodeValue();
                    String db = attributes.getNamedItem("srcdb").getNodeValue();
                    //String srcid = attributes.getNamedItem("srcid").getNodeValue();
                    //String srcdb = attributes.getNamedItem("srcid").getNodeValue();
                    NodeList compoundList = compoundNode.getChildNodes(); //find compound item
                    String name = compoundList.item(0).getFirstChild().getNodeValue();
                    String smiles = compoundList.item(1).getFirstChild().getNodeValue();
                    Node activityNode;
                    NamedNodeMap attributes1 = null;
                    String activity = "No Available";
                    if (compoundList.item(3).getFirstChild() != null) {
                        activityNode = compoundList.item(3).getFirstChild();
                        attributes1 = activityNode.getAttributes();
                    }
                    if(db.equals("DrugBank"))
                        activity = "<html><body><a href> Model of Action";
                    else if(attributes1 != null)
                        activity = attributes1.getNamedItem("type").getNodeValue() + " " + attributes1.getNamedItem("value").getNodeValue();

                    Vector rowdata = new Vector();
                    //System.out.println("*****" + id + "*****" + name + "*****" + srcdb + "*****" + activity + "*****" + score + "*****" + smiles);
                    rowdata.add(id);
                    rowdata.add(name);
                    rowdata.add("<html><body><a href>" + db + "</a></body></html>");
                    rowdata.add(activity);
                    rowdata.add(score);
                    rowdata.add(smiles);
                    rowdata.add(false);
                    data.add(rowdata);
                }
                break;
            }
        }
        return data;
    }

    /**
     * get the compound effect infor
     *
     * @param filePath
     * @param KeyID
     * @param id
     * @return
     */
    public String geteffectinfor(String filePath, String KeyID, String id) {
        Document document = loadInit(filePath);
        NodeList nodeList = document.getElementsByTagName("target");
        String effect = "NO ACTIVITY INFO";
        FLAG:
        for (int i = 0; i < nodeList.getLength(); i++) {
            String targetName = nodeList.item(i).getChildNodes().item(0).getTextContent();
            if (targetName.equals(KeyID)) {//find the compounds
                NodeList nodeList1 = nodeList.item(i).getChildNodes().item(4).getChildNodes();
                for (int j = 0; j < nodeList1.getLength(); j++) {
                    Node compoundNode = nodeList1.item(j);
                    NamedNodeMap attributes = compoundNode.getAttributes();
                    String dataId = attributes.getNamedItem("id").getNodeValue();
                    if (dataId.equals(id)) {
                        NodeList compoundList = compoundNode.getChildNodes(); //find compound item
                        effect = compoundList.item(2).getTextContent();
                        break FLAG;
                    }
                }
            }
        }
        return effect;
    }

    /**
     * get the source database
     *
     * @param filePath
     * @param KeyID
     * @param id
     * @return
     */
    public String getsrcid(String filePath, String KeyID, String id) {
        Document document = loadInit(filePath);
        String srcid = null;
        NodeList nodeList = document.getElementsByTagName("target");
        POS:
        for (int i = 0; i < nodeList.getLength(); i++) {
            String targetName = nodeList.item(i).getChildNodes().item(0).getTextContent();//document.getElementsByTagName("name").item(i).getFirstChild().getNodeValue();
            if (targetName.equals(KeyID)) {//find the compounds
                NodeList nodeList1 = nodeList.item(i).getChildNodes().item(4).getChildNodes();;
                for (int j = 0; j < nodeList1.getLength(); j++) {
                    Node compoundNode = nodeList1.item(j);
                    NamedNodeMap attributes = compoundNode.getAttributes();
                    String dataid = attributes.getNamedItem("id").getNodeValue();
                    if (dataid.equals(id)) {
                        srcid = attributes.getNamedItem("srcid").getNodeValue();
                        break POS;
                    }
                }
            }
        }
        return srcid;
    }

    /**
     * 获取goid并解析成html
     *
     * @param filePath
     * @param KeyID name
     * @return
     */
    public String getHtml(String filePath, String KeyID) {
        StringBuilder html = new StringBuilder();
        boolean isGo = false;
        Document document = loadInit(filePath);
        //get leaf node
        NodeList nodeList = document.getElementsByTagName("target");   
        for (int i = 0; i < nodeList.getLength(); i++) {
            //String name = document.getElementsByTagName("name").item(i).getFirstChild().getNodeValue();
            String name = nodeList.item(i).getChildNodes().item(0).getTextContent();
            Node rootchild = nodeList.item(i);
            NamedNodeMap attributes = rootchild.getAttributes();
            String proc = attributes.getNamedItem("proc").getNodeValue();
            if (name.equals(KeyID) && !proc.equals("Other")) {//存在相关的Go标签
                //html"";
                isGo = true;
                html.append("<html>\n");
                html.append("<body bgcolor=\"#F5F5DC\" text=\"#0000AA \">\n ");
                html.append("<font face=\"黑体\" ><b> GeneID: </b></font><a href=\"http://www.ncbi.nlm.nih.gov/gene/").append(attributes.getNamedItem("geneid").getNodeValue()).append("\">").append(attributes.getNamedItem("geneid").getNodeValue()).append("</a><br>\n");
                html.append("<font face=\"黑体\"><b>Gene Name:</b></font>").append(attributes.getNamedItem("genename").getNodeValue()).append("<br>\n");
                html.append("<font face=\"黑体\"><b>Synonyms:</b></font>").append(attributes.getNamedItem("genesyn").getNodeValue()).append("<br>\n");
                html.append("<font face=\"黑体\"><b>Function:</b></font><br>\n");
                html.append("<p style=\"text-align:justify\">");
                html.append(document.getElementsByTagName("function").item(i).getFirstChild().getNodeValue()).append("<br>\n");
                NodeList nodeList1 = document.getElementsByTagName("go_list").item(i).getChildNodes();
                html.append("</p>");
                Node pathwaylist = document.getElementsByTagName("pathway_list").item(i).getFirstChild();
                if (pathwaylist != null) {
                    NamedNodeMap attributes2 = pathwaylist.getAttributes();
                    String id = attributes2.getNamedItem("id").getNodeValue();
                    String pathwaylink = attributes2.getNamedItem("link").getNodeValue();
                    html.append("<font face=\"黑体\"><b>Pathway:</b> </font><br>\n");
                    html.append("<table width=\"80%\" border=\"1\">\n");
                    html.append("<tr><th><font face=\"黑体\"> Reactom ID </font></th>\n");
                    html.append("<th><font face=\"黑体\"> Name </font></th></tr>\n");
                    html.append("<tr><th><a href=\"").append(pathwaylink).append("\">").append(id).append("</a></th>\n");
                    html.append("<th>").append(pathwaylist.getFirstChild().getNodeValue()).append("</th></tr></table><br><br>\n");
                }
                html.append("<font face=\"黑体\"><b> Go: </b></font><br>\n");
                html.append("<table width=\"80%\" border=\"1\">\n");
                html.append("<tr><th><font face=\"黑体\"> GO ID </font></th>\n");
                html.append("<th><font face=\"黑体\"> Definition </font></th>\n");
                html.append("<th><font face=\"黑体\"> Branch </font></th></tr>\n");
                for (int j = 0; j < nodeList1.getLength(); j++) { // find go
                    Node nodeChild = nodeList1.item(j);
                    NamedNodeMap attributes1 = nodeChild.getAttributes();
                    String id1 = attributes1.getNamedItem("id").getNodeValue();
                    String link1 = "http://amigo.geneontology.org/amigo/term/" + id1;
                    String branch = attributes1.getNamedItem("branch").getNodeValue();
                    String goname = nodeChild.getFirstChild().getNodeValue();
                    html.append("<tr><th><a href=\"").append(link1).append("\">").append(id1).append("</a></th>\n");
                    html.append("<th>").append(goname).append("</th>\n");
                    html.append("<th>").append(branch).append("</th></tr>\n");
                }
                html.append("</table></body></html>\n");
                break;
            }
        }
        if(!isGo){
             html.append("<html><body onLoad=\"scrollTo(0,0)\">"
                + "<p><center><font face=\"微软雅黑\" size=\"+1\" "
                + "color=\"#0000ff\"><strong>No Gene Ontology info.</strong>"
                + "</font></center></p></body></html>");
        }
        return html.toString();
    }

    private int row;

    public static void main(String args[]) {
        InitTargetJob ttt = new InitTargetJob();
        //String html = ttt.getHtml("D:\\MyOffice\\Github\\MyTask\\RESHAFTS\\workspace\\network\\TargetNavigator\\25769\\5F7CW6TLDQResult.xml", "Cyclin-Dependent Kinase 1 (CDK1)");
    //int a = ttt.gettargetlength("E:\\Master\\Test\\99ZXL0GSIDResult.xml");
        //  System.out.println(a);
        //String[][] target = new String[a][6];
        //String[][] target = ttt.getTarget("E:\\Master\\MyOffice\\NetBeansWorkspace\\RESHAFTS\\workspace\\network\\TargetNavigator\\23672\\ZR8KOY3YYAResult.xml");//.getTarget("E:\\Master\\MyOffice\\NetBeansWorkspace\\RESHAFTS\\workspace\\network\\TargetNavigator\\23672\\ZR8KOY3YYAResult.xml","Prostaglandin G/H synthase 2");
        //Vector v = new Vector();
        //v = ttt.getTargetdata("E:\\Master\\MyOffice\\NetBeansWorkspace\\RESHAFTS\\workspace\\network\\TargetNavigator\\23672\\ZR8KOY3YYAResult.xml", "Aromatic-amino-acid aminotransferase");//,"Aromatic-amino-acid aminotransferase");
// for(int i = 0; i < target.length; i++){
        // System.out.println(target[i][0] + "*****" + target[i][1] + "*****" + target[i][2] + "*****" + target[i][3] + "*****" + target[i][4] + "*****" + target[i][5]);
        //  }
        String html = ttt.geteffectinfor
        ("F:\\Master\\MyOffice\\NetBeansWorkspace\\lib-eshafts"
                + "\\workspace\\network\\TargetNavigator\\23672"
                + "\\ZR8KOY3YYAResult.xml", "Aralkylamine dehydrogenase light chain", "DB00788");
        System.out.println(html);
    }
    //private String effect;
}
