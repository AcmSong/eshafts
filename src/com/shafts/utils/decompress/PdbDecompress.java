package com.shafts.utils.decompress;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

public class PdbDecompress {

    public String decompress(String filePath) {
        String tarFilePath = System.getProperty("user.dir") + "/Temp";
        File file;
        if (filePath != null && (file = new File(filePath)).exists()) {
            File tarFile = new File(tarFilePath);
            if (!tarFile.exists()) {
                tarFile.mkdir();
            }
            GZIPInputStream zipInputStream;
            OutputStream os;
            try {
                String fileName = file.getName().substring(0, file.getName().lastIndexOf("."));
                zipInputStream = new GZIPInputStream(new FileInputStream(file));
                String path = tarFilePath + "\\" + fileName;
                File temp = new File(path);
                os = new FileOutputStream(temp);
                int len;
                while ((len = zipInputStream.read()) != -1) {
                    os.write(len);
                }
                os.close();
                zipInputStream.close();
                zipInputStream.close();
                return path;
            } catch (IOException ex) {
                Logger.getLogger(PdbDecompress.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public static void main(String args[]) {
        String filePath = "C:\\Users\\ling0\\Downloads\\1fvv.pdb.gz";
        //System.out.println(decompress(filePath));
    }

}
