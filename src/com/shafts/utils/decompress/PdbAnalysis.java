package com.shafts.utils.decompress;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Little-Kitty
 */
public class PdbAnalysis {

    public static ArrayList getInfor(String filePath, ArrayList inforList) {
        //HashMap infor = new HashMap();
        boolean stop = false;
        FileReader reader;
        BufferedReader br;

        try {
            reader = new FileReader(new File(filePath));
            br = new BufferedReader(reader);
            String line = br.readLine();
            while (line != null) {
                String attri = line.substring(0, 6).trim();
                switch (attri) {
                    case "HEADER":
                        inforList.add("Class:" + line.substring(10, 50).trim().toLowerCase());
                        char[] date = (line.substring(50, 59)
                                .trim()).replaceFirst("-", "@").
                                replace("-", "-20")
                                .replace("@", "-").toLowerCase().toCharArray();
                        date[3] -= 32;
                        inforList.add("Deposition Date:" + String.valueOf(date));
                        line = br.readLine();
                        break;
                    case "TITLE":
                        StringBuilder title = new StringBuilder();
                        title.append(line.substring(10).trim()).append(" ");
                        while ((line = br.readLine()).substring(0, 6).trim()
                                .equals("TITLE")) {
                            title.append(line.substring(10).trim());
                        }
                        inforList.add("Title:" + title.toString().toLowerCase());
                        break;
                    case "SOURCE":
                        StringBuilder tempBuilder = new StringBuilder();
                        boolean ok = true;
                        while ((line = br.readLine()).substring(0, 6).trim()
                                .equals("SOURCE")) {                        
                            String[] srcTemp = line.substring(10).trim().split(":");                            
                            if ("ORGANISM_SCIENTIFIC".equals(srcTemp[0].trim()) && ok) {
                                tempBuilder.append(srcTemp[1].trim().replace(";", "")).append(",");
                            }
                            if ("ORGANISM_COMMON".equals(srcTemp[0].trim()) && ok) {
                                tempBuilder.append(srcTemp[1].trim().replace(";", ""));
                                ok = false;
                            }
                        }
                        inforList.add("Source:" + tempBuilder.toString().toLowerCase());
                        break;
                    case "EXPDTA":
                        inforList.add("Experimental Type:" + line.substring(10).toLowerCase());
                        line = br.readLine();
                        break;
                    case "REMARK":
                        while ((line = br.readLine()).substring(0, 6).trim()
                                .equals("REMARK")) {
                            if ("RESOLUTION".equals(line.substring(11, 21))) {
                                inforList.add("Resolution:" + line.substring(22, 31).trim().toLowerCase());
                                stop = true;
                                break;
                            }
                        }
                        break;
                    default:
                        line = br.readLine();

                }
                if (stop) {
                    br.close();
                    reader.close();
                    new File(filePath).delete();
                    break;
                }

            }

        } catch (FileNotFoundException ex) {
            Logger.getLogger(PdbAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PdbAnalysis.class.getName()).log(Level.SEVERE, null, ex);
        }

        return inforList;
    }

    public static void main(String args[]) {
        String path = "C:\\Users\\ling0\\Desktop\\1fvv.pdb\\pdb1fvv.ent";
        ArrayList inforList = new ArrayList();
        inforList  = getInfor(path, inforList);
        for (int i = 0; i<inforList.size(); i++) {
            System.out.println(inforList.get(i));
        }
    }
}
