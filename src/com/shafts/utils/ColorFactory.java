package com.shafts.utils;

import java.util.Random;

/**
 * @author Little-Kitty produce random color for atom C
 */
public class ColorFactory {

    public static String color() {
        String atomColor;
        Random random = new Random();
        int r = random.nextInt(256);
        int g = random.nextInt(256);
        int b = random.nextInt(256);
        String special = r + "" + g + b;
        //remove blcak, white, red blue yellow
        if (special.equals("000") || special.equals("255255255")
                || special.equals("2552550") || special.equals("25500")
                || special.equals("00255")) {
            color();
        }
        atomColor = "color[" + r + "," + g + "," + b + "]";
        return atomColor;
    }

}
