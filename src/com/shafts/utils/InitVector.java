package com.shafts.utils;

import java.awt.HeadlessException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

public class InitVector {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public Vector data;
    public static Vector columnNames;
    private static Vector headerName;
    JScrollPane jscrollpane;

    /**
     * read the .list file and return the data
     *
     * @param path
     * @return
     */
    public Vector getdata(String path) {
        data = new Vector();
        int i = 1;
        int flag = 1;

        if (path == null) {// initial the table
            JOptionPane.showMessageDialog(null,
                    "File not found!");
        } else {
            File file = new File(path);// result.list
            FileReader fileReader;
            BufferedReader bufferedReader;
            String s;
            if (!file.exists()) {
                JOptionPane.showMessageDialog(null,
                        "File not found!");
            } else {
                try {
                    Vector rowVector1 = new Vector();
                    rowVector1.add("  ");
                    rowVector1.add("Query");
                    rowVector1.add("2");
                    rowVector1.add("1");
                    rowVector1.add("1");
                    //rowVector.add(s1[5]);
                    rowVector1.add(true);
                    data.add(rowVector1);
                    fileReader = new FileReader(file);
                    bufferedReader = new BufferedReader(fileReader);
                    while ((s = bufferedReader.readLine()) != null) {
                        // System.out.println(s);
                        if (i != 1) {
                            String[] s1 = s.split("\\s+");
                            Vector rowVector = new Vector();
                            rowVector.add(s1[0]);
                            rowVector.add(s1[1]);
                            rowVector.add(s1[2]);
                            rowVector.add(s1[3]);
                            rowVector.add(s1[4]);
                            //rowVector.add(s1[5]);
                            rowVector.add(false);
                            data.add(rowVector);
                        }
                        i++;
                        flag++;
                    }

                    if (flag == 1) {
                        JOptionPane.showMessageDialog(null,
                                "Sorry! No any result can match your input file. Your can try again.");
                    }
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(InitVector.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(InitVector.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        }
        return data;
    }

    /**
     * retrun the column name
     *
     * @return
     *
     */
    public static Vector getcolumn() {
        columnNames = new Vector();
        //columnNames.add("Query");  //****Query
        columnNames.add("Rank");
        columnNames.add("Name");
        columnNames.add("HybridScore");
        columnNames.add("ShapeScore");
        columnNames.add("FeatureScore");
        columnNames.add("Show");
        return columnNames;
    }

    public static Vector getTargetHeader() {
        headerName = new Vector();
        headerName.add("ID");
        headerName.add("Name");
        headerName.add("SourceDB");
        headerName.add("Activity (nM)");
        headerName.add("Similarity");
        headerName.add("SMILES");
        headerName.add("View3D");
        return headerName;
    }

    public static void main(String args[]) {
        Vector V = new InitVector()
                .getdata("E:\\MyOffice\\Eclipse\\workplace\\ChemMapper\\workhome\\localwork\\Job9\\Result.list");//,"E:\\MyOffice\\Eclipse\\workplace\\ChemMapper\\workhome\\localwork\\Job9\\Input.mol2");
        // String[] v = (String[]) V.toArray();
        for (int i = 0; i < V.size(); i++) {
            System.out.println(V.elementAt(i));
        }
    }
}
