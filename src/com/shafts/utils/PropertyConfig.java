/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.shafts.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Little-Kitty
 * @date 2015-7-8 10:27:23
 */
public class PropertyConfig {

    public PropertyConfig() {
        File file = new File("eShafts.config");
        if(!file.exists()){
            createConfig();
        }
    }
    
    
    
    private boolean createConfig(){
        HashMap<String,String> hashMap = new HashMap();
        hashMap.put("workPath", System.getProperty("user.dir")+"\\workspace");
        hashMap.put("showPathSelection", "YES");
        hashMap.put("formatConvWarn", "YES");
        hashMap.put("3DgenWarn", "YES");
        hashMap.put("formatUIWarn", "YES");
        Boolean isDone;
        isDone = updateConfig(hashMap);
        return isDone;
    }
    
    private boolean updateConfig(HashMap<String,String> hashMap){
        File configFile = new File("eShafts.config");
        if(configFile.exists()){
            configFile.delete();
        }
        try {                    
           configFile.createNewFile();
           FileWriter fw = new FileWriter(configFile.getAbsoluteFile());
           BufferedWriter bw = new BufferedWriter(fw);
           bw.write("Please don't change the configuration manually!");
           bw.newLine();
           for(Entry<String,String> entry: hashMap.entrySet()){
                bw.write(entry.getKey() + "=" + entry.getValue());
                bw.newLine();
           }
           bw.flush();
           bw.close();
           return true;
        } catch (IOException ex) {
             Logger.getLogger(PropertyConfig.class.getName()).log(Level.SEVERE, null, ex);
            }
        return false;
    }
    
    /*
    get config info.
    */
    public HashMap getConfigMap(){
        HashMap config = new HashMap();
        File file = new File("eShafts.config");
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String info;
            br.readLine();
            while((info = br.readLine()) != null){
                String[] temp = info.split("=");
                config.put(temp[0], temp[1]);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PropertyConfig.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PropertyConfig.class.getName()).log(Level.SEVERE, null, ex);
        }                
        return config;
    }
    
    public boolean setConfig(String key, String value){
        boolean isDone;
        HashMap map = getConfigMap();
        map.put(key, value);
        isDone = updateConfig(map);                       
        return isDone;
    }
    public boolean setConfig(ArrayList list){
        boolean isDone;
        HashMap map = getConfigMap();
        map.put("workPath", list.get(0));
        map.put("showPathSelection", list.get(1));
        isDone = updateConfig(map);
        return isDone;
    }
    
    public boolean setConfig(HashMap map){
        boolean isDone;
        isDone = updateConfig(map);
        return isDone;
    }
    
    public String getConfig(String key){
        String value;
        HashMap map = getConfigMap();
        value = (String) map.get(key);        
        return value;
    } 
    
    public static void main(String args[]){
        PropertyConfig pc = new PropertyConfig();
        //boolean ok = pc.createConfig();
        boolean ok = pc.setConfig("formatConvWarn","NO");
        System.out.println(ok);
    }
}
