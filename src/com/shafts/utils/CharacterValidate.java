
package com.shafts.utils;

/**
 *
 * @author Little-Kitty
 * @date 2015-7-30 12:56:33
 */
public class CharacterValidate {
    
    public static boolean isContainChinese(String in){
        boolean contain = false;
        char[] c = in.toCharArray();
        for (int i = 0; i < c.length; i++) {
            if ((c[i] >= 0x4e00) && (c[i] <= 0x9fbb)) {
                contain = true;
                break;
            }
        }
        return contain;
    }
}
