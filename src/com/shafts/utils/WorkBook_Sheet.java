/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.shafts.utils;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * @author Little-Kitty
 * @date 2015-7-15 15:46:13
 */
public class WorkBook_Sheet {

    public HSSFSheet getWorkBook(HSSFWorkbook workbook, String targetModel) {        
        //workbook.setSheetName(0, "COMPUTE RESULT");        
        HSSFSheet sheet = workbook.createSheet("COMPUTE RESULT");                
        HSSFRow row = sheet.createRow(0);
        int item;
        String[] header;
        if(targetModel.equals("other")){
            item = 5;
            header = new String[]{"Rank","Name","HybridScore","ShapeScore","FeatureScore"};
        }else{
            item = 4;
            header = new String[]{"Target Name","Simi. Score","Score","Rank"};
        }
        for(int i = 0; i<item; i++){
            HSSFCell cell = row.createCell(i);
            cell.setCellType(HSSFCell.CELL_TYPE_STRING);
            cell.setCellValue(header[i]);
        }
        return sheet;
    }
}
