package com.shafts.utils;

import com.shafts.ui.InstallOBUI;
import java.io.File;
import org.openbabel.OBBuilder;

import org.openbabel.OBConversion;
import org.openbabel.OBMol;

/**
 * Format conversion
 *
 * @author Wenhao Wei 2014-07-06
 */
public class FormatConv {

    /**
     *
     */
    private boolean convFlag = false;
    private File inMol;
    private String inMolName;
    private String inFormat;

    /**
     * convert
     *
     * @param inFilePath the file path of the input molecule
     * @param outFilePath the file path of the output molecule
     * @param outFormat the format of the output molecule
     * @return return the flag if the conversion has succeeded
     */
    public boolean formatConv(String inFilePath, String outFilePath, String outFormat) {
        convFlag = false;
        boolean installed = true;
        if (!OBChecked.hasInstalled()) {
            InstallOBUI itstallOb = new InstallOBUI();
            itstallOb.setVisible(true);
            installed = itstallOb.isOBInstalled();
            if (!installed) {
                return false;
            }
        }
        if (installed) {
            inMol = new File(inFilePath);
            inMolName = inMol.getName();
            inFormat = inMolName.substring(inMolName.lastIndexOf(".") + 1);
            // Read molecule from informat       
            System.loadLibrary("openbabel_java");
            OBConversion conv = new OBConversion();
            OBMol mol = new OBMol();
            //OBBuilder builder = new OBBuilder();
            conv.SetInFormat(inFormat);
            conv.ReadFile(mol, inFilePath);
            conv.SetOutFormat(outFormat);
            //ignore output residue information (only ligands)
            conv.SetOptions("-xl", OBConversion.Option_type.OUTOPTIONS);
            conv.WriteFile(mol, outFilePath);
            conv.CloseOutFile();
            convFlag = true;
            // conv.WriteString(mol);									   	       
        }
        return convFlag;
    }

    public boolean Gen3D(String inFilePath, String outFilePath) {
        boolean flag = false;
        boolean installed = true;
        if (!OBChecked.hasInstalled()) {
            InstallOBUI itstallOb = new InstallOBUI();
            itstallOb.setVisible(true);
            installed = itstallOb.isOBInstalled();
            if (!installed) {
                return false;
            }
        }
        if (installed) {
            inMol = new File(inFilePath);
            inMolName = inMol.getName();
            inFormat = inMolName.substring(inMolName.lastIndexOf(".") + 1);
            // Read molecule from informat       
            System.loadLibrary("openbabel_java");
            OBConversion conv = new OBConversion();
            OBMol mol = new OBMol();
            OBBuilder builder = new OBBuilder();
            conv.SetInFormat(inFormat);
            conv.ReadFile(mol, inFilePath);
            conv.SetOutFormat(inFormat);
            if (!mol.Has3D()) {
                builder.Build(mol);
            }
            conv.SetOptions("-xl", OBConversion.Option_type.OUTOPTIONS);
            conv.WriteFile(mol, outFilePath);
            flag = true;
        }
        return flag;
    }

    public static void main(String args[]) {
        FormatConv fc = new FormatConv();
        boolean isConv = fc.formatConv(
                "F:\\Master\\MyOffice\\NetBeansWorkspace"
                + "\\RESHAFTS\\files\\MOL2\\shafts-file-20150809130441408.mol2",
                "F:\\Master\\MyOffice\\NetBeansWorkspace\\RESHAFTS\\files\\"
                + "MOL\\shafts-file-20150809130441408.mol2", "mol2");
        System.out.println(isConv);
    }
}
