package com.shafts.utils;

import com.shafts.bridge.ServerGate;
import com.shafts.module.WaitingGif;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author Little-Kitty
 * @date 2015-4-9 11:45:20
 */
public class PatchDoor {

    public boolean install(String keyPath, JPanel jobPanel, JPanel netJobPanel) {
        boolean isOk = false;
        File keyFile = new File(keyPath);
        if (keyFile.exists()) {
            isOk = repalceFile(keyFile, jobPanel, netJobPanel);
        } else {
            JOptionPane.showMessageDialog(null, "File not exist!", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        return isOk;
    }

    private boolean repalceFile(File keyFile, JPanel jobPanel, JPanel netJobPanel) {
        WaitingGif wait = new WaitingGif("Verifing");
        wait._setVisible(true);
        boolean isOk = false;
        String oldPath = keyFile.getAbsolutePath();
        String keyPath = PATH + "\\configuration\\PKCS#12\\";
        File file = new File(keyPath);
        InputStream inStream;
        if (!file.exists()) {
            file.mkdir();
        }
        try {
            //int bytesum = 0;
            int byteread;
            inStream = new FileInputStream(oldPath); //读入原文件
            FileOutputStream fs = new FileOutputStream(keyPath + "digiVeriSign.pem");
            byte[] buffer = new byte[1024 * 5];
            while ((byteread = inStream.read(buffer)) != -1) {
                //bytesum += byteread;   //字节数 文件大小
                //System.out.println(bytesum);
                fs.write(buffer, 0, byteread);
            }
            fs.close();
            inStream.close();
            // reStartSys();
            isOk = new ServerGate().verify();
            if (!isOk) {
                wait.close();
                new File(keyPath + "digiVeriSign.pem").delete();
                JOptionPane.showMessageDialog(null,
                        "Invalid key file!",
                        "Error", JOptionPane.OK_OPTION);
            } else {
                wait.close();
                jobPanel.removeAll();
                jobPanel.add(netJobPanel);
                jobPanel.updateUI();
                JOptionPane.showMessageDialog(null,
                        "Installation has been finished, Enjoy it!",
                        "MESSAGE", JOptionPane.PLAIN_MESSAGE);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PatchDoor.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "File not found!", "ERROR", JOptionPane.ERROR_MESSAGE);
        } catch (IOException ex) {
            Logger.getLogger(PatchDoor.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Install exception!", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        wait.close();
        return isOk;
    }

    public static void main(String arg[]) {
        PatchDoor pd = new PatchDoor();
        //pd.insall("E:\\Master\\MyOffice\\UserCrack\\Certificate.pfx");
    }

    private static final String PATH = System.getProperty("user.dir");
}
