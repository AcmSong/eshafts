package com.shafts.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PropertyConfigOld extends Properties {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    //private Properties prop;
    public void createProperties(String value1, String value2) {
        //prop = new Properties();
        put("showagain", value1);
        put("workpath", value2);

        FileOutputStream out;
        try {
            out = new FileOutputStream("userinfo.properties");
            store(out, "This file saved the user config.");
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PropertyConfigOld.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PropertyConfigOld.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //private Properties prop;
    public void putProperties(String value, String key) {
        //prop = new Properties();
        put(key, value);
        FileOutputStream out;
        try {
            out = new FileOutputStream("userinfo.properties");
            //store(out, "This file saved the user config.");
            out.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PropertyConfigOld.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PropertyConfigOld.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<String> readProperties() {
        ArrayList<String> array = new ArrayList<>();
        String usedefault;
        String workpath;
        try {

            try (FileInputStream in = new FileInputStream("userinfo.properties")) {
                load(in);
                usedefault = getProperty("showagain");
                workpath = getProperty("workpath");
                if(usedefault != null && workpath != null){
                    array.add(usedefault);
                    array.add(workpath);
                }
                else return null;
            }
            return array;
        } catch (FileNotFoundException e) {
            //e.printStackTrace();
            return null;

        } catch (IOException e) {
            //e.printStackTrace();
            return null;
        }

    }

    public String readProperties(String key) {
        String value = null;
        FileInputStream in;
        try {
            in = new FileInputStream("userinfo.properties");
            load(in);
            value = getProperty(key);
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PropertyConfigOld.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PropertyConfigOld.class.getName()).log(Level.SEVERE, null, ex);
        }
        return value;
    }

    public void updateProperties(String showagain, String workpath) {
        this.createProperties(showagain, workpath);
    }

    public static void main(String args[]) {

        PropertyConfigOld properties = new PropertyConfigOld();
        //properties.createProperties("NO", "weiwenhao");
        //ArrayList<String> a = properties.readProperties();
        //if(a == null)
        //System.out.println(a.get(1));
        properties.putProperties("YES", "formatConvWarn");
        String value = properties.readProperties("formatConvWarn");
        System.out.println(value);

    }
}
