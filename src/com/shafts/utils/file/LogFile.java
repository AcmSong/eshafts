package com.shafts.utils.file;

import com.shafts.utils.InfoFactory;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTextArea;

/**
 * the file will record the local computing log
 *
 * @author Little-Kitty
 */
public class LogFile {

    private File log;
    FileOutputStream fos = null;
    OutputStreamWriter osw = null;
    BufferedWriter bw = null;

    public LogFile() {
    }

    public boolean createLog(String logPath) {
        log = new File(logPath + "\\log.out");
        if (!log.exists()) {
            try {
                log.createNewFile();
                fos = new FileOutputStream(log);
                osw = new OutputStreamWriter(fos, "UTF-8");
                bw = new BufferedWriter(osw);
                return true;
            } catch (IOException ex) {
                Logger.getLogger(LogFile.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }
        return false;
    }

    public void log(String line) {
        if(bw != null){
            try {
                bw.write(line + "\t\n");
            } catch (IOException ex) {
                Logger.getLogger(LogFile.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void close(){
            try {
                if(bw != null)
                    bw.close();
                if(osw != null)
                    osw.close();
                if(fos != null)
                    fos.close();
        } catch (IOException ex) {
            Logger.getLogger(LogFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public JTextArea getLog(String logPath){
        logInfo = new JTextArea();
        logInfo.setEditable(false);
        logInfo.setColumns(20);
        logInfo.setRows(5);
        File logFile = new File(logPath);
        if(!logFile.exists()){
            logInfo.append("Can't find the log file!");
            return logInfo;
        }
        try {
            FileInputStream fis = new FileInputStream(new File(logPath));
            InputStreamReader isr = new InputStreamReader(fis, "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            String line;
            while((line = br.readLine()) != null){
                logInfo.append(line + "\n");
            }
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            Logger.getLogger(LogFile.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(LogFile.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return logInfo;
    }
    
    private JTextArea logInfo;

}
