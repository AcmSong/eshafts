
package com.shafts.utils.file;

import java.io.File;

/**
 *
 * @author Little-Kitty
 * @date 2015-7-30 12:34:50
 */
public class TempFile {
    
    public static String moveFile(String filePath){
        File originFile = new File(filePath);
        String tempPath = System.getProperty("user.dir") + "\\Temp\\";
        File file = new File(tempPath);
        if(!file.exists()){
            file.mkdir();
        }
        originFile.renameTo(new File(tempPath + originFile.getName()));
        
        return (tempPath + originFile.getName());
    }
    
    public static void backFile(String oldPath, String newPath){
        File file = new File(oldPath);
        file.renameTo(new File(newPath));
    }
}
