
package com.shafts.utils.file;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Little-Kitty
 * @date 2015-7-14 14:12:18
 */
public class MyFileFilter extends FileFilter{

    private final String[] suffix;
    private final String discription;
    
    public MyFileFilter(String[] suffix, String discription){
      this.discription = discription;
      this.suffix = suffix;
    }
    
    @Override
    public boolean accept(File f) {
        boolean isAccept = false;
        for (String suffix1 : suffix) {
            isAccept = (isAccept || f.getName().endsWith(suffix1));
        }
        isAccept = isAccept || f.isDirectory();
        return isAccept;
    }

    @Override
    public String getDescription() {
        return discription;
    }

}
