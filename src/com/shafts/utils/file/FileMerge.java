package com.shafts.utils.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Little-Kitty
 * @date 2015-7-15 12:39:36
 */
public class FileMerge {

    @SuppressWarnings("null")
    public boolean saveFile(ArrayList<File> fileList, String filePath) {

        boolean merge = true;
        File file = new File(filePath);
        FileReader fr;        
        FileWriter fw;
        BufferedReader br;
        PrintWriter pw;
        String line;
        try {            
            
            fw = new FileWriter(file);
            pw = new PrintWriter(fw, true);
            for (File fileList1 : fileList) {
                if (fileList1.exists()) {              
                    fr = new FileReader(fileList1);
                    br = new BufferedReader(fr);
                    while ((line = br.readLine()) != null) {
                        pw.println(line);
                    }
                    br.close();
                    fr.close();
                }
            }        
            pw.close();                        
        } catch (IOException ex) {
            merge = false;
            Logger.getLogger(FileMerge.class.getName()).log(Level.SEVERE, null, ex);
        }        
        return merge;
    }
}
