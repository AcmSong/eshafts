package com.shafts.utils;

import com.shafts.action.MainAction;
import com.shafts.render.TipWindow;
import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Little-Kitty
 * @date 2015-7-8 16:01:42
 */
public class WindowClose extends WindowAdapter {

    public WindowClose(MainAction f) {
        WindowClose.f = f;
        setService();
    }
    
    private void setService(){
        Thread server;
        lServer = new localServer(f, localPort);
        (server = new Thread(lServer)).setDaemon(true);
        server.start();
    }

    @Override
    public void windowClosing(WindowEvent e) {
        close();
    }

    public void close() {
        String content = "Are you sure to close the application?";
        int status = 0; // no job running
        if (f.getRunningJob() > 0) {
            content = "There are " + f.getRunningJob() + " jobs are running! Do you want keep it running in the \n background even close the eSHAFTS?";
            status = 1;
        }
        int result = JOptionPane.showConfirmDialog(f, content, "Tips",
                JOptionPane.YES_NO_CANCEL_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            switch (status) {
                // can exit directly
                case 0:
                    System.exit(0);
                    break;
                //exit only when all jobs fun over
                // if the app reopened before the jobs
                //over then the set will be lost
                case 1:
                    //new Thread(new runBackground()).start();
                    f.setVisible(false);
                    miniTray();
                    int x = Toolkit.getDefaultToolkit().getScreenSize().width - 300;
                    int y = Toolkit.getDefaultToolkit().getScreenSize().height - 150;
                    String tip = "eShafts has been minimized to the tray!";
                    new TipWindow(x, y, tip)._setVisible();
                    windowStatus = "closed";
                    break;
            }
        } else if (result == JOptionPane.NO_OPTION) {
            if (status == 1) {
                if (f.removeAllRunNode()) {
                    System.exit(0);
                } else {
                    JOptionPane.showConfirmDialog(f, "Delete running job error!", "Tips",
                            JOptionPane.OK_OPTION);
                }

            }
        }
//        else if(result == JOptionPane.CANCEL_OPTION){
//            //do nothing
//        }
    }

    /*
     // exit when all jobs running over
     class runBackground implements Runnable {

     @Override
     public void run() {
     f.setFlag(true);
     while (true) {
     if (f.getRunningJob() == 0) {
     break;
     }
     }
     if (f.getFlag()) {
     System.exit(0);
     }
     }
     }
     */
    private PopupMenu getPop() {
        PopupMenu pop = new PopupMenu();  //增加托盘右击菜单
        MenuItem show = new MenuItem("restore...");
        MenuItem exit = new MenuItem("exit...");
        //MenuItem split = new MenuItem("-----------");
        MenuItem info = new MenuItem("Runs: " + f.getRunningJob());
        //restore the window
        show.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                tray.remove(trayIcon);
                windowStatus = "visible";
                lServer.stopServer();
                f.setAutoRequestFocus(true);
                f.setVisible(true);
                f.setExtendedState(JFrame.NORMAL);
                f.toFront();
            }
        });

        //exit eshafts
        exit.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (f.removeAllRunNode()) {
                    tray.remove(trayIcon);
                    System.exit(0);
                } else {
                    JOptionPane.showConfirmDialog(f, "Delete running job error!", "Tips",
                            JOptionPane.OK_OPTION);
                }
            }
        });

        //info
        info.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                //do nothing
            }
        });
        
        pop.add(show);
        pop.add(exit);
        //pop.add(split);
        pop.insertSeparator(2);
        pop.add(info);
        return pop;
    }

    //System tray
    private void miniTray() {
        ImageIcon trayImg = new ImageIcon(getClass().getResource("/pic/icon1.png"));//托盘图标        
        trayIcon = new TrayIcon(trayImg.getImage());
        trayIcon.setToolTip("eShafts");
        trayIcon.setPopupMenu(getPop());
        trayIcon.setImageAutoSize(true);
        trayIcon.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) { // 鼠标器双击事件

                if (e.getClickCount() == 2) {

                    tray.remove(trayIcon); // 移去托盘图标
                    lServer.stopServer();
                    windowStatus = "visible";
                    f.setAutoRequestFocus(true);
                    f.setVisible(true);
                    f.setExtendedState(JFrame.NORMAL); // 还原窗口
                    f.toFront();
                }

                if (e.isMetaDown()) {
                    PopupMenu pop = getPop();
                    trayIcon.setPopupMenu(pop);
                }

            }
        });

//        trayIcon.addMouseListener(new MouseAdapter() {
//            @Override
//            public void mouseEntered(MouseEvent e) {
//                trayIcon.setPopupMenu(getPop());
//                
//            }
//
//            @Override
//            public void mouseExited(MouseEvent e) {
//                //trayIcon.setToolTip(null);
//            }
//
//        });
        try {
            tray.add(trayIcon);
        } catch (AWTException ex) {
            Logger.getLogger(WindowClose.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private static MainAction f;
    //private static boolean stop;
    private static final int localPort = 55555;
    static SystemTray tray = SystemTray.getSystemTray();
    public static TrayIcon trayIcon = null;
    public static String windowStatus;
    private localServer lServer;
}

class localServer implements Runnable {

    private boolean stop;
    private final int localPort;
    private static boolean status = true;
    private final MainAction oldFrame;

    public localServer(MainAction oldFrame, int localPort) {
        this.oldFrame = oldFrame;
        this.localPort = localPort;
    }

    public void stopServer() {
        stop = true;
    }

    @Override
    public void run() {
        try {
            if (status) {
                status = false;
                stop = false;
                ServerSocket server = new ServerSocket(localPort);
                while (true) {
                    server.accept();
                    stop = true;
                    if (oldFrame != null) {
                        WindowClose.tray.remove(WindowClose.trayIcon);
                        oldFrame.setAutoRequestFocus(true);
                        oldFrame.setVisible(true);
                    }
//                    if ("closed".equals(WindowClose.windowStatus)) {
//                        JOptionPane.showMessageDialog(null, "eShafts has started! "
//                                + "Please restore the window from the system tray",
//                                "tip", JOptionPane.OK_OPTION);
//                    }
                    oldFrame.setFlag(false);
                }
            } else {
                status = true;
            }
        } catch (IOException ex) {
            status = true;
            System.out.println("local server stoped");
            Logger.getLogger(WindowClose.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
