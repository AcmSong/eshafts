package com.shafts.utils;

import com.ice.jni.registry.Registry;
import com.ice.jni.registry.RegistryKey;




/**
 * check if the OB has installed
 *
 * @author Little-Kitty
 * @date 2015-7-27 12:36:29
 */
public class OBChecked {

    private static RegistryKey registryKey = null;    

    public static boolean hasInstalled() {
        //Song 2018.3.27，org没有2.4.1版本
        String[] keyName = new String[]{"Software\\OpenBabelGUI",
                                                                                "SOFTWARE\\OpenBabelGUI",
                                                                                "Software\\OpenBabe 2.3.1",
                                                                                "SOFTWARE\\OpenBabe 2.3.1",
                                                                                "Software\\OpenBabe 2.3.2",
                                                                                "SOFTWARE\\OpenBabe 2.3.2",
//                                                                                "Software\\OpenBabe 2.4.1",
//                                                                                "SOFTWARE\\OpenBabe 2.4.1",         
                                                                                };
        for (String keyName1 : keyName) {
            registryKey = Registry.
                                        openSubkey(Registry.HKEY_CURRENT_USER, 
                                                        keyName1, RegistryKey.ACCESS_READ);
            if(registryKey != null)
                return true;
        }
        return false;
    }

    
    public static void main(String args[]) {
        boolean b = hasInstalled();
        System.out.println(b);
    }

}
