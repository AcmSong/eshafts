
package com.shafts.utils;

import com.shafts.ui.MainUI;
import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Little-Kitty
 * @date 2015-7-12 10:23:38
 */
public class InfoFactory {
 
    private static final String docPath = System.getProperty("user.dir") 
            + "\\doc\\eSHAFTS_manual_EN.html";
    
    public static String baseInfor(){
        return "<html><body onLoad=\"scrollTo(0,0)\">"
                + "<p><center><font face=\"微软雅黑\" size=\"+1\" "
                + "color=\"#0000ff\"><strong>No Information.</strong>"
                + "</font></center></p></body></html>";
    }
    
    public static String setInfor(ArrayList inforList){
        StringBuilder builder = new StringBuilder();
        int tableWidth = MainUI.HTML_WIDTH - 50;
        builder.append("<html><body onLoad=\"scrollTo(0,0)\"><table width=\"")
                .append(tableWidth).
                append("\"align=\"center\" border=\"1\" bgcolor=\"#9AFF9A\">");
        for (Iterator it = inforList.iterator(); it.hasNext();) {
            Object inforList1 = it.next();
            String[] infor = ((String) inforList1).split(":", 2);
            builder.append("<tr><td><font face=\"微软雅黑\" color=\"#000000\"><strong>")
                    .append(infor[0])
                    .append(": </strong><font></td><td>"
                            + "<font face=\"微软雅黑\" color=\"#0000ff\"><strong>")
                    .append(infor[1])
                    .append("</font></strong></td></tr>");            
        }
        builder.append("</table></body></html>");
        return builder.toString();
    }
    
    public static String localDes(){        
        String description = "<html><body bgcolor=\" #EED5B7 \" style=\"text-align:justify;\">"
                + "<font face=\"微软雅黑\" size=\"3\">"
                + "<strong>Local</strong> "
                + "provides virtual screening services"
                + " to perform similar chemical searching against "
                + "user-uploaded databases, which is based on the local "
                + "computing devices. To start with <strong>Local"
                + "</strong>, you must provide a "
                + "single query molecule structure with valid 3D conformation. Besides, "
                + "given the user-uploaded target compounds, multiple conformations "
                + "(at most 100) can be generated with in-house method <strong>"
                + "Cyndi</strong> prior to "
                + "3D similarity calculation if the <strong>Generate Conformers"
                + "</strong> is toggled on. "
                + "Eventually, just click on the <strong>Start</strong> button and "
                + "<strong>eSHAFTS</strong> will calculate "
                + "the 3D similarities between the query and each molecule in the target "
                + "database, and then output the top ranked hit molecules in the result panel. "
                + "The user can refer to the <a href=\"file:///"
                + docPath
                + "\">manual</a> for detailed instructions."
                + "</font>"
                + "</body></html>";
        return description;
    }
    
    public static String remoteDes(){
        String description = "<html><body bgcolor=\" #EED5B7 \" style=\"text-align:justify;\">"
                + "<font face=\"微软雅黑\" size=\"3\">"
                +"The major difference "
                + "from <strong>Local"
                + "</strong> with <strong>Remote"
                + "</strong> is that the "
                + "computing of Remote is based on "
                + "the server-side resources, while of "
                + "which <strong>Local</strong> "
                + "is based on the local devices. "
                + "<strong>Remote</strong> "
                + "technically provides two types of "
                + "chemical database searching services, namely "
                + "<strong>Target Navigator</strong> "
                + "and <strong>Hit Explorer</strong>. For the former, "
                + "<strong>eSHAFTS</strong> curates nearly 300,000 "
                + "drug-like molecules "
                + "from ChEMBL, DrugBank, BindingDB with appropriate "
                + "pharmacology annotations of the predicted targets, as "
                + "well as structural information from the PDB " // and KEGG "
                + "databases. While for the latter, eSHAFTS collects open "
                + "accessed databases, like ZINC_TCM and "  //, Specs, MayBridge
                + "NCI to perform 3D structural superposition. The user "
                + "can refer to the <a href=\"file:///"
                + docPath
                + "\">manual</a> "
                + "for detailed instructions."
                + "</font></body></html>";
        
        return description;
    }
    
    public static String setInfo(String info){
        return "<html><body bgcolor=\" #EED5B7 \" style=\"text-align:justify;\">"
                + "<font face=\"微软雅黑\" size=\"4\">"
                + info
                + "</font>"
                + "</body></html>";
    }
}
