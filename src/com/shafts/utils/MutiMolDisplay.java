package com.shafts.utils;

import com.shafts.targetshow.ChildRow;
import com.shafts.targetshow.TargetManager;
import com.shafts.module.JmolPanel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import javax.swing.JTable;

/**
 *
 * @author Little-Kitty
 * @date 2015-6-18 19:38:15
 */
public class MutiMolDisplay extends MouseAdapter {

    private final String showMolPath;
    private final JTable jTable;
    private final JmolPanel jmolPanel;
    private final Hashtable<String, Integer> hasShow;
    private final String tableType;
    private String filePath;
    private String keyId;     // if the table is a target type, the keyId will be used
    private List<String[]> nameList;

    private TargetManager manager = TargetManager.TARGET_MANAGER;

    public MutiMolDisplay(String showMolPath, JTable jTable, JmolPanel jmolPanel, Hashtable<String, Integer> hasShow, String tableType, String filePath) {
        this.hasShow = hasShow;
        this.jTable = jTable;
        this.jmolPanel = jmolPanel;
        this.showMolPath = showMolPath;
        this.tableType = tableType;
        this.filePath = filePath;
        initName("NOTARGET");
    }

    public MutiMolDisplay(String showMolPath, JTable jTable,
            JmolPanel jmolPanel, Hashtable<String, Integer> hasShow,
            String tableType, String filePath, String keyId) {
        this.hasShow = hasShow;
        this.jTable = jTable;
        this.jmolPanel = jmolPanel;
        this.showMolPath = showMolPath;
        this.tableType = tableType;
        this.filePath = filePath;
        this.keyId = keyId;
        initName("TARGET");
    }

    private void initName(String type) {
        nameList = new ArrayList();
        switch (type) {
            case "TARGET":
                for (int i = 0; i < jTable.getModel().getRowCount(); i++) {
                    //first pos is the rank, second is the mol name which can locate the real mol
                    String[] item = new String[2];
                    item[0] = (String) jTable.getModel().getValueAt(i, 0);
                    String name = (String) jTable.getModel().getValueAt(i, 1);
                    String path1 = showMolPath + name + ".mol2";
                    File mol2file = new File(path1);
                    if (!mol2file.exists()) {
                        item[1] = (String) jTable.getModel().getValueAt(i, 0);
                    } else {
                        item[1] = name;
                    }
                    nameList.add(item);
                }
                break;
            case "NOTARGET":
                int molNum = 0;
                nameList.add(new String[]{"query", "input"});
                for (int i = 1; i < jTable.getModel().getRowCount(); i++) {
                    //first pos is the rank, second is the mol name which can locate the real mol
                    String[] item = new String[2];
                    item[0] = (String) jTable.getModel().getValueAt(i, 0);
                    String name = (String) jTable.getModel().getValueAt(i, 1);
                    String path1 = showMolPath + name + ".mol2";
                    File mol2file = new File(path1);
                    if (!mol2file.exists()) {
                        item[1] = ++molNum + "";
                    } else {
                        item[1] = name;
                    }
                    nameList.add(item);
                }
                break;
        }

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        String molKey;
        int keyPos;//the selectbox postion
        if (tableType.equals("TargetInfor")) {
            keyPos = 6;
        } else {
            keyPos = 5;
        }
        int showNo = 0;
        if (e.getButton() == MouseEvent.BUTTON1) {// left click
            String[] stringName = nameList.get(jTable.getSelectedRow());
            String show3Dname = stringName[1];
            molKey = stringName[0];
            if (jTable.getSelectedColumn() == keyPos) {
                boolean bl = !(boolean) jTable.getModel().getValueAt(jTable.getSelectedRow(), keyPos);
                jTable.getModel().setValueAt(bl, jTable.getSelectedRow(), keyPos);
                // if the job model is target, the manager will record the select flag
                // so you can reexpand the father table and won't lost the status of 
                // the flag
                if (keyPos == 6) {
                    ((ChildRow) (((HashMap) manager.getTable(keyId)).get(molKey))).setSelect(bl);
                }
                if (bl) {
                    if (jmolPanel.viewer.getModelCount() == 1) {
                        jmolPanel.enableButton();
                    }
                    for (Map.Entry entry : hasShow.entrySet()) {
                        int value = (int) entry.getValue();
                        if (showNo < value) {
                            showNo = value;
                        }
                    }
                    if (keyPos == 5 && jTable.getSelectedRow() == 0) {
                        showNo++;
                        hasShow.put("Input", showNo);
                        filePath = showMolPath + "Input.mol2";
                        String controller1 = "load APPEND " + "\"" + filePath
                                + "\"" + " ;frame*" + " ;hide Hydrogens"
                                + ";select carbon/" + showNo
                                + ".1; color [0,255,255]; center "
                                + showNo + ".1"; // first row is the query mol                                   
                        jmolPanel.viewer.evalString(controller1);

                    } else {
                        showNo++;
                        filePath = showMolPath + show3Dname + ".mol2";
                        hasShow.put(molKey, showNo);
                        String controller1 = "load APPEND " + "\""
                                + filePath + "\"" + " ;frame*"
                                + " ;hide Hydrogens" + ";select carbon/"
                                + showNo + ".1; " + ColorFactory.color() + ";";
                        jmolPanel.viewer.evalString(controller1);
                    }
                } else {
                    if (keyPos == 5 && jTable.getSelectedRow() == 0) {
                        int a = (int) hasShow.get("Input");
                        String b = a + ".1";
                        String controller = "zap " + b + " ;hide Hydrogens";
                        jmolPanel.viewer.evalString(controller);
                        hasShow.remove("Input");

                    } else {
                        int a = (int) hasShow.get(molKey);
                        String b = a + ".1";
                        String controller = "zap " + b + " ;hide Hydrogens";
                        jmolPanel.viewer.evalString(controller);
                        hasShow.remove(molKey);
                    }
                    showNo--;
                    if (hasShow.isEmpty()) {
                        showNo = 0;
                        jmolPanel.viewer.evalString("zap all");
                        jmolPanel.disableButton();
                    }
                }
            }
        }
    }

}
