package com.shafts.utils;

import java.util.Vector;

/**
 *
 * @author Little-Kitty
 * @date 2015-4-17 11:03:22
 */
public class TableSorter {
    
    /**
     * from big to small
     * @param tableData
     * @param col
     *          the number pos which use to compare
     * @param initNum
     *          the initial number of the sort, if the vector type is target,
     *          the initNum should be 0, or will be 1.
     * @return 
     */
    public Vector sortTable(Vector tableData, int col, int initNum){
       // Vector data = new Vector();
        for(int i = initNum; i < tableData.size(); i++){                
            for(int j = i+1; j < tableData.size(); j++){
                Vector data1 = (Vector) tableData.get(i);
                Vector data2 = (Vector) tableData.get(j);
                if(Double.parseDouble((String) data1.get(col)) < Double.parseDouble((String) data2.get(col))){
                    //Vector v = new Vector();
                    //v = data1;
                    tableData.setElementAt(data2, i);
                    tableData.setElementAt(data1, j);
                }
            }
        }
        return tableData;
    }
    
    public Vector reverseVector(Vector tableData){
        Vector data = new Vector();
        data.add(tableData.get(0));
        for(int i = tableData.size()-1; i > 0; i--){
            data.add(tableData.get(i));
        }
        return data;
    }
    public static void main(String args[]){
        Vector v1 = new Vector();
        Vector v = new Vector();
        v1.add("0");
        v1.add("diyiwei");
        v1.add("1.5");
        v1.add("3.2");
        v1.add(false);
        v.add(v1);
        v1 = new Vector();
        v1.add("0");
        v1.add("dierwei");
        v1.add("1");
        v1.add("3");
        v1.add(false);
        v.add(v1);
        v1 = new Vector();
        v1.add("0");
        v1.add("disanwei");
        v1.add("1.734");
        v1.add("2.45");
        v1.add(false);
        v.add(v1);
        v1 = new Vector();
        v1.add("0");
        v1.add("disiwei");
        v1.add("1.256");
        v1.add("1.325");
        v1.add(false);
        v.add(v1);
        System.out.println("排序前：******************************");
        for(int i = 0; i < v.size(); i++)
            System.out.println(v.get(i));
        Vector vv = new TableSorter().sortTable(v, 3, 1);
        System.out.println("排序后：******************************");
        for(int i = 0; i < vv.size(); i++)
            System.out.println(vv.get(i));
    }

}
