package com.shafts.utils;

import com.shafts.ui.BabelTipUI;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

/**
 *
 * @author Little-Kitty
 */
public class HyperLink implements HyperlinkListener {

    @Override
    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() != HyperlinkEvent.EventType.ACTIVATED) {
            return;
        }
        URL linkUrl = e.getURL();
        if (linkUrl != null) {
            try {
                Desktop.getDesktop().browse(linkUrl.toURI());
            } catch (IOException | URISyntaxException ex) {
                Logger.getLogger(BabelTipUI.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
