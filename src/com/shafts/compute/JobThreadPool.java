package com.shafts.compute;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * @author Little-Kitty
 */
public class JobThreadPool{
    
    private static ThreadPoolExecutor jobBox = null;
    public JobThreadPool(){
        
    }
    
    public static ThreadPoolExecutor threadPool(){
        if(jobBox == null)
            jobBox = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
        return jobBox;
    }
}
