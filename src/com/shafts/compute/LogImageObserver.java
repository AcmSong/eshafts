package com.shafts.compute;

import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.ImageObserver;
import static java.awt.image.ImageObserver.ABORT;
import static java.awt.image.ImageObserver.ALLBITS;
import static java.awt.image.ImageObserver.FRAMEBITS;
import javax.swing.JTable;

/**
 *
 * @author Little-Kitty
 */
public class LogImageObserver implements ImageObserver {

    JTable table;
    int row;
    int col;

    public LogImageObserver(JTable table, int row, int col) {
        this.table = table;
        this.row = row;
        this.col = col;
    }

    @Override
    public boolean imageUpdate(Image img, int infoflags, int x, int y,
            int width, int height) {
        if ((infoflags & (FRAMEBITS | ALLBITS)) != 0) {
            Rectangle rect = table.getCellRect(row, col, false);
            table.repaint(rect);
        }
        return (infoflags & (ALLBITS | ABORT)) == 0;
    }

}
