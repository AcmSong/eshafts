package com.shafts.compute;

import com.shafts.module.LogTableModel;
import com.shafts.module.jobmanager.LocalManager;
import com.shafts.render.CheckTableModle;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Vector;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Little-Kitty
 */
public class JobStatusPool {

    public JobStatusPool() {
        column = new Vector();
        column.add("WorkID");
        column.add("Status");
        column.add("Opts");
    }

    public JTable runJobTable() {
        JTable jTable = new JTable();
        //init(jTable);
        Vector running = getData("running");
        if(!running.isEmpty()){
            jTable.setModel(new LogTableModel(running, column));
            setImageObserver(jTable);
        }else{
            jTable = null;
        }
        return jTable;
    }

    public JTable waitQueueTable() {
        JTable jTable = new JTable();
        //init(jTable);
        Vector waiting = getData("waiting");
        if (!waiting.isEmpty()) {
            jTable.setModel(new LogTableModel(waiting, column));
            setImageObserver(jTable);
        } else {
            jTable = null;
        }

        return jTable;
    }

    public JTable doneTable() {
        JTable jTable = new JTable();
        //init(jTable);
        Vector done = getData("done");
        if (!done.isEmpty()) {
            jTable.setModel(new CheckTableModle(done, column));
        } else {
            jTable = null;
        }

        return jTable;
    }

//    private void init(JTable jTable){
//        jTable = new JTable();
//        jTable.setDefaultRenderer(Object.class, new MyCellRenderer());
//        //jTable.setBorder(null);
//    }
    private Vector getData(String type) {
        Vector data = new Vector();
        switch (type) {
            case "running":
                ArrayList list = getStatus("run");
                for (Object workerId : list) {
                    Vector row = new Vector();
                    ImageIcon icon = new ImageIcon(path);
                    row.add(workerId);
                    row.add(icon);
                    row.add("<html><body><a href> detail </a></body></html>");
                    data.add(row);
                }
                break;
            case "waiting":
                ArrayList list1 = getWaiting();
                for (Object workerId : list1) {
                    Vector row = new Vector();
                    ImageIcon icon1 = new ImageIcon(path);
                    row.add(workerId);
                    row.add(icon1);
                    row.add("<html><body><a href> cancel </a></body></html>");
                    data.add(row);
                }
                break;
            case "done":
                ArrayList list2 = getStatus("done");
                for (Object workerId : list2) {
                    Vector row = new Vector();
                    row.add(workerId);
                    row.add("<html><body><a href> Computing complete! </a></body></html>"); //click to show detail
                    row.add("<html><body><a href> remove </a></body></html>"); //click to remove(jobs.remove(key))
                    data.add(row);
                }
                break;
        }

        return data;
    }

    private void setImageObserver(JTable table) {
        TableModel model = table.getModel();
        //int colCount = model.getColumnCount();
        int rowCount = model.getRowCount();
//    for (int col = 0; col < colCount; col++) {
//      if (ImageIcon.class == model.getColumnClass(col)) {
        for (int row = 0; row < rowCount; row++) {  //row = 1 without the header
            ImageIcon icon = (ImageIcon) model.getValueAt(row, 1);
            if (icon != null) {
                icon.setImageObserver(new LogImageObserver(table, row,
                        1));
            }
//        }
//      }
        }
    }

    class imageLabel extends JLabel {

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            ImageIcon image = new ImageIcon(path);
            g.drawImage(image.getImage(), 0, 0, 200, 16, this);
        }
    }

    private ArrayList getWaiting() {
        ArrayList data = new ArrayList();
        BlockingQueue<Runnable> queue = jobPool.getQueue();
        Iterator it = queue.iterator();
        while (it.hasNext()) {
            Worker worker = (Worker) it.next();
            data.add(worker.getJob().getJobId());
        }
        return data;
    }

    private ArrayList getStatus(String type) {
        ArrayList run = new ArrayList();
        ArrayList done = new ArrayList();
        Iterator it = workers.keySet().iterator();
        while (it.hasNext()) {
            Worker key = (Worker) it.next();
            String value = (String) workers.get(key);
            if (value.equals("NO")) {
                run.add(key.getJob().getJobId());
            } else {
                done.add(key.getJob().getJobId());
            }
        }
        if ("run".equals(type)) {
            return run;
        } else {
            return done;
        }
    }

//    private JTable jTable;
    private Vector column;

    private final LocalManager workers = LocalManager.localJob();
    private final ThreadPoolExecutor jobPool = JobThreadPool.threadPool();
    private final String path = System.getProperty("user.dir") + "\\Pictures\\init_waiting.gif";

}
