package com.shafts.compute;

import com.shafts.ui.LogWriter;
import java.io.Serializable;

/**
 *
 * @author Little-Kitty
 */
public class Job implements Serializable{
    
    private String storePath;
    private String inMolPath;
    private String dbPath;
    private String maxOutNum;
    private String threshold;
    private String jobId;
    private boolean genConformer;
    
    private LogWriter log;

    public Job(){
        
    }
    
    public Job(String storePath, String inMolPath, String dbPath, String maxOutNum, String threshold, boolean genConformer) {
        this.storePath = storePath;
        this.inMolPath = inMolPath;
        this.dbPath = dbPath;
        this.maxOutNum = maxOutNum;
        this.threshold = threshold;
        this.genConformer = genConformer;
        //log = new LogWriter();
    }

    public String getStorePath() {
        return storePath;
    }

    public void setStorePath(String storePath) {
        this.storePath = storePath;
    }

    public String getInMolPath() {
        return inMolPath;
    }

    public void setInMolPath(String inMolPath) {
        this.inMolPath = inMolPath;
    }

    public String getDbPath() {
        return dbPath;
    }

    public void setDbPath(String dbPath) {
        this.dbPath = dbPath;
    }

    public String getMaxOutNum() {
        return maxOutNum;
    }

    public void setMaxOutNum(String maxOutNum) {
        this.maxOutNum = maxOutNum;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    public boolean isGenConformer() {
        return genConformer;
    }

    public void setGenConformer(boolean genConformer) {
        this.genConformer = genConformer;
    }

    public LogWriter getLog() {
        return log;
    }

    public void setLog(LogWriter log) {
        this.log = log;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }
    
}
