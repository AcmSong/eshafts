package com.shafts.compute;

import com.shafts.module.GoShafts;
import com.shafts.module.jobmanager.JobManager;
import com.shafts.module.jobmanager.LocalManager;
import com.shafts.render.TipPanel;
import com.shafts.render.TipWindow;
import com.shafts.render.treerender.IconNode;
import com.shafts.ui.JobStatusUI;
import com.shafts.utils.JobInfo_XML;
import com.shafts.utils.WindowClose;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;

/**
 *
 * @author Little-Kitty
 */
public class Worker extends Thread {

    private String jobConfigPath;
    private IconNode iconNode;
    private JobInfo_XML jobInfor;
    private JLabel jobTip;
    private JobManager manager = JobManager.manager();
    private Job job;
    private JTree jTree;
    private JLabel jobNum;
    
    public static int jobCount;

    public Worker() {

    }

    public Worker(String jobConfigPath, Job job,
            JTree jTree, IconNode iconNode, JobInfo_XML jobInfor,
            JLabel jobTip, JLabel jobNum) {
        this.jobConfigPath = jobConfigPath;
        this.iconNode = iconNode;
        this.jobInfor = jobInfor;
        this.jobTip = jobTip;
        this.job = job;
        this.jTree = jTree;
        this.jobNum = jobNum;
    }

    public String getJobConfigPath() {
        return jobConfigPath;
    }

    public void setJobConfigPath(String jobConfigPath) {
        this.jobConfigPath = jobConfigPath;
    }

    public IconNode getIconNode() {
        return iconNode;
    }

    public void setIconNode(IconNode iconNode) {
        this.iconNode = iconNode;
    }

    public JobInfo_XML getJobInfor() {
        return jobInfor;
    }

    public void setJobInfor(JobInfo_XML jobInfor) {
        this.jobInfor = jobInfor;
    }

    public JLabel getJobTip() {
        return jobTip;
    }

    public void setJobTip(JLabel jobTip) {
        this.jobTip = jobTip;
    }

    public JobManager getManager() {
        return manager;
    }

    public void setManager(JobManager manager) {
        this.manager = manager;
    }

    public Job getJob() {
        return job;
    }

    public void setJob(Job job) {
        this.job = job;
    }

    public JTree getjTree() {
        return jTree;
    }

    public void setjTree(JTree jTree) {
        this.jTree = jTree;
    }

    public JLabel getJobNum() {
        return jobNum;
    }

    public void setJobNum(JLabel jobNum) {
        this.jobNum = jobNum;
    }
    

    private static final GoShafts goShafts = new GoShafts();
    private final LocalManager localJob = LocalManager.localJob();

    @Override
    public void run() {
        jobCount++;
        jobNum.setText(jobCount + " / " + 5);
        localJob.put(this, "NO");//add new job "NO" presents running
        goShafts.shaftinit(job);
        jobInfor.setJobStatus(job.getJobId(),
                "Local", jobConfigPath);// set complete falg
        iconNode.setIcon(new ImageIcon(Worker.class
                .getResource("/pic/done.png")));
        String tip = job.getJobId() + " has finished!";
        if("visible".equals(WindowClose.windowStatus))
            new TipWindow(jobTip.getLocationOnScreen().x - 200, jobTip.getLocationOnScreen().y - 110, tip)._setVisible();
        manager.setImageObserver(jTree, iconNode);
        localJob.put(this, "YES"); // set "YES" when conputing complete
        jobCount--;
        jobNum.setText(jobCount + " / " + 5);
        frame.initAll();
    }
    
    private final JobStatusUI frame = JobStatusUI.getFrame();

}
