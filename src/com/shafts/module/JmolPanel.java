package com.shafts.module;

import com.shafts.ui.JcpUI;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JPanel;
import javax.swing.JTextField;

import org.jmol.adapter.smarter.SmarterJmolAdapter;
import org.jmol.api.JmolAdapter;
import org.jmol.api.JmolViewer;

public class JmolPanel extends JPanel {

    private static final long serialVersionUID = -4747481289286342678L;

    public static void main(String[] args) {
//		(new JmolTest()).viewer.loadInline(strXyzHOH);
        JmolPanel jmolPanel = new JmolPanel();

        JFrame newFrame = new JFrame();
        newFrame.add(jmolPanel);
        newFrame.setSize(500, 500);
        newFrame.setVisible(true);
    }

    public JmolPanel() {
        initialize();
    }

    private void initialize() {
        adapter = new SmarterJmolAdapter();
        viewer = JmolViewer.allocateViewer(this, adapter);
        viewer.setColorBackground("black");
        viewer.evalString("set language en");
    }

    @Override
    public void paint(Graphics g) {
        getSize(currentSize);
        viewer.renderScreenImage(g, currentSize.width, currentSize.height);
    }

    public void writeimage() {

    }

    public void setpath(String path) {
        this.filePath = path;
    }

    public void createFileModifyListener() {
        //设置为true可监听编辑器
        setIfEditMol(true);
        jcpUI.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                setIfEditMol(false);
            }
        });
        TwoDThread td = new TwoDThread();
        td.start();
    }

    /**
     *
     */
    class TwoDThread extends Thread {

        private long lastmodified1 = 0;
        private final int SLEEP_TIME = 500;
        File tempfile;

        @Override
        public void run() {
            //String tempfilePath = System.getProperty("user.dir") + "\\" + CreateMolecule.RENDER_FILE_NAME;
            tempfile = new File(filePath);
            while (ifEditMol) {
                if (tempfile.exists() && tempfile.lastModified() > lastmodified1) {
                    viewer.openFile(filePath);// Open                   
//                    File file = new File(filePath);
//                    if (file.exists()) {
//                        file.delete();
//                    }
//                    tempfile.renameTo(file);
//                    viewer.openFile(filePath);
                    lastmodified1 = tempfile.lastModified();
                }

                sleep(SLEEP_TIME);
                if (viewer.getAtomCount() > 0) {
                    ableComp();
                } else {
                    disableComp();
                }
            }

        }

        @Override
        public void interrupt() {
            super.interrupt();
            if (tempfile.exists()) {
                tempfile.delete();
            }
        }

        private void sleep(int milliSec) {
            try {
                Thread.sleep(milliSec);
            } catch (InterruptedException ex) {
                Logger.getLogger(JmolPanel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public String[] getchains() {
        String chainID;
        //String chain[] = null;
        String chaincommand = "show chain";
        chainID = viewer.scriptWaitStatus(chaincommand, jmolSuffix).toString().replace("\n", "").trim();//scriptWait(chaincommand);
        String chain[] = new String[chainID.length() + 1];
        chain[0] = "Select chain...";
        for (int i = 0; i < chainID.length(); i++) {
            chain[i + 1] = String.valueOf(("chain: " + chainID.charAt(i)));
        }
        return chain;
    }

    private String filePath;
    private boolean ifEditMol = true;

    public void setIfEditMol(boolean ifEditMol) {
        this.ifEditMol = ifEditMol;
    }

    private JcpUI jcpUI;

    public void setJcpUI(JcpUI jcpUI) {
        this.jcpUI = jcpUI;
    }

    public void initComp(JButton jButton2, JButton jButton3, JButton jButton4, JButton jButton5, JButton jButton6, JButton jButton7, JButton jButton8, JButton jButton13, JButton jButton18, JMenu jMenu3, JTextField jTextField1) {
        this.jButton2 = jButton2;
        this.jButton3 = jButton3;
        this.jButton4 = jButton4;
        this.jButton5 = jButton5;
        this.jButton6 = jButton6;
        this.jButton7 = jButton7;
        this.jButton8 = jButton8;
        this.jButton13 = jButton13;
        this.jButton18 = jButton18;
        this.jMenu3 = jMenu3;
        this.jTextField1 = jTextField1;
    }

    private void ableComp() {
        //String filename = (new File(filePath).getName());
        enableButton();
        jTextField1.setText(JcpUI.DD_FILE_NAME);
        jTextField1.setToolTipText(filePath);
    }

    private void disableComp() {
        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton4.setEnabled(false);
        jButton5.setEnabled(false);
        jButton6.setEnabled(false);
        jButton7.setEnabled(false);
        jButton8.setEnabled(false);
        jButton18.setEnabled(false);
        jMenu3.setEnabled(false);
        jButton13.setEnabled(false);
        jTextField1.setText("");
        jTextField1.setToolTipText(null);
    }

    public void enableButton() {
        jButton2.setEnabled(true);
        jButton3.setEnabled(true);
        jButton4.setEnabled(true);
        jButton5.setEnabled(true);
        jButton6.setEnabled(true);
        jButton7.setEnabled(true);
        jButton8.setEnabled(true);
        jButton18.setEnabled(true);
        jMenu3.setEnabled(true);
        jButton13.setEnabled(true);
    }

    public void disableButton() {
        jButton2.setEnabled(false);
        jButton3.setEnabled(false);
        jButton4.setEnabled(false);
        jButton5.setEnabled(false);
        jButton6.setEnabled(false);
        jButton7.setEnabled(false);
        jButton8.setEnabled(false);
        jButton18.setEnabled(false);
        jMenu3.setEnabled(false);
        jButton13.setEnabled(false);
    }

    public javax.swing.JButton jButton2;
    public javax.swing.JButton jButton3;
    public javax.swing.JButton jButton4;
    public javax.swing.JButton jButton5;
    public javax.swing.JButton jButton6;
    public javax.swing.JButton jButton7;
    public javax.swing.JButton jButton8;
    public javax.swing.JButton jButton13;
    public javax.swing.JButton jButton18;
    public javax.swing.JMenu jMenu3;
    public javax.swing.JTextField jTextField1;
    private String jmolSuffix;
    public JmolViewer viewer;
    private JmolAdapter adapter;
    private final Dimension currentSize = new Dimension();
}
