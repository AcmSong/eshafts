package com.shafts.module;

import com.shafts.utils.ProgressPanel;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author Little-Kitty
 */
public class LaunchGif extends javax.swing.JDialog {

    /**
     * Creates new form LaunchGif
     */
    public LaunchGif() {
        initImagePath();
        initComponents();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setUndecorated(true);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(0, 0, 0));

        jPanel2.setBackground(new java.awt.Color(0, 0, 0));
        jPanel2.setLayout(new java.awt.BorderLayout());
        jPanel2.add(new ProgressPanel());

        jLabel2.setBackground(new java.awt.Color(0, 0, 0));
        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/LaunchBottom.png"))); // NOI18N

        jLabel3.setFont(new java.awt.Font("微软雅黑", 0, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 255, 255));

        jPanel3.setLayout(new java.awt.GridLayout(1, 0));
        jPanel3.add(new LaunchPanel());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 450, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 350, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 18, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel2))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void _setVisible(){
        new Thread() {
            @Override
            public void run() {
                setVisible(true);
                showProgress("Loading compoment...");
            }
        }.start();
    }
    
    public void close() {
        dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(LaunchGif.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                LaunchGif dialog = new LaunchGif();
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog._setVisible();
                dialog.showProgress("Time to show.");
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(LaunchGif.class.getName()).log(Level.SEVERE, null, ex);
                }
                System.out.println("Will close after 5s...");
                dialog.setStop(false);
            }
        });
    }

    /**
     * launch animation panel
     */
    class LaunchPanel extends JPanel {

        String imagePath;

        public LaunchPanel() {
            runAnimation();
        }
        private void runAnimation() {
           
            new Thread(new Runnable() {

                @Override
                public void run() {
                    for (int i = 0; i < imageList.size(); i++) {
                        imagePath = (String) imageList.get(i);
                        launchImage = new ImageIcon(LaunchGif.class.getResource(imagePath));
                        repaint();
                        if (i == (imageList.size()-1) && stop) {
                            i = 102;
                        }
                        sleep(30);
                    }
                    dispose();
                }

            }).start();
        }

        @Override
        public void paintComponent(Graphics g) {
            if (launchImage != null) {
                g.drawImage(launchImage.getImage(), 0, 0, 450, 350, this);
                launchImage = null;
            }
        }

        private void sleep(int milliSec) {
            try {
                Thread.sleep(milliSec);
            } catch (InterruptedException ex) {
                Logger.getLogger(LaunchGif.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * 0~101: animation_a; 102~202: animation_b, should loop
     */
    private void initImagePath() {
        imageList = new ArrayList();
        String a_Path = "/pic/Animation_A/";
        String b_Path = "/pic/Animation_B/";
        for (int i = 0; i < 101; i++) {
            if (i < 10) {
                imageList.add(a_Path + "A_00" + i + ".jpg");
            } else if (i < 100) {
                imageList.add(a_Path + "A_0" + i + ".jpg");
            } else {
                imageList.add(a_Path + "A_" + i + ".jpg");
            }
        }
        for (int i = 100; i < 201; i++) {
            imageList.add(b_Path + "B_" + i + ".jpg");
        }
    }

    public void showProgress(String str) {
        jLabel3.setText(str);
    }
    
    public void setStop(boolean stop){
        this.stop = stop;
    }
    
    //private final String path = System.getProperty("user.dir") + "\\Pictures\\";
    private List imageList;
    private ImageIcon launchImage;
    private boolean stop;
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    // End of variables declaration//GEN-END:variables
}
