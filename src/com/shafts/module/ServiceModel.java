
package com.shafts.module;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ecust.remote.chemmapper.model.ChemmapperServiceModel;

/**
 *
 * @author Little-Kitty
 * @date 2015-7-12 8:03:42
 */
public class ServiceModel {

    /**
     *  boundle the model
     * @param path
     * @param Model
     * @param outputnum
     * @param program
     * @param screendb
     * @param threshold
     * @return 
     */
    public ChemmapperServiceModel getModel(String path, int Model, String outputnum, String program, String screendb, String threshold){
        
        String type = path.substring(path.lastIndexOf(".") + 1, path.length());
        ChemmapperServiceModel csm = new ChemmapperServiceModel();
        File file = new File(path); 
       // File file = new File();

        byte[] input = null;
        if (file.exists()) {           
            try {
                InputStream ins;
                ins = new FileInputStream(file);
                BufferedInputStream bufin = new BufferedInputStream(ins);
                ByteArrayOutputStream out = new ByteArrayOutputStream(1024);
                byte[] temp = new byte[1024];
                int size;
                while ((size = bufin.read(temp)) != -1) {
                    out.write(temp, 0, size);
                }
                bufin.close();
                input = out.toByteArray();
                out.close();
                ins.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(ServiceModel.class.getName()).log(Level.SEVERE, null, ex);
            }  catch (IOException ex) {
                    Logger.getLogger(ServiceModel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        
        csm.setMode(Model);
        csm.setFileType(type);
        csm.setInput(input);
        csm.setOutputNum(outputnum);
        csm.setProgram(program);
        if(Model == 1)
        csm.setScreenDb(screendb);
        else{
            String[] array = {screendb};            
          csm.setTargetDb(array);   
        }      
        csm.setThreshold(threshold);
        
        return csm;
    }
}
