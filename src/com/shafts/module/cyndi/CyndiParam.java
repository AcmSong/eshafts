package com.shafts.module.cyndi;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CyndiParam {

    private final String path;

    public CyndiParam(String path) {
        this.path = path;
    }

    public CyndiParam() {
        this.path = System.getProperty("user.dir") + "\\CyndiParam.in";
    }

    public ArrayList<String[]> read() {
        FileReader fr;
        BufferedReader reader;
        ArrayList context = new ArrayList();
        try {
            fr = new FileReader(path);
            reader = new BufferedReader(fr);
            String line;
            String[] array;
            while ((line = reader.readLine()) != null) {
                array = line.split("\t\t");
                context.add(array);  //flag key:MOGA_Max_Conformers
            }
            reader.close();
            fr.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CyndiParam.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CyndiParam.class.getName()).log(Level.SEVERE, null, ex);
        }
        return context;
    } 
}
