package com.shafts.module.cyndi;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Cyndi {

    private final String path;
    private final CyndiParam cyndiParam;

    public Cyndi() {
        this.path = System.getProperty("user.dir") + "\\CyndiParam.in";
        cyndiParam = new CyndiParam();
    }

    private ArrayList<String[]> getContext() {
        return cyndiParam.read();

    }

    public int updateParam(String newParam) {
        ArrayList<String[]> context = getContext();
        boolean shouldUpdate = false;
        FileWriter fw;
        File file = new File(path);
        for (String[] array : context) {
            if ("MOGA_Max_Conformers".equals(array[0]) && (!newParam.equals(array[1]))) {
                array[1] = newParam;
                shouldUpdate = true;
            }
        }
        if (shouldUpdate) {
            if (file.exists()) {
                file.delete();
            }
            try {
                fw = new FileWriter(path);
                StringBuilder out = new StringBuilder();
                for (String[] array : context) {
                    out.append(array[0])
                            .append("\t\t")
                            .append(array[1])
                            .append("\r\n");
                }
                fw.write(out.toString());
                fw.close();
                return 1;
            } catch (IOException ex) {
                Logger.getLogger(Cyndi.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        return 0;
    }

    public String getParam() {
        String param = null;
        ArrayList<String[]> context = getContext();
        for (String[] array : context) {
            if ("MOGA_Max_Conformers".equals(array[0])) {
                param = array[1];
                break;
            }
        }
        return param;
    }

}
