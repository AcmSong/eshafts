package com.shafts.module;

import java.util.Vector;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Little-Kitty
 */
public class LogTableModel extends DefaultTableModel{
    private static final long serialVersionUID = 1L;

    public LogTableModel(Vector data, Vector columnNames) {
        super(data, columnNames);
    }
    
    @Override
    public Class<?> getColumnClass(int c){
        if(c == 1){
            return ImageIcon.class;
        }else
            return Object.class;
    }
    
    @Override
    public boolean isCellEditable(int row, int column) {
        //if (column == 5) {
           // return true;
        //} else {
            return false;
        //}
    }
    
//    public Object getValueAt(int row, int col){
//        
//    }
    
}
