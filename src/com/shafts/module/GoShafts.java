/*
 * the shafts
 */
package com.shafts.module;

import com.shafts.compute.Job;
import com.shafts.ui.LogWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Vector;

import com.shafts.utils.file.FileSplit;
import com.shafts.utils.FormatConv;
import com.shafts.utils.InitVector;
import com.shafts.utils.file.LogFile;
import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baoabo
 *
 */
public class GoShafts {

    private LogWriter writer;
    private LogFile logger;
    
    /**
     * @param job
     */
    public void shaftinit(Job job) {
        String filePath = job.getInMolPath();
        String inMolPath = job.getInMolPath();
        String storePath = job.getStorePath();
        String dbPath = job.getDbPath();
        workPath = job.getStorePath();
        writer = new LogWriter();
        job.setLog(writer);
        logger = new LogFile();
        logger.createLog(workPath);
        logger.log("========================= Start computing! =========================");
        //boolean isOk = false;
        if (job.isGenConformer()) // decide the run model 
        {
            dbPath = generateConformer(dbPath, "Database");
        }
        if (new File(dbPath).exists()) {
            boolean movefile = false;
            if (!((inMolPath.substring(inMolPath.lastIndexOf(".") + 1)).equals("mol2"))) { // convert the input format to mol2
                movefile = true;
                filePath = format_To_Mol2(inMolPath);
            }
            if (!movefile) {                // 
                String newpath = storePath + "\\Input.mol2";
                copyMol2(inMolPath, newpath);
            }
            compute(filePath, dbPath, job.getMaxOutNum(), job.getThreshold());
        }
        logger.close();
    }

//    public String getworkid(String newPath) {
//        
//    }

    /**
     * generate conformers
     * @param inFile
     * @param model
     * @return
     */
    public String generateConformer(String inFile, String model) {
        File file1 = new File(inFile);
        String name = file1.getName();
        String outpath = System.getProperty("user.dir") + "\\conformers";
        File file2 = new File(outpath);
        InputStream ins;
        if (!file2.exists()) {
            file2.mkdir();
        }
        if (model.equals("Database")) {
            outpath = outpath + "\\Databases";
            outPath = outpath + "\\" + name;
            cmd = "Cyndi.exe -i " + inFile + " -o " + outPath + " -p" + " CyndiParam.in ";
        } else {
            outpath = outpath + "\\files";
            outPath = outpath + "\\" + name;
            cmd = "Cyndi.exe -i " + inFile + " -o " + outPath + " -p" + " CyndiParamPerMol2.in ";
        }
        if (!(new File(outpath).exists())) {
            new File(outpath).mkdir();
        }
        try {
            //Runtime.getRuntime().exec(cmd);
            Process process = Runtime.getRuntime().exec(cmd);
            ins = process.getInputStream(); // cmd start
            BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
            String line;
            writer.write("********** Generate Conformer! **********");
            logger.log("********** Generate Conformer! **********");
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
                writer.write(line);
                logger.log(line);
                //writer.updateUI();
            }
            int exitValue = process.waitFor();
            System.out.println("return value:" + exitValue);
            writer.write("********** OK! **********");
            logger.log("********** OK! **********");
            process.getOutputStream().close(); // close the stream
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(GoShafts.class.getName()).log(Level.SEVERE, null, ex);
        }
        return outPath;
    }

    /**
     * begin the process
     *
     * @param FilePath
     * @param DataBase
     * @param outputNum
     * @param threshold
     */
    private void compute(String inMolFile, String database, String outPutNum, String threshold) {
        if (!inMolFile.equals("stop")) {
            writer.write("********** Run SHAFTS! **********");
            logger.log("********** Run SHAFTS! **********");
            String shafts = "Cynthia.exe -q " + inMolFile + " -t " + database + " -n " + outPutNum + " -sCutoff " + threshold;
            InputStream ins;
            try {
                Process process = Runtime.getRuntime().exec(shafts);
                ins = process.getInputStream(); // cmd start
                BufferedReader reader = new BufferedReader(new InputStreamReader(ins));
                String line;
                while ((line = reader.readLine()) != null) {
                    System.out.println(line);
                    writer.write(line);
                    logger.log(line);
                    //writer.updateUI();
                }
                int exitValue = process.waitFor();
                System.out.println("return value:" + exitValue);
                writer.write("********** finished! **********");
                logger.log("********** finished! **********");
                //writer.updateUI();
                process.getOutputStream().close(); // close the stream

            } catch (IOException | InterruptedException e) {
                Logger.getLogger(GoShafts.class.getName()).log(Level.SEVERE, null, e);
            }
            movefiles();
        }
    }

    /**
     * copy the query file to the result dir
     * @param oldPath
     * @param newPath
     */
    private void copyMol2(String oldPath, String newPath) {
        int bytesum = 0;
        int byteread;
        File oldfile = new File(oldPath);
        if (oldfile.exists()) {
            InputStream inStream;
            try {
                inStream = new FileInputStream(oldPath); 
                FileOutputStream fs = new FileOutputStream(newPath);
                byte[] buffer = new byte[1024 * 5];
                while ((byteread = inStream.read(buffer)) != -1) {
                    bytesum += byteread;  
                    System.out.println(bytesum);
                    fs.write(buffer, 0, byteread);
                }
                inStream.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GoShafts.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(GoShafts.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * move result and hits
     */
    public void movefiles() {
        byte[] b = new byte[1024 * 5];
        try {
            File F2 = new File("Result.list");
            if (F2.exists()) {
                File F3 = new File("Hits.mol2");
                FileInputStream input1;
                FileOutputStream output1;
                int len1;
                try (FileInputStream input = new FileInputStream(F2)) {
                    input1 = new FileInputStream(F3);
                    try (FileOutputStream output = new FileOutputStream(workPath + "/" + F2.getName())) {
                        output1 = new FileOutputStream(workPath + "/" + F3.getName());
                        // byte[] b = new byte[1024 * 5];
                        int len;
                        while ((len = input.read(b)) != -1) {
                            output.write(b, 0, len);
                        }
                        output.flush();
                    }
                }
                F2.delete();
                while ((len1 = input1.read(b)) != -1) {
                    output1.write(b, 0, len1);
                }
                output1.flush();
                output1.close();
                input1.close();
                F3.delete();
                new FileSplit().StartSplit(workPath);
            }
        } catch (IOException ioe) {
            Logger.getLogger(GoShafts.class.getName()).log(Level.SEVERE, null, ioe);
        }
    }

    private String format_To_Mol2(String infilepath) {
        FormatConv fc = new FormatConv();
        String outformat = "mol2";
        String path = workPath + "\\Input.mol2";
        boolean flag = fc.formatConv(infilepath, path, outformat);
        if (flag) {
            return path;
        } else {
            return "stop";
        }
    }

    /**
     * @return
     */
    public Vector getdata() {
        String path = workPath + "\\Result.list";
        IV = new InitVector();
        workdata = IV.getdata(path);
        return workdata;
    }

    public static void main(String args[]) {
        GoShafts sm = new GoShafts();
        //sm.shaftinit("D:\\TEST\\result\\job", "D:\\TEST\\DB00800.mol2", "D:\\TEST\\Hits.mol2", "100", "1.2", "conformer");
        //sm.generateConformer("D:\\TEST\\Hits.mol2", "Database");
        //sm.test();
    }

    //private String LocalworkID;
    private String workPath;
    private InitVector IV;
    private Vector workdata;
    private String cmd;
    private String outPath;
}
