package com.shafts.module.jobmanager;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Little-Kitty
 */
public class LocalWork {
    
    public static String genId(String path){
        String workId = null;
        String workPath;
        Date date = new Date();
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        String today = df.format(date);
        File workFile = new File(path);
        System.out.println(path + "===========================" + workFile.exists());
        File[] listFiles = workFile.listFiles();
        boolean isNewDate = true;
        for (File listFile : listFiles) {
            String name = listFile.getName();
            String[] s = name.split("-");
            String time = s[0];
            if (time.equals(today)) {
                isNewDate = false;
                int j = 1;
                String k = j + "";
                workPath = path + today + "-" + k;
                File newJob = new File(workPath);
                while (newJob.exists()) {
                    j++;
                    k = j + "";
                    workPath = path + today + "-" + k;
                    newJob = new File(workPath);
                }
                newJob.mkdir();
                workId = today + "-" + k;
                break;
            }
        }
        if (isNewDate) {
            File jobFile = new File(path + today + "-1");
            jobFile.mkdir();
            workId = today + "-1";

        }
        return workId;
    }
    
}
