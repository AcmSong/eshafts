
package com.shafts.module.jobmanager;

import com.shafts.bridge.SFTPConnection;
import com.shafts.render.treerender.IconNode;
import com.shafts.render.treerender.NodeImageObserver;
import com.shafts.utils.JobInfo_XML;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JTree;

/**
 * manage the remote job
 * @author Little-Kitty
 * @date 2015-7-24 13:38:23
 */
public class JobManager {

    private Map hashMap;
    private JTree jTree;
    private String xmlPath;
    private static boolean ISALLDONE;
    private static boolean ISSTARTED;
    private final String serverPath = "/home2/scbit/WebApps/FeatureAlign/jobs/";
    private final SFTPConnection server = new SFTPConnection();
    private final JobInfo_XML jobConfig = new JobInfo_XML();

    private JobManager() {
    }

    public void setHashMap(Map hashMap) {
        this.hashMap = hashMap;
        if (!hashMap.isEmpty()) {
            // start loop to check result
            new CheckResult().start();
            ISSTARTED = true;
        }
    }

    public void setXmlPath(String xmlPath) {
        this.xmlPath = xmlPath;
    }        

    public void setjTree(JTree jTree) {
        this.jTree = jTree;
    }    
    

    public void addJob(String model, IconNode node) {
        hashMap.put(node, model);
        if (!ISSTARTED) {
            new CheckResult().start();
            ISSTARTED = true;
        }
    }

    public void removeNode(IconNode node) {
        hashMap.remove(node);
        if (hashMap.isEmpty()) {
            ISALLDONE = true;
        }
    }

    /*
    check job if has finished
    */
    class CheckResult extends Thread {

        String nodeName = null;
        List<IconNode> hasDone = new ArrayList();

        @Override
        public void run() {
            while (!ISALLDONE) {
                Iterator it = hashMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    IconNode node = (IconNode) entry.getKey();
                    nodeName = node.toString();
                    if(isDone(nodeName)){
                        node.setIcon(new ImageIcon(JobManager.class.getResource("/pic/done.png")));
                        hasDone.add(node);
                        removeNode(node);
                        jobConfig.setJobStatus(nodeName, (String) entry.getValue(), xmlPath);
                    }
                }
                if(!hasDone.isEmpty()){
                    setImageObserver(jTree, hasDone);
                }
                if(hashMap.isEmpty()){
                    ISALLDONE = true;
                }
                sleep();
            }
        }

        private void sleep() {
            try {
                Thread.sleep(300000);//five minutes
            } catch (InterruptedException ex) {
                Logger.getLogger(JobManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    private boolean isDone(String id){
        server.connect();
        boolean finish = (server.isDirExist(serverPath + id + "/")
                        &&server.isDirExist(serverPath + id + "/result/"));
        server.connect();
        return finish;
    }
    
    /**
     * refresh icon for one node
     * @param tree
     * @param node 
     */
    public void setImageObserver(JTree tree, IconNode node) {
            ImageIcon icon = (ImageIcon) node.getIcon();
            if (icon != null) {
                icon.setImageObserver(new NodeImageObserver(tree, node));
            }
    }
    
    /**
     * refresh icon for multi nodes
     * @param tree
     * @param nodes 
     */
    public void setImageObserver(JTree tree, List<IconNode> nodes) {
        for (IconNode node : nodes) {
            ImageIcon icon = (ImageIcon) node.getIcon();
            if (icon != null) {
                icon.setImageObserver(new NodeImageObserver(tree, node));
            }
        }
    }
    
    public boolean loadJob(String jobid, String localPath, String name){
        return server.batchDownLoadFile(jobid, localPath, name);
    }
    
    public static JobManager manager(){
        if(jobManager == null)
            return new JobManager();
        else
            return jobManager;
    }

    private static final JobManager jobManager = new JobManager(); //= new JobManager();

}
