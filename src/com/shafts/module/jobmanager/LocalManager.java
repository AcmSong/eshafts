package com.shafts.module.jobmanager;

import java.util.concurrent.ConcurrentHashMap;


/**
 *
 * @author Little-Kitty
 */
public class LocalManager extends ConcurrentHashMap{
    
    private static LocalManager manager;
    
    private LocalManager(){
        
    }
    
    public static LocalManager localJob(){
        if(manager == null)
            manager = new LocalManager();
        return manager;
    }
    
            
            
}
