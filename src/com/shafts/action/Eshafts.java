package com.shafts.action;

import com.shafts.module.LaunchGif;
import com.shafts.utils.WindowClose;
import java.awt.Color;
import java.awt.Insets;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.plaf.BorderUIResource;
import org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper;

/**
 *
 * @author Little-Kitty
 * @date 2014-10-15 13:07:31
 */
public class Eshafts {

    private void enjoy() {
        //MainAction.startUp();
        LaunchGif lg = new LaunchGif();
        long originTime = System.currentTimeMillis();
        lg.setStop(true);
        lg._setVisible();
        MainAction f = new MainAction();
        f.addWindowListener(new WindowClose(f));
        lg.showProgress("Enter system...");
        f._setVisible(true);
        long finishTime = System.currentTimeMillis();
        int time = (int) (finishTime - originTime);
        System.out.println("start time: " + time);
        if (time < 5000) {
            sleep(5000 - time);
        }
        lg.setStop(false);
    }

    private void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            Logger.getLogger(Eshafts.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String args[]) {
        try {
            BeautyEyeLNFHelper.frameBorderStyle
                    = BeautyEyeLNFHelper.FrameBorderStyle.osLookAndFeelDecorated;//.translucencyAppleLike;//.translucencySmallShadow;
            org.jb2011.lnf.beautyeye.BeautyEyeLNFHelper.launchBeautyEyeLNF();
            Border bd = new org.jb2011.lnf.beautyeye.ch8_toolbar.BEToolBarUI.ToolBarBorder(UIManager.getColor(Color.GRAY)//Floatable 时触点的颜色 
                    , UIManager.getColor(Color.BLUE)//Floatable 时触点的阴影颜色 
                    , new Insets(2, 2, 2, 2)); //border 的默认insets
            UIManager.put("ToolBar.border", new BorderUIResource(bd));
            BeautyEyeLNFHelper.translucencyAtFrameInactive = false;
            //UIManager.put("ToolBar.isPaintPlainBackground",Boolean.TRUE);
            UIManager.put("RootPane.setupButtonVisible", false);
            UIManager.put("TabbedPane.tabAreaInsets", new javax.swing.plaf.InsetsUIResource(2, 5, 2, 5));
            //new BEButtonUI().setNormalColor(BEButtonUI.NormalColor.blue);
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Eshafts.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(Eshafts.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            Socket socket = new Socket("localhost", port);
            socket.close();
        } catch (IOException ex) {
            //f = new MainAction();
            new Eshafts().enjoy();
        }
    }

    private static final int port = 55555;
    private static MainAction f;
}
