package com.shafts.action;

import com.result.view.PieChart;
import com.shafts.bridge.CheckNetWork;
import com.shafts.bridge.DecName;
import com.shafts.bridge.LoadPDB;
import com.shafts.bridge.ServerGate;
import com.shafts.compute.JobThreadPool;
import com.shafts.compute.Job;
import com.shafts.compute.Worker;
import com.shafts.module.InitTable;
import com.shafts.module.JobTree;
import com.shafts.module.RunningGif;
import com.shafts.module.ServiceModel;
import com.shafts.module.WaitingGif;
import com.shafts.module.jobmanager.JobManager;
import com.shafts.module.jobmanager.LocalWork;
import com.shafts.render.InitTargetTable;
import com.shafts.render.TipPanel;
import com.shafts.render.TipWindow;
import com.shafts.render.treerender.IconNode;
import com.shafts.render.treerender.IconNodeRenderer;
import com.shafts.ui.EnterKeyUI;
import com.shafts.ui.ExportXlsUI;
import com.shafts.ui.InstallOBUI;
import com.shafts.ui.JcpUI;
import com.shafts.ui.JobStatusUI;
import com.shafts.ui.LocalVisualization;
import com.shafts.ui.PathChooserUI;
import com.shafts.ui.LoginUI;
import com.shafts.ui.MainUI;
import static com.shafts.ui.MainUI.defaultPath;
import com.shafts.ui.ResultSelected;
import com.shafts.ui.StatusUI;
import com.shafts.utils.CharacterValidate;
import com.shafts.utils.FormatConv;
import com.shafts.utils.InfoFactory;
import com.shafts.utils.InitTargetJob;
import com.shafts.utils.InitVector;
import com.shafts.utils.JobInfo_XML;
import com.shafts.utils.file.ListFinder;
import com.shafts.utils.MutiMolDisplay;
import com.shafts.utils.file.MyFileFilter;
import com.shafts.utils.OBChecked;
import com.shafts.utils.PropertyConfig;
import com.shafts.utils.file.TempFile;
import com.shafts.utils.WindowClose;
import com.shafts.utils.decompress.PdbAnalysis;
import com.shafts.utils.decompress.PdbDecompress;
import com.shafts.utils.file.LogFile;
import com.socket.bean.StatusBean;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.plaf.FontUIResource;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import org.ecust.remote.chemmapper.model.ChemmapperServiceModel;

/**
 *
 * @author Little-Kitty
 * @date 2014-10-10 13:02:24
 */
public class MainAction extends MainUI {

    private boolean exitFlag = true; //exit is default
 
    public MainAction() {
        super();
        Color color = new Color(204, 204, 204);
        getContentPane().setBackground(color);
        initSystem();
        setLocation(10, 10);
        setLocationRelativeTo(null);
    }

    public boolean getFlag() {
        return exitFlag;
    }

    public void setFlag(boolean exitFlag) {
        this.exitFlag = exitFlag;
    }

    public void _setVisible(boolean isVisible) {
        //this.setAutoRequestFocus(true);
        scanningPanel.add(new imagePanel());//new TipPanel("Initializing...", "Error"));
        verify();
        initAction();
        initTree();
        scanningPanel.removeAll();
        scanningPanel.add(new TipPanel("Initializing complete", "OK"));
        jobStatus.setText(Worker.jobCount + " / " + 5);
        jobStatus4.setText("version 1.0.2");
        setVisible(isVisible);
    }

    class imagePanel extends javax.swing.JPanel {

        @Override
        public void paint(Graphics g) {
            super.paint(g);
            ImageIcon image = new ImageIcon(defaultPath + "\\Pictures\\init_waiting.gif");
            g.drawImage(image.getImage(), 0, 0, 200, 16, this);
        }
    }

    private void verify() {
        String namePath = defaultPath + "\\configuration\\.namespaces.5";
        File file10 = new File(namePath);
        String keyPath = defaultPath + "\\configuration\\PKCS#12\\digiVeriSign.pem";
        File file5 = new File(keyPath);
        if (!file10.exists()) {
            isAuth = false;
            jButton17.setToolTipText("unlogin");
            jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/unlogin.png"))); // NOI18N
            loginAction();
        } else {
            String userName = DecName.GETNAME();
            jButton9.setText(userName);
            jButton17.setToolTipText("Welcome!");
            jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/online.png"))); // NOI18N
            if (!file5.exists()) {
                isAuth = false;
            } else {
                try {
                    //license = "installed";
                    isAuth = serGate.verify();
                } catch (Exception ex) {
                    Logger.getLogger(MainUI.class.getName()).log(Level.SEVERE, null, ex);
                    isAuth = false;
                }
            }
        }
        if (isAuth) {
            netJobPanel.removeAll();
            netJobPanel.add(netJobParaPanel);
        } else {
            netJobPanel.removeAll();
            netJobPanel.add(netJobLockPanel);
        }
        netJobPanel.updateUI();
    }

    /**
     * init system
     */
    private void initSystem() {

        //cus = new CheckUserStatus();
        serGate = new ServerGate();
        pc = new PropertyConfig();
        if (pc.getConfig("showPathSelection").equals("YES")) {
            new PathChooserUI().setVisible(true);
        }
        workPath = pc.getConfig("workPath");
        File file = new File(workPath);
        if (!file.exists()) {
            file.mkdir();
        }
        localworkPath = workPath + "\\localwork\\";
        File file4 = new File(localworkPath);
        if (!file4.exists()) {
            file4.mkdir();
        }
        String networkPath = workPath + "\\network\\";
        File file1 = new File(networkPath);
        if (!file1.exists()) {
            file1.mkdir();
        }
        targetworkPath = networkPath + "TargetNavigator\\";
        File file2 = new File(targetworkPath);
        if (!file2.exists()) {
            file2.mkdir();
        }
        hitworkPath = networkPath + "HitExplorer\\";
        File file3 = new File(hitworkPath);
        if (!file3.exists()) {
            file3.mkdir();
        }
        defaultPath = System.getProperty("user.dir");

        String tempPath = defaultPath + "\\files\\";
        File file6 = new File(tempPath);
        if (!file6.exists()) {
            file6.mkdir();
        }
        createfilePath = tempPath + "MOL\\";
        File file7 = new File(createfilePath);
        if (!file7.exists()) {
            file7.mkdir();
        }
        pdbfilePath = tempPath + "PDB\\";
        File file8 = new File(pdbfilePath);
        if (!file8.exists()) {
            file8.mkdir();
        }
        mol2filePath = tempPath + "MOL2\\";
        File file9 = new File(mol2filePath);
        if (!file9.exists()) {
            file9.mkdir();
        }
    }

    private void loginAction() {
        new Thread() {
            @Override
            public void run() {
                jButton9.setText("Login");
                login = new LoginUI(jButton9, jButton17);
                //login.setButton(jButton9);
                login.setVisible(true);
                wait = new WaitingGif("Verify account...");
                boolean isLogin = login.getIsLogin();
                if (isLogin) {
                    wait._setVisible(true);
                    //jButton9.setText(login.getAccountName());
                    isAuth = serGate.verify();
                }
                if (isAuth) {
                    netJobPanel.removeAll();
                    netJobPanel.add(netJobParaPanel);
                } else {
                    netJobPanel.removeAll();
                    netJobPanel.add(netJobLockPanel);
                }
                wait.close();
            }
        }.start();
    }

    /**
     * init the action event
     */
    private void initAction() {
        jTable = new JTable();
        //childTable = new JTable();
        IV = new InitVector();
        hasShow = new Hashtable<>();
        //wg = new WaitingGif();
        rg = new RunningGif();
        inittable = new InitTable();
        //loadpdb = new LoadPDB();
        //sf = new Similarity();
        //tiprender = new TipsRender();
        cnw = new CheckNetWork();
        initTargetTable = new InitTargetTable();
        inittargetjob = new InitTargetJob();
        /**
         * create new molecule
         */
        jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                // create new molecule file
                jmolPanel.viewer.evalString("zap");
                System.out.println();
                initPara();
                jcpui = new JcpUI();
                //String filename = (new File(CreateMolecule.RENDER_FILE_NAME)).getName();
                filePath = createfilePath + JcpUI.DD_FILE_NAME;
                jmolPanel.setpath(filePath);
                //enableButton();
                //jTextField1.setText(filename);
                // jTextField1.setToolTipText(filePath);
                jmolPanel.setJcpUI(jcpui);
                jmolPanel.createFileModifyListener();
                jcpui.setVisible(true);
            }
        });

        jMenuItem12.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                String fileName = jTextField1.getText();
                if ("".equals(fileName)) {
                    fileName = (new File(filePath)).getName();
                }
                saveFile(filePath, fileName);
            }
        });

        //exit system
        jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                new WindowClose(MainAction.this).close();
            }
        });

        /**
         * add hyperlink for href
         */
        jEditorPane1.addHyperlinkListener(new HyperlinkListener() {
            @Override
            public void hyperlinkUpdate(HyperlinkEvent e) {
                if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
                    Desktop desktop = Desktop.getDesktop();

                    try {
                        desktop.browse(new URI(e.getURL().toString()));
                    } catch (IOException | URISyntaxException ex) {
                        Logger.getLogger(MainAction.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });

        /**
         * no active panel
         */
        jLabel10.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                boolean isLogin = true;// = false;
                if (jButton9.getText().equals("Login")) {
                    login = new LoginUI(jButton9, jButton17);
                    //login.setButton(jButton9);
                    login.setVisible(true);
                    isLogin = login.getIsLogin();
                    if (isLogin) {
                        jButton9.setText(login.getAccountName());
                        isAuth = serGate.verify();
                    }
                }
                if (isAuth) {
                    netJobPanel.removeAll();
                    netJobPanel.add(netJobParaPanel);
                } else if (isLogin) {
//                        netJobPanel.removeAll();
//                        netJobPanel.add(netJobLockPanel);
                    new EnterKeyUI(isAuth, jButton9.getText(), netJobPanel, netJobParaPanel).setVisible(true);
                }
            }
        });

        /**
         * show job processing status
         */
        jobStatus1.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int x = e.getXOnScreen();
                int y = e.getYOnScreen();
                frame.show(x, y);
            }
        });

        /**
         * format conversion
         */
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int atomNum = jmolPanel.viewer.getAtomCount();
                if (atomNum == 0 || jTextField1.getText().equals("")) {
                    tipPanel1.removeAll();
                    tipPanel1.add(new TipPanel("No molecule to convert!", "Error"));
                } else if ((jTextField1.getText().substring(jTextField1.getText().lastIndexOf(".") + 1)).equals("mol2")) {
                    tipPanel1.removeAll();
                    tipPanel1.add(new TipPanel("This is already a MOL2 file.", "Error"));
                } else {
                    new Thread() {
                        @Override
                        public void run() {
                            wait = new WaitingGif("converting...");
                            wait._setVisible(true);
                            File mol2 = new File(mol2filePath);
                            if (!mol2.exists()) {
                                mol2.mkdir();
                            }
                            String basePath = mol2filePath + "\\" + jTextField1.getText().substring(0, (jTextField1.getText()).lastIndexOf("."));
                            boolean convFlag = new FormatConv().formatConv(filePath, basePath + ".mol2", "mol2");
                            if (convFlag) {
                                filePath = basePath + ".mol2";
                                jTextField1.setText(new File(filePath).getName());
                                jTextField1.setToolTipText(filePath);
                                jmolPanel.viewer.openFile(filePath);
                                jmolPanel.viewer.evalString("hide Hydrogens");
                            }
                            wait.close();
                        }
                    }.start();
                }
            }
        });
        /**
         * optimize
         */
        /*  jButton7.addActionListener(new java.awt.event.ActionListener() {

         @Override
         public void actionPerformed(ActionEvent ae) {
         new Thread() {
         public void run() {
         wg.setvisible(true);
         if (((filePath.substring(filePath.lastIndexOf(".") + 1)).equals("mol2"))) {
         filePath = sf.generateConformer(filePath, "File");
         jTextField1.setText(new File(filePath).getName());
         jTextField1.setToolTipText(filePath);
         jPanel1.updateUI();
         jmolPanel.viewer.openFile(filePath);
         } else {
         String str = "Please converse the file format to mol2 first!";
         tiprender.render(TipLable, str, "Error");
         }
         wg.close();
         }
         }.start();
         }
         });
         String name;
         if (!cnw.netstatus()) {
         name = "Off Line";
         } else {
         name = gbn.getname(isAuth);
         }*/
        // jRadioButton2.setVisible(isAuth);            //********************isAuth
        //jButton9.setText(name);
        /**
         * active component
         */
        jButton9.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionRun();
            }
        });

        /**
         * eastpanel
         */
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            //choose the local database
            @Override
            public void actionPerformed(ActionEvent ae) {
                JFileChooser jfc = new JFileChooser(defaultPath);
                jfc.setMultiSelectionEnabled(false);
                jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
                jfc.setAcceptAllFileFilterUsed(false);
                jfc.addChoosableFileFilter(new MyFileFilter(new String[]{"mol2"}, "Sybyl Mol2 format(*.mol2)"));
                int result = jfc.showOpenDialog(null);
                if (result == JFileChooser.APPROVE_OPTION) {
                    databasePath = jfc.getSelectedFile().getAbsolutePath();
                    File file1 = new File(databasePath);
                    jTextField3.setText(file1.getName());
                    jTextField3.setToolTipText(databasePath);
                }
            }
        });

        jButton11.addActionListener(new java.awt.event.ActionListener() {
            //export the similarity result
            @Override
            public void actionPerformed(ActionEvent ae) {
                ExportXlsUI exportXlsUI = new ExportXlsUI(jTable, showMolPath, tableModel, nodeName);
                exportXlsUI.setTipPanel(tipPanel1);
                exportXlsUI.setVisible(true);
            }
        });
          jButton25.addActionListener(new java.awt.event.ActionListener() {
              //choose the results to view
          @Override
            public void actionPerformed(ActionEvent e) {
                if((treeNode.getParent()).toString().equals("Local")||(treeNode.getParent()).toString().equals("Hit Explorer")){
                  
               ResultSelected rs=new ResultSelected();
               rs.setVisible(true);
               //LocalVisualization lv=new LocalVisualization();
             //  lv.setVisible(true);
                jButton25.setEnabled(false);
                }
                 if((treeNode.getParent()).toString().equals("Target Navigator")){
                       
                    try {
                        PieChart.createPieChart();
                    } catch (IOException ex) {
                        Logger.getLogger(MainAction.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    jButton25.setEnabled(false);
                 }
            }
        });
       

        jButton12.addActionListener(new java.awt.event.ActionListener() {
            // start the shafts work

            @Override
            public void actionPerformed(ActionEvent ae) {
                String input = jTextField1.getText();
                filePath = jTextField1.getToolTipText();
                System.out.println(filePath);
                databasePath = jTextField3.getToolTipText();
                if (input.isEmpty()) {
                    tipPanel2.removeAll();
                    String str = "Please input molecule!";
                    tipPanel2.add(new TipPanel(str, "Error"));
                } else if ((new File(filePath)).length() == 0) {
                    tipPanel2.removeAll();
                    tipPanel2.add(new TipPanel("Invailid File!", "Error"));
                } else if (CharacterValidate.isContainChinese(filePath)) {
                    tipPanel2.removeAll();
                    tipPanel2.add(new TipPanel("File path contains Chinese character!", "Error"));
                } else if ("PDB".equals(molType) || "MULTI".equals(molType)) {
                    if ("PDB".equals(molType)) {
                        tipPanel2.removeAll();
                        tipPanel2.add(new TipPanel("It's a PDB file!", "Error"));
                    } else {
                        tipPanel2.removeAll();
                        tipPanel2.add(new TipPanel("Input can't be muti-mols!", "Error"));
                    }
                } else {
                    switch (shaftsModel) {
                        case 1://local model
                            boolean prepared = isPrepared();
                            if (prepared) {
                                createWork();
                            }
                            break;
                        case 2: //network model
                            if (!(cnw.netstatus())) {
                                String str = "Connection failed! Please check your net work!";
                                tipPanel1.removeAll();
                                tipPanel1.add(new TipPanel(str, "Error"));
                            } else if (screenDB.equals("Choose...")) {
                                //JOptionPane.showMessageDialog(null, "Login first!", "MESSAGE", JOptionPane.INFORMATION_MESSAGE);
                                tipPanel2.removeAll();
                                tipPanel2.add(new TipPanel("Choose a database!", "Error"));
                            } else {
                                //String isExpired = serGate.isExpired(jButton9.getText());
                                ServiceModel serviceModel = new ServiceModel();
                                String Filed2text=jTextField2.getText();
                              //  String StringScreenModel=Integer.toString(screenModel);
                             //   ChemmapperServiceModel model = serviceModel.getModel(filePath, screenModel, Filed2text, programModel, screenDB, threshold);
                                String id = serGate.getJobId(filePath, screenModel, Filed2text, programModel, screenDB, threshold);
                                switch (id) {
                                    case "ISEXPIRED":
                                        tipPanel1.removeAll();
                                        String str = "Authorization has expired! Please renew it. ";
                                        tipPanel1.add(new TipPanel(str, "Error"));
                                        break;
                                    case "ERROR":
                                        tipPanel1.removeAll();
                                        String str1 = "Sorry! The server is under maintenance!";
                                        tipPanel1.add(new TipPanel(str1, "Error"));
                                        break;
                                    case "EXCEPTION":
                                        tipPanel1.removeAll();
                                        String str2 = "Internal error!";
                                        tipPanel1.add(new TipPanel(str2, "Error"));
                                        break;
                                    default:
                                        addnode(id, nodePlace);
                                        String tip1;
                                        //JOptionPane.showMessageDialog(null, "", "MESSAGE", JOptionPane.INFORMATION_MESSAGE);
                                        if (nodePlace == 2) {
                                            jobInfor = new JobInfo_XML();
                                            jobInfor.addXML(id, "TargetNavigator", tmpPath);//org workPath Song 2017.6.13 start后，程序报错，说前言存在。。。
                                            tip1 = "Job " + id + " has been created in TargetNavigator!";
                                            new TipWindow(jobStatus2.getLocationOnScreen().x - 200, jobStatus2.getLocationOnScreen().y - 110, tip1)._setVisible();
                                        } else if (nodePlace == 3) {
                                            jobInfor = new JobInfo_XML();
                                            jobInfor.addXML(id, "HitExplorer", tmpPath);
                                            tip1 = "Job " + id + " has been created in HitExplorer!";
                                            new TipWindow(jobStatus2.getLocationOnScreen().x - 200, jobStatus2.getLocationOnScreen().y - 110, tip1)._setVisible();
                                        }
                                        initPara();
                                        break;
                                }
                            }
                            break;
                        case 0:
                            tipPanel1.removeAll();
                            tipPanel1.add(new TipPanel("Choose the database!", "Error"));
                            break;
                        default:
                            // JOptionPane.showMessageDialog(null, "Unknown Error!", "Message",
                            // JOptionPane.INFORMATION_MESSAGE);
                            break;
                    }
                }
            }
        });

        /**
         * PDB download
         */
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                new Thread() {
                    @Override
                    public void run() {
                        boolean downloadSuc;
                        String url = "https://files.rcsb.org/download/" + pdbCode + ".pdb.gz";//org http://www.rcsb.org/pdb/files/ Song 2017.6.13更换url地址为了解决PDB板块中点击下载按钮无法下载的问题。
                        String savePath = pdbfilePath + pdbCode + ".pdb.gz";
                        if (!((new File(savePath)).exists())) {
                            LoadPDB loadPdb = new LoadPDB();
                            downloadSuc = loadPdb.saveToFile2(url, savePath, pdbCode);
                        } else {
                            downloadSuc = true;
                        }
                        if (downloadSuc) {
                            String loadpdb = "load " + savePath + ";select not protein and not solvent;spacefill off;select not selected;cpk off";
                            jmolPanel.viewer.evalString(loadpdb);//.scriptWait(loadpdb);
                            jComboBox5.setModel(new javax.swing.DefaultComboBoxModel(jmolPanel.getchains()));
                            jComboBox6.setModel(new javax.swing.DefaultComboBoxModel(new String[]{"Select ligand..."}));
                            PdbDecompress decPdb = new PdbDecompress();
                            String tempPath = decPdb.decompress(savePath);
                            if (tempPath != null) {
                                ArrayList inforList = new ArrayList();
                                inforList.add("PDB ID:" + pdbCode);
                                //inforList.add("Load from:http://www.rcsb.org/");
                                inforList = PdbAnalysis.getInfor(tempPath, inforList);
                                String str = InfoFactory.setInfor(inforList);
                                jEditorPane1.setText(str);
                                jEditorPane1.setCaretPosition(0);

                            }
                            tipPanel1.removeAll();
                            tipPanel1.add(new TipPanel("Load complete! ", "Error"));
                        }
                    }
                }.start();
            }
        });

        /**
         * save ligand file to local and open it in jmol
         */
        jButton14.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (!OBChecked.hasInstalled()) {
                    new InstallOBUI().setVisible(true);
                } else {
                    new Thread() {
                        @Override
                        public void run() {
                            File pdbFile;
                            disableButton();
                            wait = new WaitingGif("extracting... ");
                            wait._setVisible(true);
                            filePath = createfilePath + jTextField4.getText() + chainid + ligandid + ".pdb";
                            String command = "write pdb " + filePath;
                            jmolPanel.viewer.scriptWait(command);
                            pdbFile = new File(filePath);
                            String tempFileName = pdbFile.getName();
                            File mol2 = new File(mol2filePath);
                            if (!mol2.exists()) {
                                mol2.mkdir();
                            }
                            String basePath = mol2filePath + "\\" + tempFileName.substring(0, tempFileName.lastIndexOf("."));
                            FormatConv convert = new FormatConv();
                            boolean convFlag = convert.formatConv(filePath, basePath + ".mol2", "mol2");
                            if (convFlag) {
                                filePath = basePath + ".mol2";
                                filePath = basePath + ".mol2";
                                jTextField1.setText(new File(filePath).getName());
                                jTextField1.setToolTipText(filePath);
                                jmolPanel.viewer.openFile(filePath);
                                jmolPanel.viewer.evalString("select carbon/1.1; color [0,255,255]");
                            }
                            wait.close();
                            enableButton();
                            jButton22.setEnabled(false);
                            tipPanel1.removeAll();
                            tipPanel1.add(new TipPanel("finished!", "Error"));
                            pdbFile.delete();
                        }
                    }.start();
                }
            }
        });

        /**
         * collapse or expand the tree
         */
        jButton15.addActionListener(new java.awt.event.ActionListener() {

            int treeFlag = 0; //callopse the tree

            @Override
            public void actionPerformed(ActionEvent e) {
                if (treeFlag == 0) {
                    jobtree.expandAll(jTree1, new TreePath(treeRoot), false);
                    treeFlag = 1;
                } else {
                    jobtree.expandAll(jTree1, new TreePath(treeRoot), true);
                    treeFlag = 0;
                }
                jTree1.updateUI();
            }
        });

        /**
         * remove work
         */
        jButton16.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                initPara();
                jButton11.setVisible(false);
            }
        });

        //same as button2
        jButton17.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                actionRun();
            }
        });

        /**
         * 3D gennerate
         */
        jButton18.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int atomNum = jmolPanel.viewer.getAtomCount();
                if (atomNum == 0 || jTextField1.getText().equals("")) {
                    tipPanel1.removeAll();
                    tipPanel1.add(new TipPanel("No molecule to convert!", "Error"));
                } else {
//                    BabelTipUI babelTipUI = new BabelTipUI();
//                    if ((pc.getConfig("3DgenWarn")).equals("YES")) {
//                        babelTipUI.setVisible(true);
//                        if (babelTipUI.isShowTipAgain()) {
//                            pc.setConfig("3DgenWarn", "YES");
//                        } else {
//                            pc.setConfig("3DgenWarn", "NO");
//                        }
//                    }
                    new Thread() {
                        @Override
                        public void run() {
                            wait = new WaitingGif("building...");
                            wait._setVisible(true);
                            String tempPath = TempFile.moveFile(filePath);
                            boolean isSuc = new FormatConv().Gen3D(tempPath, filePath);
                            if (isSuc) {
                                new File(tempPath).delete();
                                jTextField1.setText(new File(filePath).getName());
                                jTextField1.setToolTipText(filePath);
                                localJobPanel.updateUI();
                                jmolPanel.viewer.openFile(filePath);
                                jmolPanel.viewer.evalString("hide Hydrogens");
                                tipPanel1.removeAll();
                                tipPanel1.add(new TipPanel("Converting finished.", "Error"));
                            } else {
                                TempFile.backFile(tempPath, filePath);
                            }
                            wait.close();
                        }
                    }.start();
                }
            }
        });

        /**
         * update molecule
         */
        jButton21.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int modelNum = jmolPanel.viewer.getModelCount();
                if (modelNum == 0 || modelNum > 1) {
                    tipPanel1.removeAll();
                    tipPanel1.add(new TipPanel("Illegal update!", "Error"));
                } else if (jTextField1.getText().equals("")) {
                    tipPanel1.removeAll();
                    tipPanel1.add(new TipPanel("Please open/create new molecule!", "Error"));
                } else {

                    jmolPanel.viewer.evalString("write mol " + filePath);
                    tipPanel1.removeAll();
                    tipPanel1.add(new TipPanel("Update complete!", "Error"));
                }
                jButton21.setEnabled(false);
            }
        });

        /**
         * show or hide input mol
         */
        jButton23.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showNo = hasShow.size();
                if (isShowInput) {
                    int a = (int) hasShow.get("Input");
                    String b = a + ".1";
                    String controller = "zap " + b + " ;hide Hydrogens";
                    jmolPanel.viewer.evalString(controller);
                    hasShow.remove("Input");
                    isShowInput = false;
                    jButton23.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/hideInput.png"))); // NOI18N
                    jButton23.setToolTipText("Hide input mol");
                    showNo--;
                } else {
                    for (Map.Entry entry : hasShow.entrySet()) {
                        int value = (int) entry.getValue();
                        if (showNo < value) {
                            showNo = value;
                        }
                    }
                    showNo++;
                    hasShow.put("Input", showNo);
                    String queryfilePath = showMolPath + "Input.mol2";
                    String controller1 = "load APPEND " + "\"" + queryfilePath
                            + "\"" + " ;frame*" + " ;hide Hydrogens"
                            + ";select carbon/" + showNo + ".1; color [0,255,255]; center "
                            + showNo + ".1"; // first row is the query mol                                   
                    jmolPanel.viewer.evalString(controller1);
                    isShowInput = true;
                    jButton23.setIcon(new javax.swing.ImageIcon(getClass().
                            getResource("/pic/showInput.png"))); // NOI18N
                    jButton23.setToolTipText("Show input mol");
                }
            }
        });
      

        /**
         * show surface
         */
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                if (jToggleButton1.isSelected()) {
                    int a = hasShow.get("Input");
                    String b = a + ".1";
                    String controller = "select " + b + ";isosurface delete resolution 0 solvent 1.4 translucent";
                    jmolPanel.viewer.evalString(controller);
                    jToggleButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/VDW.png")));
                    jToggleButton1.setToolTipText("Show VDW");
                } else {
                    String controller = "isosurface off";
                    jmolPanel.viewer.evalString(controller);
                    jToggleButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/NOVDW.png")));
                    jToggleButton1.setToolTipText("Hide VDW");
                }
            }
        });

        logOut.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                logout();
            }
        });

        checkInfo.addActionListener(new java.awt.event.ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                checkInfo();
            }
        });
    }

    private void actionRun() {
        String name = jButton9.getText();
        switch (name) {
            case "Login":
                loginAction();
                break;
            default:
                userPop.show(jButton9, 0, jButton9.getY() + jButton9.getHeight());

        }
    }

    /**
     * log out current account
     */
       public  static String getCurrentPath(){
           String CurrentPath=showMolPath;
              return CurrentPath;
          }
    private void logout() {
        int sure = JOptionPane.showConfirmDialog(null,
                "Log out will lock the remote module. \nAre you sure to do that?",
                "Tip", JOptionPane.YES_NO_OPTION);
        wait = new WaitingGif("Loging out...", 3000);
        if (sure == JOptionPane.YES_OPTION) {
            wait._setVisible(true);
            String namePath = defaultPath + "\\configuration\\.namespaces.5";
            File loginFile = new File(namePath);
            try {
                loginFile.delete();
                netJobPanel.removeAll();
                netJobPanel.add(netJobLockPanel);
                netJobPanel.updateUI();
                jButton9.setText("Login");
                jButton9.setToolTipText("unlogin");
                jButton17.setToolTipText("unlogin");
                jButton17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/pic/unlogin.png")));
                wait.close();
            } catch (Exception e) {
                wait.close();
                JOptionPane.showConfirmDialog(null, "Logout error! Error: \n" + e, "Error", JOptionPane.OK_OPTION);
                Logger.getLogger(MainAction.class.getName()).log(Level.SEVERE, null, e);
            }
            //jToolBar2.updateUI();           
        }
    }

    /**
     * check user base info.
     */
    private void checkInfo() {
        String name = jButton9.getText();
        String StatusString = serGate.getStatusInfor(name);
        String strArray[]=StatusString.split(" ");
        String authorStatus=strArray[0];
        int leftDays=Integer.parseInt(strArray[1]);
        boolean isOrder;
        isOrder = serGate.verify();
        //userName = jButton9.getText();
        new StatusUI(isOrder, name, authorStatus, leftDays,  netJobPanel,netJobParaPanel).setVisible(true);
    }

//    class Converting extends Thread {
//
//        WaitingGif wait;
//        public Converting() {
//
//        }
//
//        public Converting(WaitingGif wait) {
//            this.wait = wait;
//        }
//
//        @Override
//        public void run() {
//            wait._setVisible(true);
//        }
//    }
    /**
     * check the loacl work if prepared
     *
     * @return
     */
    private boolean isPrepared() {
        boolean available = false;
        if (databasePath == null) {
            String str = "Database can't be null!";
            tipPanel2.removeAll();
            tipPanel2.add(new TipPanel(str, "Error"));
        } else if (filePath.contains(" ") || databasePath.contains(" ")) {
            String str = "Mol or database path contains blank space!";
            tipPanel2.removeAll();
            tipPanel2.add(new TipPanel(str, "Error"));
        } else if (!((databasePath.substring(databasePath.lastIndexOf(".") + 1)).equals("mol2"))) {
            String str = "Database format must be mol2!";
            tipPanel2.removeAll();
            tipPanel2.add(new TipPanel(str, "Error"));
        } else if (CharacterValidate.isContainChinese(databasePath)) {
            tipPanel2.removeAll();
            tipPanel2.add(new TipPanel("File path contains Chinese character!", "Error"));
        } else {
            available = true;
        }

        return available;
    }

    /**
     * create a new local work
     */
    private void createWork() {
        genConformer = jCheckBox1.isSelected();
        //是否选择多构象
        if (genConformer) {
            // set the conformer param
            cyndi.updateParam(jTextField5.getText());
        }
        //ThreadCount++;
        String jobId = LocalWork.genId(localworkPath);
        if (jobId == null) {
            tipPanel1.removeAll();
            tipPanel1.add(new TipPanel("Can't generate work id! ", "Error"));
        } else {
            Job job = new Job();
            String str = "Job " + jobId + " has been created!";
            //tipPanel1.removeAll();
            //tipPanel1.add(new TipPanel(str, "Error"));
            new TipWindow(jobStatus2.getLocationOnScreen().x - 200, jobStatus2.getLocationOnScreen().y - 110, str)._setVisible();
            IconNode iconNode = addnode(jobId, shaftsModel);
            showMolPath = localworkPath + jobId + "\\";
            jobInfor.addXML(jobId, "Local", tmpPath);
            job.setStorePath(showMolPath);
            job.setInMolPath(filePath);
            job.setDbPath(databasePath);
            job.setMaxOutNum(jTextField2.getText());
            job.setThreshold(threshold);
            job.setGenConformer(genConformer);
            job.setJobId(jobId);
            Thread worker = new Worker(tmpPath, job, jTree1, iconNode, jobInfor, jobStatus2, jobStatus);
            jobPool.execute(worker);
            //waiting
        }

        initPara();
    }

    /**
     * show the similarity result
     */
    class showTable extends Thread {

        @Override
        public void run() {
            wait = new WaitingGif("loading...");
            wait._setVisible(true);
            initPara();
            nodeName = treeNode.toString();
            ResultPath = jobtree.getpath((treeNode.getParent()).
                    toString(), localworkPath, targetworkPath, hitworkPath);
            showMolPath = ResultPath + nodeName + "\\";
            String name = new ListFinder().GetList(showMolPath);
            String str;
            boolean resultComplete = true;
            if (name == null && (treeNode.getParent().toString().
                    equals("Target Navigator") || treeNode.getParent().
                    toString().equals("Hit Explorer"))) {
                wait.close();
                resultComplete = manager.loadJob(nodeName, showMolPath, nodeName);//sftp.batchDownLoadFile(nodeName, showMolPath);
                //sftp.disconnect();
                if (resultComplete) {
                    wait = new WaitingGif("Parsing " + nodeName + " ...");
                    wait._setVisible(true);
                }
                name = new ListFinder().GetList(showMolPath);
            } else if (name == null && treeNode.getParent().toString().equals("Local")) {
                resultComplete = false;
                String localcomplete = jobInfor.getJobStatus(nodeName, "Local", tmpPath);
                switch (localcomplete) {
                    case "NO":
                        str = nodeName + " Still running! Please waiting... ";
                        tipPanel1.removeAll();
                        tipPanel1.add(new TipPanel(str, "Error"));
                        break;
                    case "YES":
                        str = " Invalid job! ";
                        tipPanel1.removeAll();
                        tipPanel1.add(new TipPanel(str, "Error"));
                        break;
                }
            }
            if (resultComplete) {
                jToggleButton1.setEnabled(true);
                filePath = showMolPath + "Input.mol2";
                if (new File(filePath).exists()) {
                    String controller1 = "load APPEND " + "\""
                            + filePath + "\"" + " ;frame*" + " ;hide Hydrogens"
                            + ";select carbon/1.1;color [0,255,255]; center 1.1";
                    jmolPanel.viewer.evalString(controller1);
                    jTextField1.setText((new File(filePath)).getName());
                    jTextField1.setToolTipText(filePath);
                    if (!hasShow.isEmpty()) {
                        hasShow.clear();
                    }
                    hasShow.put("Input", 1);
                    enableButton();
                
                }
                String path = showMolPath + name;
                data = IV.getdata(path);
                if (1 < data.size()) {
                    ArrayList inforList = new ArrayList();
                    jTable = new JTable();
                    resultShowPanel.removeAll();
                    jScrollPane4 = new JScrollPane();
                    jScrollPane4.setBorder(null);
                    if (treeNode.getParent().toString().equals("Target Navigator")) {
                        tableModel = "target";
                        XMLpath = showMolPath + new ListFinder().getXML(showMolPath);
                        InitTargetTable.path = XMLpath;
                        initTargetTable.setpara(jmolPanel, showMolPath, hasShow, filePath);
                        jTable = initTargetTable.getTargetTable();
                        jTable.addMouseListener(new showInfor());
                        isShowInput = true;
                        jButton23.setEnabled(true);
                    } else {
                        tableModel = "other";
                        jTable = inittable.getTable(data, InitVector.getcolumn());
                        jTable.addMouseListener(new MutiMolDisplay(showMolPath, jTable, jmolPanel, hasShow, "NoTargetInfor", filePath));
                    }
                    inforList.add("Job Name:" + treeNode.toString());
                    inforList.add("Job Type:" + treeNode.getParent().toString());
                    if ("target".equals(tableModel)) {
                        inforList.add("Hits Num:" + (jTable.getRowCount()));
                    } else {
                        inforList.add("Hits Num:" + (jTable.getRowCount() - 1));
                    }
                    //inforList.add("More Tips:You can select multi mols to show, try it!");
                    String tableInfo = InfoFactory.setInfor(inforList);
                    jEditorPane1.setText(tableInfo);
                    jEditorPane1.setCaretPosition(0);
                    jScrollPane4.setViewportView(jTable);
                    resultShowPanel.add(jScrollPane4);
                    resultShowPanel.updateUI();
                    jButton11.setVisible(true);
                } else {
                    tipPanel1.removeAll();
                    tipPanel1.add(new TipPanel("No result! The log will be shown...", "Error"));
                    JTextArea log = new LogFile().getLog(showMolPath + "log.out");
                    jScrollPane3 = new JScrollPane();
                    jScrollPane3.setViewportView(log);
                    resultShowPanel.removeAll();
                    resultShowPanel.add(jScrollPane3);
                    resultShowPanel.updateUI();
                }
            }
            wait.close();
        }
    }

    class showInfor extends MouseAdapter {

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                int row = jTable.getSelectedRow();
                String targetName;
                targetName = (String) jTable.getValueAt(row, 0);
                // System.out.println(XMLpath + "*********" + targetName);
                String target = inittargetjob.getHtml(XMLpath, targetName);
                if (target == null) {
                    target = InfoFactory.baseInfor();
                }
                jEditorPane1.setText(target);
                jEditorPane1.setCaretPosition(0);
            }
        }
    }

    /**
     * job tree mouse event
     *
     * @author baoabo
     *
     */
    class TreeHandle extends MouseAdapter {

        boolean F = false;

        @Override
        public void mousePressed(final MouseEvent e) {
            if (e.getButton() == MouseEvent.BUTTON1) {
                F = true;
                JTree jTree = (JTree) e.getSource();
                int rowLocation = jTree.getRowForLocation(e.getX(), e.getY());
                TreePath treepath = jTree.getPathForRow(rowLocation);
                if (treepath == null) {
                    F = false;
                    if (IsRename == 1) {//do rename operation
                        jTree.stopEditing();
                        String newName = ((DefaultMutableTreeNode) jTree1.getLastSelectedPathComponent()).toString();
                        boolean renameflag = jobInfor.updateXML(oldName, newName, nowNode.getParent().toString(),
                                tmpPath, ResultPath);
                        if (renameflag) {
                            //jTree.stopEditing();

                            jTree1.clearSelection();
                            IsRename = 0;
                        } else {
                            IsRename = 0;
                            jTree1.startEditingAtPath(jTree1.getSelectionPath());

                        }
                    } else {
                        jTree1.cancelEditing();
                        jTree1.clearSelection();
                    }
                } else {
                    treeNode = (TreeNode) treepath.getLastPathComponent();
                    if (!treeNode.isLeaf() || (treeNode.getParent()).
                            toString().equals("My Work") || (treeNode.getParent()).
                            toString().equals("Remote")) {
                        F = false;
                    }
                    // System.out.println(nodeName);
                }
            }
            if (e.isMetaDown()) { // right click
                if (F) {
                    nodeName = treeNode.toString();
                    setmenupop();
                    pm.show(jTree1, e.getX(), e.getY());
                }
            }
            if (e.getClickCount() == 2 && F) {
                jButton21.setEnabled(false);
                new showTable().start();
                jButton25.setEnabled(true);
               
                
            }
        }
    }

    /**
     * right mouse click event
     */
    private void setmenupop() {
        pm = new JPopupMenu();
        JMenuItem menuitem1 = new JMenuItem();
        JMenuItem menuitem2 = new JMenuItem();
        JMenuItem menuitem3 = new JMenuItem();
        menuitem1.setText("Rename...");
        menuitem1.addActionListener(new ActionListener() { // rename the file
            @Override
            public void actionPerformed(ActionEvent e) {
                jTree1.setEditable(true);
                nowNode = (DefaultMutableTreeNode) jTree1.getLastSelectedPathComponent();
                if (nowNode.isLeaf()) {
                    oldName = nowNode.toString();
                    ResultPath = jobtree.getpath((nowNode.getParent()).toString(), localworkPath, targetworkPath, hitworkPath);
                    jTree1.startEditingAtPath(jTree1.getSelectionPath());
                    IsRename = 1;
                }
            }
        });
        menuitem2.setText("Open Path");
        menuitem2.addActionListener(new ActionListener() { // open the job
            // directory
            @Override
            public void actionPerformed(ActionEvent e) {
                ResultPath = jobtree.getpath((treeNode.getParent()).toString(), localworkPath, targetworkPath, hitworkPath);
                try {
                    String filepath = ResultPath + nodeName;
                    Runtime.getRuntime().exec("explorer.exe /select," + filepath);
                } catch (IOException e1) {
                    Logger.getLogger(MainAction.class.getName()).log(Level.SEVERE, null, e1);
                }
            }
        });
        menuitem3.setText("Delete...");
        menuitem3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ResultPath = jobtree.getpath((treeNode.getParent()).toString(), localworkPath, targetworkPath, hitworkPath);
                String filepath = ResultPath + nodeName;
                // System.out.println(filepath);
                String localcomplete = jobInfor.getJobStatus(nodeName, "Local", tmpPath);
                if ("NO".equals(localcomplete) && ResultPath.equals(localworkPath)) {
                    JOptionPane.showMessageDialog(null, "Can't remove running job of local!",
                            "Tip", JOptionPane.OK_OPTION);
                } else {
                    int i = JOptionPane.showConfirmDialog(null, "Do you want to delete the source file at the same time?",
                            "Delete", JOptionPane.YES_NO_OPTION);//org YES_NO_CANCEL_OPTION Song 2017.6.13 为了美观去除了取消按钮
                 //   wait = new WaitingGif("Deleting...");
                  //  wait._setVisible(true);//org 有这两行，Song 2017.6.13 为了解决删除文件时选择取消依然出现删除缓冲条，选择将这两行注释掉，移到了下面
                    if (i == JOptionPane.OK_OPTION) {
                         File file = new File(filepath);
                          
                        try {
                          
                            if (file.isFile()) {
                                //org 没这两行 Song 2017.6.13 将上面的两行放到了这里,控制选择是的时候出现删除缓冲条
                                 wait = new WaitingGif("Deleting...");
                                 wait._setVisible(true);
                                file.delete();
                            }
                            if (file.isDirectory()) {
                                File files[] = file.listFiles();
                                for (File file1 : files) {
                                    //将上面的两行放到了这里。
                                     wait = new WaitingGif("Deleting...");
                                     wait._setVisible(true);
                                    file1.delete();
                                }
                                file.delete();
                                 
                     
                            }
                            jobInfor.deleteXML(nodeName, treeNode.getParent().toString(), tmpPath);
                            delNode((DefaultMutableTreeNode) treeNode);
                            nodeName = null;
                            wait.close();
                        } catch (Exception e1) {
                            wait.close();
                            //tiprender.render(TipLable, "Delete has been interrupt! ", "Error");
                            tipPanel1.removeAll();
                            tipPanel1.add(new TipPanel("Delete has been interrupt! ", "Error"));
                            Logger.getLogger(MainAction.class.getName()).log(Level.SEVERE, null, e1);
                        }
                    } else if (i == JOptionPane.NO_OPTION) {
                        //jobInfor.deleteXML(nodeName, treeNode.getParent().toString(), tmpPath);
                       // delNode((DefaultMutableTreeNode) treeNode);
                        nodeName = null;
                    }
                }
            }
        });
        pm.add(menuitem1);
        pm.add(menuitem2);
        pm.add(menuitem3);
    }

    /**
     * add tree node
     *
     * @param node
     * @param model add where
     * @return
     */
    public IconNode addnode(String node, int model) {
        IconNode iconNode = new IconNode(node);
        iconNode.setIcon(new ImageIcon(MainAction.class.getResource("/pic/computing.gif")));
        switch (model) {
            case 1:
                localworkNode.add(iconNode);
                break;
            case 2:
                targetNode.add(iconNode);
                manager.addJob("TargetNavigator", iconNode);
                break;
            case 3:
                hitNode.add(iconNode);
                manager.addJob("HitExplorer", iconNode);
                break;
        }
        manager.setImageObserver(jTree1, iconNode);
        jTree1.updateUI();
        return iconNode;
    }

    /**
     * delete local work tree node
     *
     * @param node
     */
    public void delNode(DefaultMutableTreeNode node) {
        DefaultTreeModel model = (DefaultTreeModel) jTree1.getModel();
        model.removeNodeFromParent(node);
        jTree1.updateUI();

    }

    /**
     * init the job tree
     */
    private void initTree() {
        jobtree = new JobTree();
        iconNodes = new ArrayList();
        noDone = new HashMap();
        localworkNode.removeAllChildren();
        targetNode.removeAllChildren();
        hitNode.removeAllChildren();

        String path = workPath + "\\TMP";
        File f = new File(path);
        if (!f.exists()) {
            f.mkdir();
        }
        tmpPath = path + "\\jobinfor.xml";
        jobInfor = new JobInfo_XML();
        jobInfor.getAllJob(tmpPath);
        HashMap local = jobInfor.getLocal();
        HashMap target = jobInfor.getTarget();
        HashMap hit = jobInfor.getHit();
        if (!local.isEmpty()) {
            String nodeId;
            String status;
            Iterator it = local.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                nodeId = (String) entry.getKey();
                status = (String) entry.getValue();
                IconNode node = new IconNode(nodeId);
                iconNodes.add(node);
                localworkNode.add(node);
                if (null != status) {
                    switch (status) {
                        case "YES":
                            node.setIcon(new ImageIcon(MainAction.class.getResource("/pic/done.png")));
                            break;
                        case "NO":
                            node.setIcon(new ImageIcon(MainAction.class.getResource("/pic/computing.gif")));
                            break;
                    }
                }
            }
        }
        if (!target.isEmpty()) {
            String nodeId;
            String status;
            Iterator it = target.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                nodeId = (String) entry.getKey();
                status = (String) entry.getValue();
                IconNode node = new IconNode(nodeId);
                iconNodes.add(node);
                targetNode.add(node);
                if (null != status) {
                    switch (status) {
                        case "YES":
                            node.setIcon(new ImageIcon(MainAction.class.getResource("/pic/done.png")));
                            break;
                        case "NO":
                            noDone.put(node, "TargetNavigator");
                            node.setIcon(new ImageIcon(MainAction.class.getResource("/pic/computing.gif")));
                            break;
                    }
                }
            }
        }
        if (!hit.isEmpty()) {
            String nodeId;
            String status;
            Iterator it = hit.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry entry = (Map.Entry) it.next();
                nodeId = (String) entry.getKey();
                status = (String) entry.getValue();
                IconNode node = new IconNode(nodeId);
                iconNodes.add(node);
                hitNode.add(node);
                if (null != status) {
                    switch (status) {
                        case "YES":
                            node.setIcon(new ImageIcon(MainAction.class.getResource("/pic/done.png")));
                            break;
                        case "NO":
                            noDone.put(node, "HitExplorer");
                            node.setIcon(new ImageIcon(MainAction.class.getResource("/pic/computing.gif")));
                            break;
                    }
                }
            }
        }
        jTree1.setEditable(false);
        jTree1.setCellRenderer(new IconNodeRenderer());
        jTree1.scrollPathToVisible(new TreePath(treeRoot.getPath()));
        jobtree.expandAll(jTree1, new TreePath(treeRoot), true);
        jTree1.addMouseListener(new TreeHandle());
        manager.setHashMap(noDone);
        manager.setXmlPath(tmpPath);
        manager.setjTree(jTree1);
        manager.setImageObserver(jTree1, iconNodes);
        jTree1.updateUI();
        //jTree1.setCellRenderer(new TreeRender());
    }

    /**
     * remove all the running local node used when forced to exit the system and
     * don't let the job running in backgroud
     *
     * @return
     */
    public boolean removeAllRunNode() {
        String path = workPath + "\\TMP";
        File f = new File(path);
        if (f.exists()) {
            tmpPath = path + "\\jobinfor.xml";
            jobInfor = new JobInfo_XML();
            jobInfor.getAllJob(tmpPath);
            HashMap local = jobInfor.getLocal();
            System.out.println("Running jobs: " + local.size());
            if (!local.isEmpty()) {
                String nodeId;
                String status;
                Iterator it = local.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry entry = (Map.Entry) it.next();
                    nodeId = (String) entry.getKey();
                    status = (String) entry.getValue();
                    IconNode node = new IconNode(nodeId);
                    localworkNode.add(node);
                    if ("NO".equals(status)) {
                        jobInfor.deleteXML(nodeId, node.getParent().toString(), tmpPath);
                        System.out.println("delete running job: " + nodeId);
                        delNode((DefaultMutableTreeNode) node);
                    }
                }
            }
        }
        return true;
    }

    public int getRunningJob() {
        return Worker.jobCount;
    }



    //设置全局字体
    public static void initGlobalFontSetting(Font fnt) {
        FontUIResource fontRes = new FontUIResource(fnt);
        for (Enumeration keys = UIManager.getDefaults().keys(); keys.hasMoreElements();) {
            Object key = keys.nextElement();
            Object value = UIManager.get(key);
            if (value instanceof FontUIResource) {
                UIManager.put(key, fontRes);
            }
        }
  
    }
    
      
    private ArrayList<IconNode> iconNodes;
    private HashMap noDone;

    private final JobStatusUI frame = JobStatusUI.getFrame();

    private JcpUI jcpui;
    // private int status;
    private int IsRename;//rename flag 
    private int showNo;//record the opened molfile num
    private boolean isShowInput;
    //private int ThreadCount = 0;
    //private String userName;
    //private String license = "uninstalled";
    private String tmpPath;
    private String workPath;
    private String databasePath;
    private String localworkPath;
    private String targetworkPath;
    private String hitworkPath;
    private String createfilePath;
    private static  String showMolPath;
    private String pdbfilePath;//pdb file path
    private String mol2filePath; //mol2 file path
    private String ResultPath;
    private String oldName;
    private String nodeName;
    private boolean genConformer; // whether generate conformer
    private String XMLpath;
    private boolean isAuth;
    private final JobManager manager = JobManager.manager();
    //private boolean resultcomplete = true;
    private final ExecutorService jobPool = JobThreadPool.threadPool();
    private Hashtable<String, Integer> hasShow;
    private String tableModel;
    private PropertyConfig pc;
    private JobInfo_XML jobInfor;
    private CheckNetWork cnw;
    private InitVector IV;
    private InitTargetTable initTargetTable;
    private JobTree jobtree;
    //private Similarity sf;
    private WaitingGif wait;
    private RunningGif rg;
    public InitTable inittable;
    private InitTargetJob inittargetjob;
    private LoginUI login;
    private ServerGate serGate;
    private StatusBean statusBean;
    private Vector data;
    private JTable jTable;
    private DefaultMutableTreeNode nowNode;
    private TreeNode treeNode;
    private JPopupMenu pm;
}
