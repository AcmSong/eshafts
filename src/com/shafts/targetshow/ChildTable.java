package com.shafts.targetshow;

import java.util.HashMap;

/**
 *
 * @author Little-Kitty
 */
public class ChildTable extends HashMap{
    
    public void add(ChildRow row){
        put(row.getId(), row);
    }
    
    public ChildRow getRow(String id){
        return (ChildRow)get(id);
    }
    
}
