package com.shafts.targetshow;

/**
 * design for target result
 * @author Little-Kitty
 */
public class ChildRow {
    private String id;
    private String name;
    private String database;
    private String activity;
    private String score;
    private String smiles;
    private boolean select;

    public ChildRow() {
    }

    public ChildRow(String id, String name, String database, String activity, String score, String smiles, boolean select) {
        this.id = id;
        this.name = name;
        this.database = database;
        this.activity = activity;
        this.score = score;
        this.smiles = smiles;
        this.select = select;
    }    
    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getSmiles() {
        return smiles;
    }

    public void setSmiles(String smiles) {
        this.smiles = smiles;
    }

    public boolean isSelect() {
        return select;
    }

    public void setSelect(boolean select) {
        this.select = select;
    }
        
        
}
