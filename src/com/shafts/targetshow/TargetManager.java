package com.shafts.targetshow;

import com.shafts.utils.TableSorter;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.Vector;

/**
 * design for manager the global target infor
 * @author Little-Kitty
 */
public class TargetManager extends HashMap{
    
    private TargetManager(){
        
    }
    /**
     * add a child table
     * @param keyId
     *          the father table'index
     * @param data 
     */
    public void addTable(String keyId, Vector data){
        HashMap childTable = changeToHashMap(data);
        put(keyId, childTable);        
    }
    
    public HashMap getTable(String keyId){
        return (HashMap)get(keyId);
    }
    
    public Vector getData(String keyId){
       Vector data;
       HashMap table = (HashMap)get(keyId);
       if(table != null)
           data = changeToVector((HashMap)get(keyId));
       else
           data = null;
       return data;
    }
    
    
    // change the Vector type to HashMap
    private HashMap changeToHashMap(Vector data){
        HashMap table = new HashMap();
        for(int i = 0; i < data.size(); i++){
            ChildRow row = new ChildRow();
            Vector rowData = (Vector) data.get(i);
            row.setId((String) rowData.get(0));
            row.setName((String) rowData.get(1));
            row.setDatabase((String) rowData.get(2));
            row.setActivity((String) rowData.get(3));
            row.setScore((String) rowData.get(4));
            row.setSmiles((String) rowData.get(5));
            row.setSelect((boolean) rowData.get(6));
            table.put((String) rowData.get(0), row);
        }        
        return table;
    }
    
    //change the HashMap type to Vector
    private Vector changeToVector(HashMap childTable){
        Vector data = new Vector();
        Set<Entry<String, ChildRow>> entrySet =childTable.entrySet();
        for(Entry<String, ChildRow> entry: entrySet){
            Vector row = new Vector();
           ChildRow childRow = (ChildRow)entry.getValue();
            row.add(childRow.getId());
            row.add(childRow.getName());
            row.add(childRow.getDatabase());
            row.add(childRow.getActivity());
            row.add(childRow.getScore());
            row.add(childRow.getSmiles());
            row.add(childRow.isSelect());
            data.add(row);
        }
        //sort the result with score
        data = new TableSorter().sortTable(data, 4, 0);
        return data;
    }
    
    public final static TargetManager TARGET_MANAGER = new TargetManager();
    
}
