/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.result.view;

import static com.shafts.action.MainAction.getCurrentPath;
import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.data.general.DefaultPieDataset;

/**
 *
 * @author 18300
 */
public class PieChart {

    /**
     * @param args the command line arguments
     */
    public static void createPieChart() throws IOException {
        // TODO code application logic here
        // create a dataset...
        DefaultPieDataset data = new DefaultPieDataset();
        String CureentPath = getCurrentPath();
        System.out.println(CureentPath);
        ArrayList<String> fileList = new ArrayList<String>();
        FileFilter.filterfile(CureentPath, "xml", fileList);
        String name = null;
        for (String s : fileList) {
            name = CureentPath+s;
        }
        String fun = "fun=";
        String fun1 = "fun=\"nucleic acid binding transcription factor activity\"";
        String fun2 = "fun=\"enzyme regulator activity\"";
        String fun3 = "fun=\"transporter activity\"";
        String fun4 = "fun=\"molecular transducer activity\"";
        String fun5 = "fun=\"catalytic activity\"";
        String fun6 = "fun=\"binding\"";
        int num = GetFuncNum.count(name, fun);
        int num1 = GetFuncNum.count(name, fun1);
        int num2 = GetFuncNum.count(name, fun2);
        int num3 = GetFuncNum.count(name, fun3);
        int num4 = GetFuncNum.count(name, fun4);
        int num5 = GetFuncNum.count(name, fun5);
        int num6 = GetFuncNum.count(name, fun6);
        int num7 = num - num1 - num2 - num3 - num4 - num5 - num6;

        data.setValue("nucleic acid binding transcription factor activity", num1);
        data.setValue("enzyme regulator activity", num2);
        data.setValue("transporter activity", num3);
        data.setValue("molecular transducer activity", num4);
        data.setValue("catalytic activity", num5);
        data.setValue("binding", num6);
        data.setValue("Other", num7);
// create a chart...
        JFreeChart chart = ChartFactory.createPieChart3D(
                "Function Classification",
                data,
                true, // legend?
                true, // tooltips?
                false // URLs?
        );
        PiePlot plot = (PiePlot) chart.getPlot();
        plot.setSectionPaint("Category 1", new Color(255, 218, 185));
        plot.setSectionPaint("Category 2", new Color(106, 90, 205));
        plot.setSectionPaint("Category 3", new Color(135, 206, 235));
        plot.setSectionPaint("Category 1", new Color(0, 255, 255));
        plot.setSectionPaint("Category 1", new Color(240, 128,128));
        plot.setSectionPaint("Category 1", new Color(218, 112, 214));
        plot.setSectionPaint("Category 1", new Color(255, 99, 71));

        // plot.setExplodePercent("Category 1", 0.30);//扇块爆炸出来
        plot.setIgnoreNullValues(true);//忽略空值与零值
        plot.setIgnoreZeroValues(true);
        plot.setSectionOutlinesVisible(false); //隐藏与显示轮廓线
        plot.setSectionOutlinesVisible(true);

// create and display a frame...
        ChartFrame frame = new ChartFrame("First", chart);
        frame.pack();
        frame.setVisible(true);
    }

    public static void main(String[] args) throws IOException {
        createPieChart();

    }
}
