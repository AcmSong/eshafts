/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.result.view;



import static java.lang.Math.log10;


/**
 *
 * @author 18300
 */
public class Normalization {
 public static void main(String args[]) throws Exception {
        double data[] = new double[6];
        data[0] = 0.9561;
        data[1] = 0.8934;
        
        Normalization mm = new Normalization();
        double nData[] = mm.normal(data);
        for (int i = 0; i < 6; i++) {
            System.out.println(nData[i]);
        }
    }

    public double[] normal(double data[]) {

        int n = data.length;
        double Ndata[] = new double[n];
        for(int i=0;i<n;i++){
           
             Ndata[i]=log10(1+data[i]);
        }
        return Ndata;
    }

 
}
