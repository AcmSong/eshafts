/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.result.view;

import org.openbabel.OBConversion;
import org.openbabel.OBMol;

/**
 *
 * @author 18300
 */
public class FormatChange {

    public static void main(String[] args) {
      change("E:\\ZINC03812962.mol2", "E:\\2.smiles");
      change("E:\\ZINC03814501.mol2", "E:\\1.smiles");
       

    }

    public static void change(String mol2Path, String smilesPath) {
        String smiles = null;
       // System.loadLibrary("openbabel_java");
        OBConversion conv = new OBConversion();
        OBMol mol = new OBMol();
        conv.SetInFormat("mol2");
        conv.ReadFile(mol, mol2Path);
        conv.SetOutFormat("smiles");
        conv.SetOptions("-x1", OBConversion.Option_type.OUTOPTIONS);
        conv.WriteFile(mol, smilesPath);

    }
}
