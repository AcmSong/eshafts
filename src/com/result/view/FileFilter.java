/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.result.view;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Arrays;

public class FileFilter implements FilenameFilter
{
    private String type; // 后缀名
    public FileFilter(String type)
    {
        this.type = type;
    }
    public boolean accept(File dir, String name)
    {
        return name.endsWith(type);
    }
    static void filterfile(String dir, String suffix, ArrayList<String> fileList)
    {
        File f = new File(dir);
        File [] myList = f.listFiles();
        FileFilter filter = new FileFilter(suffix);
        String [] files = f.list(filter);
        fileList.addAll(Arrays.asList(files));
        for (File a : myList)
        {
            if (a.isDirectory())
            {
                    filterfile(a.toString(), suffix, fileList);
            } 
        }
    }
    public static void main(String[] args)
    {
        ArrayList<String> fileList = new ArrayList<String>();
        FileFilter.filterfile("E:\\Ecust\\project\\Code\\lib-eshafts(2017.11.21)\\workspace\\network\\TargetNavigator\\59086\\", "xml", fileList);
        for(String s:fileList)
        {
            System.out.println(s);
        }
    }
}