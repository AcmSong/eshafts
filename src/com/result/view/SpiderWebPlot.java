/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.result.view;

import static com.shafts.action.MainAction.getCurrentPath;
import com.shafts.utils.file.ListFinder;
import org.RDKit.RDKFuncs;
import org.RDKit.RWMol;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.title.LegendTitle;
import org.jfree.chart.title.TextTitle;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.RectangleEdge;



/**
 *
 * @author 18300
 */
public class SpiderWebPlot {

    double MaxNum = 0;

    public JFreeChart createChart(int m, int n) {
        CalibrationSpiderWebPlot spiderwebplot = new CalibrationSpiderWebPlot(createDataset(m, n), MaxNum, true, 6);
        //使用SpiderWebPlot
        JFreeChart jfreechart = new JFreeChart("Visualization of Descriptors", TextTitle.DEFAULT_FONT, spiderwebplot, false);
        LegendTitle legendtitle = new LegendTitle(spiderwebplot);
        legendtitle.setPosition(RectangleEdge.BOTTOM);
        jfreechart.addSubtitle(legendtitle);

        //    jfreechart.getPlot();
        return jfreechart;
    }

    public DefaultCategoryDataset createDataset(int m, int n) {
        System.loadLibrary("GraphMolWrap");
        //RWMol mol2=RWMol.MolFromMol2File("E:\\Ecust\\project\\Code\\lib-eshafts(2017.11.21)\\workspace\\localwork\\20170813-1\\ZINC03812962.mol2");
        //System.out.println("hello------------"+RDKFuncs.calcNumHBA(mol2));
        //data1[][0]、data2[][0]放的编号。data1[][1]、data2[][1]放的分子名，data1[][2]、data2[][2]放的分数
        //data3[][0]、data4[][0]、data5[][0]放的编号，data3[][1]、data4[][1]、data5[][1]放的分数
        GetScore rt = new GetScore();
        String CureentPath = getCurrentPath();
        System.out.println(CureentPath);
        String name = new ListFinder().GetList(CureentPath);
        Object[][] data1 = rt.read(CureentPath + name, m, n, 3);

        GetScore rt2 = new GetScore();
        Object[][] data2 = rt2.read(CureentPath + name, m, n, 4);
        Object[][] data3 = new Object[n - m + 1][2];
        Object[][] data4 = new Object[n - m + 1][2];
        Object[][] data5 = new Object[n - m + 1][2];
        Object[][] data6 = new Object[n - m + 1][2];
        double d1[] = new double[n - m + 1];
        double d2[] = new double[n - m + 1];
        double d3[] = new double[n - m + 1];
        double d4[] = new double[n - m + 1];
        double d5[] = new double[n - m + 1];
        double d6[] = new double[n - m + 1];
        double nd1[] = new double[n - m + 1];
        double nd2[] = new double[n - m + 1];
        double nd3[] = new double[n - m + 1];
        double nd4[] = new double[n - m + 1];
        double nd5[] = new double[n - m + 1];
        double nd6[] = new double[n - m + 1];
        //OBConversion conv=new OBConversion();
        //OBMol mol = new OBMol();
        for (int i = 0; i < n - m + 1; i++) {

            // RWMol mol=RWMol.MolFromMol2File("E:\\Ecust\\project\\Code\\lib-eshafts(2017.11.21)\\workspace\\localwork\\20170813-1\\ZINC03812962.mol2");
             RWMol mol = RWMol.MolFromMol2File(CureentPath + data2[i][1] + ".mol2");
             //临时更改
            //将mol2转换为smiles
//            String mol2Path = CureentPath + data2[i][1] + ".mol2";
//            String smilesPath = CureentPath + data2[i][1] + ".smiles";
//            System.out.println("mol2path:"+mol2Path+" smilespath: "+smilesPath);
//            FormatChange.change(mol2Path, smilesPath);
//            System.out.println("1111111111111111111111111");
//            SmilesReader sr = new SmilesReader();
//            String smiles = sr.sreader(smilesPath);
//            System.out.println("2222222222222222222222222222");
            
         //   RWMol rm = RWMol.MolFromSmiles(smiles);
          //  System.out.println(smiles);
          
            data3[i][0] = data2[i][1];
            data3[i][1] = RDKFuncs.calcNumHBA(mol);
            data4[i][0] = data2[i][1];
            data4[i][1] = RDKFuncs.calcNumHBD(mol);
            data5[i][0] = data2[i][1];
            data5[i][1] = RDKFuncs.calcMolLogP(mol);
            //data6[i][0] = data2[i][1];
            //data6[i][1] = RDKFuncs.calcTPSA(mol);
        }
        for (int i = 0; i < n - m + 1; i++) {
            d1[i] = Double.parseDouble(data1[i][2].toString());

        }
        for (int i = 0; i < n - m + 1; i++) {
            d2[i] = Double.parseDouble(data2[i][2].toString());

        }
        for (int i = 0;i < n - m + 1; i++) {
            d3[i] = Double.parseDouble(data3[i][1].toString());

        }
        for (int i = 0;i < n - m + 1; i++) {
            d4[i] = Double.parseDouble(data4[i][1].toString());

        }
        for (int i = 0;i < n - m + 1; i++) {
            d5[i] = Double.parseDouble(data5[i][1].toString());

        }
//        for (int i = 0;i < n - m + 1; i++) {
//            d6[i] = Double.parseDouble(data6[i][1].toString());
//
//        }
        //将数值归一化处理
        Normalization nm = new Normalization();
        nd1 = nm.normal(d1);
        nd2 = nm.normal(d2);
        nd3 = nm.normal(d3);
        nd4 = nm.normal(d4);
        nd5 = nm.normal(d5);
       // nd6 = nm.normal(d6);

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        for (int i = 0;
                i < n - m + 1; i++) {
            String groupi = "Molecule ranked " + data1[i][0];
            //临时更改
//            dataset.addValue(nd1[i], groupi, "ShapeScore");
//            dataset.addValue(nd2[i], groupi, "FeatureScore");
//            dataset.addValue(nd3[i], groupi, "NumHBA");
//            dataset.addValue(nd4[i], groupi, "NumHBD");
//            dataset.addValue(nd5[i], groupi, "MolLogP");
//            dataset.addValue(nd6[i], groupi, "TPSA");
            dataset.addValue(d1[i], groupi, "ShapeScore");  
            dataset.addValue(d2[i], groupi, "FeatureScore");  
            dataset.addValue(d3[i], groupi, "NumHBA");  
            dataset.addValue(d4[i], groupi,"NumHBD");
            dataset.addValue(d5[i], groupi, "MolLogP");
            //临时更改
//            dataset.addValue(d6[i], groupi, "TPSA");

        }
        //临时更改
        //求出最大的数值
//        double MaxData1 = nd1[0];
//        double MaxData2 = nd2[0];
//        double MaxData3 = nd3[0];
//        double MaxData4 = nd4[0];
//        double MaxData5 = nd5[0];
//        double MaxData6 = nd6[0];
//
//        for (int i = 0;
//                i < n - m + 1; i++) {
//            if (MaxData1 < nd1[i]) {
//                MaxData1 = nd1[i];
//            }
//            if (MaxData2 < nd2[i]) {
//                MaxData2 = nd2[i];
//            }
//            if (MaxData3 < nd3[i]) {
//                MaxData3 = nd3[i];
//            }
//            if (MaxData4 < nd4[i]) {
//                MaxData4 = nd4[i];
//            }
//            if (MaxData5 < nd5[i]) {
//                MaxData5 = nd5[i];
//            }
//            if (MaxData6 < nd6[i]) {
//                MaxData6 = nd6[i];
//            }
//        }
       double MaxData1=d1[0];
       double MaxData2=d2[0];
       double MaxData3=d3[0];
       double MaxData4=d4[0];
       double MaxData5=d5[0];
       double MaxData6=d6[0];
       
       for(int i=0;i<n-m+1;i++){
           if(MaxData1<d1[i]){
               MaxData1=d1[i];
           }
            if(MaxData2<d2[i]){
               MaxData2=d2[i];
           }
             if(MaxData3<d3[i]){
               MaxData3=d3[i];
           }
              if(MaxData4<d4[i]){
               MaxData4=d4[i];
           }
               if(MaxData5<d5[i]){
               MaxData5=d5[i];
           }
                if(MaxData6<d6[i]){
               MaxData6=d6[i];
           }
       }
        double[] List = {MaxData1, MaxData2, MaxData3, MaxData4, MaxData5, MaxData6};
        MaxNum = List[0];
        //临时更改
        for (int i = 0;
                i < 5; i++) {
            if (MaxNum < List[i]) {
                MaxNum = List[i];
            }
        }

        System.out.print(MaxNum);

        return dataset;
    }

}
